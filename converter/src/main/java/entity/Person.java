package entity;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

@XStreamAlias("person")
public class Person {

    @XStreamAsAttribute
    @XStreamAlias("ID")
    private int id;

    @XStreamAlias("surname")
    private String surname;

    @XStreamAlias("name")
    private String name;

    @XStreamAlias("birthday")
    private Birthday birthday;

    @XStreamAlias("birthplace")
    private Birthplace birthplace;

    @XStreamAlias("work")
    private String work;

    public Person() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getWork() {
        return work;
    }

    public void setWork(String work) {
        this.work = work;
    }


    public Birthday getBirthday() {
        return birthday;
    }

    public void setBirthday(Birthday birthday) {
        this.birthday = birthday;
    }


    public Birthplace getBirthplace() {
        return birthplace;
    }

    public void setBirthplace(Birthplace birthplace) {
        this.birthplace = birthplace;
    }
}
