package entity;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import java.time.LocalDate;
import java.time.Period;

@XStreamAlias("birthday")
public class Birthday {

    @XStreamAlias("day")
    private int day;

    @XStreamAlias("month")
    private int month;

    @XStreamAlias("year")
    private int year;

    public int getAge(){
        LocalDate birthday = LocalDate.of(year,month,day);
        return Period.between(birthday,LocalDate.now()).getYears();
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
}
