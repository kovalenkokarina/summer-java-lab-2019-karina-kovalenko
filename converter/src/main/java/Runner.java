import entity.Birthday;
import entity.Birthplace;
import entity.Person;
import org.xml.sax.SAXException;
import parser.ParseMarshalling;
import parser.ParsePeople;

import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Runner {
    public static void main(String[] args) throws IOException, TransformerException, SAXException {
        String file = "converter/src/main/resources/people.xml";
        ParsePeople parsePeople = new ParsePeople(file);
        Person person = new Person(){{
            setId(3);
            setSurname("Elenova");
            setName("Elena");
            setWork("ITechArt");
            setBirthday(new Birthday(){{setDay(1); setYear(2000); setMonth(1);}});
            setBirthplace(new Birthplace(){{setCity("Minsk");}});
        }};

        parsePeople.addPersonBeforeElementByIndex(person,2);
        parsePeople.changeIdAfterElementByIndex(3);
        parsePeople.changeWorkPersonByStartName();

        ParseMarshalling marshalling = new ParseMarshalling(file);

        List<Person> people = marshalling.unmarshalling();
        people.sort(Comparator.comparingInt((Person p) -> p.getBirthday().getAge()));
        marshalling.marshaller(people);
        System.out.println(marshalling.readFromFile());

    }
}

