package parser;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import entity.Birthday;
import entity.Birthplace;
import entity.Person;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class ParseMarshalling {
    private final String fileName;

    public ParseMarshalling(String fileName){
        this.fileName = fileName;
    }

    public void marshaller(List<Person> personList) throws IOException {
        XStream xStream = new XStream(new DomDriver());
        xStream.alias("people",List.class);
        xStream.processAnnotations(Person.class);
        saveToFile(xStream.toXML(personList));
    }

    public List<Person> unmarshalling(){
        XStream xStream = new XStream(new DomDriver());
        xStream.alias("people",List.class);
        xStream.alias("person",Person.class);
        xStream.aliasField("surname", Person.class, "surname");
        xStream.aliasField("name", Person.class, "name");
        xStream.aliasField("birthday", Birthday.class,"birthday");
        xStream.aliasField("work", Person.class, "work");
        xStream.aliasAttribute(Person.class,"id","ID");
        xStream.aliasField("birthplace",Person.class,"birthplace");
        xStream.aliasAttribute(Birthplace.class,"city","city");
        return (ArrayList<Person>) xStream.fromXML(new File(fileName));

    }

    private void saveToFile(String xml) throws IOException {
        try(BufferedWriter bw = new BufferedWriter(new FileWriter(new File(fileName)))){
            bw.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            bw.write(xml);
        }
    }

    public String readFromFile() throws IOException {
        StringBuilder stringBuilder = new StringBuilder();
        try(BufferedReader br = new BufferedReader(new FileReader(new File(fileName)))){
            String line;
            while ((line = br.readLine())!=null){
                stringBuilder.append(line).append("\n");
            }
        }
        return stringBuilder.toString();
    }
}
