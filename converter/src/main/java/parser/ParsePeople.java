package parser;

import entity.Person;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;
import java.util.stream.IntStream;

public class ParsePeople {
    private DocumentBuilder builder;
    private final String fileName;
    private Document doc;

    public ParsePeople(String fileName){
        this.fileName = fileName;
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            builder = factory.newDocumentBuilder();
            doc = builder.parse(fileName);
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }
    }

    private Element createPersonElement(Person person) {
        Element root = doc.getDocumentElement();
        Element elementPerson = doc.createElement("person");
        root.appendChild(elementPerson);
        elementPerson.setAttribute("ID", String.valueOf(person.getId()));

        Element name = doc.createElement("name");
        elementPerson.appendChild(name);
        name.appendChild(doc.createTextNode(person.getName()));

        Element surname = doc.createElement("surname");
        elementPerson.appendChild(surname);
        surname.appendChild(doc.createTextNode(person.getSurname()));

        Element birthday = doc.createElement("birthday");
        elementPerson.appendChild(birthday);

        Element day = doc.createElement("day");
        Element month = doc.createElement("month");
        Element year = doc.createElement("year");

        birthday.appendChild(day);
        birthday.appendChild(month);
        birthday.appendChild(year);

        day.appendChild(doc.createTextNode(String.valueOf(person.getBirthday().getDay())));
        month.appendChild(doc.createTextNode(String.valueOf(person.getBirthday().getMonth())));
        year.appendChild(doc.createTextNode(String.valueOf(person.getBirthday().getYear())));

        Element birthplace = doc.createElement("birthplace");
        elementPerson.appendChild(birthplace);
        birthplace.setAttribute("city",person.getBirthplace().getCity());

        Element work = doc.createElement("work");
        elementPerson.appendChild(work);
        work.appendChild(doc.createTextNode(person.getWork()));

        return elementPerson;
    }

    public void addPersonBeforeElementByIndex(Person person, int index) throws IOException, SAXException, TransformerException {
        NodeList list = doc.getElementsByTagName("person");
        doc.getFirstChild().insertBefore(createPersonElement(person), list.item(index));
        changeFile();
    }

    public void changeIdAfterElementByIndex(int index) throws TransformerException, FileNotFoundException {
        NodeList list = doc.getElementsByTagName("person");
        IntStream.range(index, list.getLength()).forEach(i->{
            Element element = (Element) list.item(i);
            element.setAttribute("ID",String.valueOf(i+1));
        });
        changeFile();
    }

    public void changeWorkPersonByStartName() throws TransformerException, FileNotFoundException {
        NodeList list = doc.getElementsByTagName("person");
        IntStream.range(0, list.getLength()).forEach(i->{
            Element person = (Element) list.item(i);
            Element work = (Element) person.getElementsByTagName("work").item(0);
            if(person.getElementsByTagName("name").item(0).getTextContent().startsWith("I")) {
                Random r = new Random();
                StringBuilder builder = new StringBuilder();
                IntStream.range(0,10).forEach(j-> builder.append((char)(r.nextInt(26) + 'a')));
                work.setTextContent(builder.toString());
            }
        });
        changeFile();
    }

    private void changeFile() throws TransformerException, FileNotFoundException {
        Transformer t = TransformerFactory.newInstance().newTransformer();
        t.setOutputProperty(OutputKeys.INDENT, "yes");
        t.transform(new DOMSource(doc), new StreamResult(new FileOutputStream(fileName)));
    }

}
