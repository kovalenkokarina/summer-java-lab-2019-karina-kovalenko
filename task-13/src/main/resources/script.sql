-- drop database if exists shop;
-- create database shop;
create table product_type(
	id int not null auto_increment,
	name varchar(255) not null,
    primary key (id)
);

create table provider(
	id int auto_increment not null,
    name varchar(255) not null,
    primary key(id)
);

create table product(
	id int auto_increment not null,
    name varchar(255) not null,
    price decimal(18,2),
    product_type_id int,
    about varchar(255),
    primary key (id),
    foreign key (product_type_id) references product_type(id)
);

create table delivery(
	id int not null auto_increment,
    provider_id int,
    delivery_date Date not null,
    primary key(id),
    foreign key (provider_id) references provider(id)
);

create table waybill(
	id int not null auto_increment,
    product_id int,
    delivery_id int,
	amount int,
    primary key(id),
    foreign key (product_id) references product(id),
    foreign key (delivery_id) references provider(id)
);

insert into product_type (name) values ('Phone');
insert into product_type (name) values ('Laptop');
insert into product_type (name) values ('Computer');
insert into product_type (name) values ('Clock');
insert into product_type (name) values ('Electric scooter');

insert into product (name, price, product_type_id, about) values ('IPhone SE', 800, 1, 'qwertyu');
insert into product (name, price, product_type_id, about) values ('Sumsung', 1700, 1, 'ggfgg');
insert into product (name, price, product_type_id, about) values ('HP 250', 1000, 2, 'gfgfg');
insert into product (name, price, product_type_id, about) values ('ZALMAN A1', 1500, 3, 'ssss');
insert into product (name, price, product_type_id, about) values ('Apple Watch 4', 899, 4, 'ewwww');
insert into product (name, price, product_type_id, about) values ('Xiaomi Mijia Smart', 699, 5, 'rggew');
insert into product (name, price, product_type_id, about) values ('Kugoo S3', 389, 5, 'hgrgg');

insert into provider (name) values ('Apple');
insert into provider (name) values ('Sumsung');
insert into provider (name) values ('Xiaomi');
insert into provider (name) values ('HP');
insert into provider (name) values ('ZALMAN');

insert into delivery (provider_id, delivery_date) values (1, '2010-01-01');
insert into delivery (provider_id, delivery_date) values (2, '2019-02-02');
insert into delivery (provider_id, delivery_date) values (3, '2019-03-03');
insert into delivery (provider_id, delivery_date) values (4, '2019-04-04');
insert into delivery (provider_id, delivery_date) values (5, '2019-05-05');

insert into waybill (product_id, delivery_id, amount) values (1,1,100);
insert into waybill (product_id, delivery_id, amount) values (2,2,10);
insert into waybill (product_id, delivery_id, amount) values (3,4,24);
insert into waybill (product_id, delivery_id, amount) values (4,5,0);
insert into waybill (product_id, delivery_id, amount) values (5,1,98);
insert into waybill (product_id, delivery_id, amount) values (6,5,1);
insert into waybill (product_id, delivery_id, amount) values (7,1,98);
