package model;

import org.springframework.stereotype.Component;

import javax.persistence.*;

@Entity
@Table(name = "type_product")
@Component
public class TypeProduct {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        StringBuilder table = new StringBuilder();
        table.append(String.format("%-3s", id));
        table.append(String.format("%-20s",name));
        table.append("\n");
        return table.toString();
    }
}
