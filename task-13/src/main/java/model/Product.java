package model;

import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "product")
@Component
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "name")
    private String name;
    @Column(name = "price")
    private BigDecimal price;
    @Column(name = "typeProduct")
    private TypeProduct typeProduct;
    @Column(name = "about")
    private String about;
//    @Column(name = "image", columnDefinition="TEXT")
//    private byte[] image;

//    public Product(byte[] image) {
//        this.image = image;
//    }

    public Product() {

    }

//    public byte[] getImage() {
//        return this.image;
//    }
//
//    public void setImage(byte[] image) {
//        this.image = image;
//    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public TypeProduct getTypeProduct() {
        return typeProduct;
    }

    public void setTypeProduct(TypeProduct typeProduct) {
        this.typeProduct = typeProduct;
    }

    @Override
    public String toString() {
        StringBuilder table = new StringBuilder();
        table.append(String.format("%-3s", id)).append("|");
        table.append(String.format("%-20s",name)).append("|");
        table.append(String.format("%-20s",price)).append("|");
        table.append(String.format("%-20s", typeProduct.getName())).append("|");
        table.append("\n");
        return table.toString();
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }
}
