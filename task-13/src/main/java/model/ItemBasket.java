package model;

public class ItemBasket {

    private Product product;
    private Integer amount;

    public ItemBasket(){}

    public ItemBasket(Product product, Integer amount) {
        this.product = product;
        this.amount = amount;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public void plusAmount(int amount){
        this.amount+=amount;
    }
}
