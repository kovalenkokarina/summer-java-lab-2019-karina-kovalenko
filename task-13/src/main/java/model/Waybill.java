package model;

public class Waybill {
    private int id;
    private Product product;
    private Delivery delivery;
   // private BigDecimal cost;
    private int amount;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

//    public BigDecimal getCost() {
//        return cost;
//    }
//
//    public void setCost(BigDecimal cost) {
//        this.cost = cost;
//    }

    public void minus(int amount){
        this.amount-=amount;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Delivery getDelivery() {
        return delivery;
    }

    public void setDelivery(Delivery delivery) {
        this.delivery = delivery;
    }

    public String toString(){
        StringBuilder table = new StringBuilder();
        table.append(String.format("%-3s", id)).append("|");
        table.append(String.format("%-20s",product.getName())).append("|");
        table.append(String.format("%-20s",product.getPrice())).append("|");
        table.append(String.format("%-20s", product.getTypeProduct().getName())).append("|");
        table.append(String.format("%-20s", delivery.getProvider().getName())).append("|");
        table.append(String.format("%-15s", delivery.getDateDelivery())).append("|");
       // table.append(String.format("%-15s", cost)).append("|");
        table.append(String.format("%-15s", amount)).append("|");
        table.append("\n");
        return table.toString();
    }
}
