package services;

import dao.ProductDao;
import model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService implements CrudService<Product> {

    @Autowired
    ProductDao productDao;

    @Override
    public void add(Product product){
        productDao.create(product);
    }

    @Override
    public List<Product> getAll(){
        return productDao.findAll();
    }

    @Override
    public Product getById(int id){
        return productDao.findById(id);
    }

    @Override
    public void update(Product entity) {
        productDao.update(entity);
    }

    @Override
    public void delete(Product entity) {
        productDao.delete(entity);
    }
}
