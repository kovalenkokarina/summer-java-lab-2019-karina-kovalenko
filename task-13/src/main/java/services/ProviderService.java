package services;

import dao.ProviderDao;
import model.Provider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProviderService implements CrudService<Provider> {

    @Autowired
    private ProviderDao providerDao;

    @Override
    public void add(Provider entity) {
        providerDao.create(entity);
    }

    @Override
    public void update(Provider entity) {
        providerDao.update(entity);
    }

    @Override
    public void delete(Provider entity) {
        providerDao.delete(entity);
    }

    @Override
    public Provider getById(int id) {
        return providerDao.findById(id);
    }

    @Override
    public List<Provider> getAll() {
        return providerDao.findAll();
    }
}
