package services;

import dao.TypeProductDao;
import model.TypeProduct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TypeProductService {

    @Autowired
    TypeProductDao typeProductDao;

    public List<TypeProduct> getAll(){
        return typeProductDao.findAll();
    }
}
