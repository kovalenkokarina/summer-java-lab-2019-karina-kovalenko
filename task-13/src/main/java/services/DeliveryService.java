package services;

import dao.DeliveryDao;
import model.Delivery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DeliveryService implements CrudService<Delivery> {
    @Autowired
    private DeliveryDao deliveryDao;

    @Override
    public void add(Delivery entity) {
        deliveryDao.create(entity);
    }

    @Override
    public void update(Delivery entity) {
        deliveryDao.update(entity);
    }

    @Override
    public void delete(Delivery entity) {
        deliveryDao.delete(entity);
    }

    @Override
    public Delivery getById(int id) {
        return deliveryDao.findById(id);
    }

    @Override
    public List<Delivery> getAll() {
        return deliveryDao.findAll();
    }
}
