package services;

import dao.WaybillDao;
import model.ItemBasket;
import model.Waybill;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class WaybillService  implements CrudService<Waybill>{

    @Autowired
    private WaybillDao waybillDao;

    @Override
    public void add(Waybill entity) {
        waybillDao.create(entity);
    }

    @Override
    public void update(Waybill waybill){
        waybillDao.update(waybill);
    }

    @Override
    public void delete(Waybill entity) {
        waybillDao.delete(entity);
    }

    @Override
    public Waybill getById(int id) {
        return waybillDao.findById(id);
    }

    @Override
    public List<Waybill> getAll() {
        return waybillDao.findAll();
    }

    public Waybill getWaybillByProductId(int id){
        return waybillDao.findByProductId(id);
    }

    public void addToBasket(ItemBasket itemBasket, HttpSession httpSession,int id) throws Exception {
        Waybill waybill = getWaybillByProductId(id);
        int amount = itemBasket.getAmount()!=null ? itemBasket.getAmount() : 1;
        if((waybill.getAmount() - amount)>=0) {
            List<ItemBasket> basket = (List<ItemBasket>) httpSession.getAttribute("basket");
            if(basket == null){
                basket = new ArrayList<>();
                basket.add(new ItemBasket(waybill.getProduct(), amount));
            }else{
                Optional<ItemBasket> findItem = basket.stream().filter(item-> item.getProduct().getId()==waybill.getProduct().getId()).findAny();
                if(findItem.isPresent()){
                    ItemBasket updateItem = findItem.get();
                    basket.remove(updateItem);
                    updateItem.plusAmount(amount);
                   // basket.add(basket.indexOf(findItem.get()), updateItem);
                   // basket.remove(findItem.get());
                    basket.add(updateItem);
                }else basket.add(new ItemBasket(waybill.getProduct(), amount));
            }
            httpSession.setAttribute("basket", basket);
            waybill.minus(amount);
            update(waybill);
        }else {
            throw new Exception("This product is not available. Quantity in stock:" + waybill.getAmount());
        }
    }
}
