package dao.jdbc;

import dao.ProviderDao;
import model.Provider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class JdbcProviderDao implements ProviderDao {

    public static final String PROVIDER_COLUMN_ID = "id";
    public static final String PROVIDER_COLUMN_NAME = "name";
    public static final String QUERY_SELECT_BY_ID = "SELECT * FROM provider WHERE id=";
    public static final String QUERY_SELECT = "SELECT * FROM provider";
    public static final String QUERY_DELETE = "DELETE FROM provider WHERE id = ?";
    public static final String QUERY_UPDATE = "UPDATE provider SET name = ? WHERE id = ?";
    public static final String QUERY_INSERT = "INSERT INTO provider (name) VALUES (?)";

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public void create(Provider entity) {
        jdbcTemplate.update(QUERY_INSERT, entity.getName());
    }

    @Override
    public void update(Provider entity) {
        jdbcTemplate.update(QUERY_UPDATE, entity.getName(),entity.getId());
    }

    @Override
    public void delete(Provider entity) {
        jdbcTemplate.update(QUERY_DELETE,entity.getId());
    }

    @Override
    public Provider findById(int id) {
        return (Provider) jdbcTemplate.queryForObject(QUERY_SELECT_BY_ID + id, new BeanPropertyRowMapper(Provider.class));
    }

    @Override
    public List<Provider> findAll() {
        return jdbcTemplate.query(QUERY_SELECT, new BeanPropertyRowMapper(Provider.class));
    }
}
