package dao.jdbc;

import dao.ProductDao;
import dao.TypeProductDao;
import model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class JdbcProductDao implements ProductDao {

    private final static String QUERY_UPDATE ="UPDATE product SET name = ?, price = ?, product_type_id = ?, about = ? WHERE id = ?";
    private final static String QUERY_CREATE = "INSERT INTO product (name, price, product_type_id, about) VALUES (?, ?, ?, ?)";
    private final static String QUERY_DELETE = "DELETE FROM product WHERE id = ?";
    private final static String QUERY_SELECT_BY_ID = "SELECT * FROM product WHERE id =";
    private final static String QUERY_SELECT = "SELECT * FROM product";
    public static final String PRODUCT_COLUMN_ID = "id";
    public static final String PRODUCT_COLUMN_NAME = "name";
    public static final String PRODUCT_COLUMN_PRICE = "price";
    public static final String PRODUCT_COLUMN_TYPE = "product_type_id";
    public static final String PRODUCT_COLUMN_ABOUT = "about";

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    TypeProductDao typeProductDao;

    @Override
    public void create(Product product) {
        jdbcTemplate.update(QUERY_CREATE, product.getName(), product.getPrice(), product.getTypeProduct().getId(), product.getAbout());
    }

    @Override
    public void update(Product product) {
        jdbcTemplate.update(QUERY_UPDATE, product.getName(), product.getPrice(), product.getTypeProduct().getId(), product.getAbout());

    }

    @Override
    public void delete(Product product) {
        jdbcTemplate.update(QUERY_DELETE, product.getId());
    }

    @Override
    public Product findById(int id) {
        return jdbcTemplate.queryForObject(QUERY_SELECT_BY_ID + id, (resultSet,i) -> toProduct(resultSet));
    }

    @Override
    public List<Product> findAll() {
        return jdbcTemplate.query(QUERY_SELECT, (resultSet,i) -> toProduct(resultSet));
    }

    private Product toProduct(ResultSet resultSet) throws SQLException {
        Product product = new Product(){{
            setId(resultSet.getInt(PRODUCT_COLUMN_ID));
            setName(resultSet.getString(PRODUCT_COLUMN_NAME));
            setPrice(resultSet.getBigDecimal(PRODUCT_COLUMN_PRICE));
            setTypeProduct(typeProductDao.findById(resultSet.getInt(PRODUCT_COLUMN_TYPE)));
            setAbout(resultSet.getString(PRODUCT_COLUMN_ABOUT));
        }};
        return product;
    }
}
