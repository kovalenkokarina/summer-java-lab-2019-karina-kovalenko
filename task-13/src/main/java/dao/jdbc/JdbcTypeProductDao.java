package dao.jdbc;

import dao.TypeProductDao;
import model.TypeProduct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class JdbcTypeProductDao implements TypeProductDao {

    public static final String QUERY_SELECT = "SELECT * FROM product_type";
    public static final String QUERY_INSERT = "INSERT INTO product_type (name) VALUES (?)";
    public static final String QUERY_UPDATE= "UPDATE product_type SET name = ? WHERE id = ?";
    public static final String QUERY_DELETE = "DELETE FROM product_type WHERE id = ?";
    public static final String QUERY_SELECT_BY_ID = "SELECT * FROM product_type WHERE id =";


    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public void create(TypeProduct entity) {
        jdbcTemplate.update(QUERY_INSERT, entity.getName());
    }

    @Override
    public void update(TypeProduct entity) {
        jdbcTemplate.update(QUERY_UPDATE, entity.getName(), entity.getId());
    }

    @Override
    public void delete(TypeProduct entity) {
        jdbcTemplate.update(QUERY_DELETE,entity.getId());
    }

    @Override
    public TypeProduct findById(int id) {
        return (TypeProduct) jdbcTemplate.queryForObject(QUERY_SELECT_BY_ID + id, new BeanPropertyRowMapper(TypeProduct.class));
    }

    @Override
    public List<TypeProduct> findAll() {
        List<TypeProduct> typeProducts = jdbcTemplate.query(
                QUERY_SELECT, new BeanPropertyRowMapper(TypeProduct.class));
        return typeProducts;
    }
}
