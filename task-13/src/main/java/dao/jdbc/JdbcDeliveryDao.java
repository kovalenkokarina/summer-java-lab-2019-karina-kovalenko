package dao.jdbc;

import dao.DeliveryDao;
import dao.ProviderDao;
import model.Delivery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class JdbcDeliveryDao implements DeliveryDao {

    public static final String DELIVERY_COLUMN_ID = "id";
    public static final String DELIVERY_COLUMN_PROVIDER_ID = "provider_id";
    public static final String DELIVERY_COLUMN_DATE = "delivery_date";

    public static final String QUERY_SELECT_BY_ID = "SELECT * FROM delivery WHERE id = ?";
    public static final String QUERY_SELECT = "SELECT * FROM delivery";
    public static final String QUERY_DELETE = "DELETE FROM delivery WHERE id = ?";
    public static final String QUERY_UPDATE = "UPDATE delivery SET provider_id = ?, delivery_date = ? WHERE id = ?";
    public static final String QUERY_INSERT = "INSERT INTO delivery (provider_id, delivery_date) VALUES (?, ?)";

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private ProviderDao providerDao;

    @Override
    public void create(Delivery entity) {
        jdbcTemplate.update(QUERY_INSERT, entity.getProvider().getId(), entity.getDateDelivery());
    }

    @Override
    public void update(Delivery entity) {
        jdbcTemplate.update(QUERY_UPDATE, entity.getProvider().getId(), entity.getDateDelivery(), entity.getId());
    }

    @Override
    public void delete(Delivery entity) {
        jdbcTemplate.update(QUERY_DELETE, entity.getProvider().getId(), entity.getDateDelivery(), entity.getId());
    }

    @Override
    public List<Delivery> findAll() {
        List<Delivery> deliveries = jdbcTemplate.query(QUERY_SELECT, (resultSet,i) -> toDelivery(resultSet));
        return deliveries;
    }

    @Override
    public Delivery findById(int id) {
        return  jdbcTemplate.queryForObject(QUERY_SELECT_BY_ID,new Object[]{id}, (resultSet,i) -> toDelivery(resultSet));
    }

    private Delivery toDelivery(ResultSet resultSet) throws SQLException {
        Delivery delivery = new Delivery(){{
                setId(resultSet.getInt(DELIVERY_COLUMN_ID));
                setProvider(providerDao.findById(resultSet.getInt(DELIVERY_COLUMN_PROVIDER_ID)));
                setDateDelivery(resultSet.getDate(DELIVERY_COLUMN_DATE).toLocalDate());
            }};
        return delivery;
    }
}
