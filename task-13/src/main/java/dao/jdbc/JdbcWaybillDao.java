package dao.jdbc;

import dao.DeliveryDao;
import dao.ProductDao;
import dao.WaybillDao;
import model.Waybill;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class JdbcWaybillDao implements WaybillDao {

    public static final String QUERY_SELECT = "SELECT * FROM waybill";
    public static final String QUERY_INSERT = "INSERT INTO waybill (product_id, delivery_id,  amount) VALUES (?, ?, ?)";
    public static final String QUERY_UPDATE= "UPDATE waybill SET product_id = ?, delivery_id = ?,  amount = ? WHERE id = ?";
    public static final String QUERY_DELETE = "DELETE FROM waybill WHERE id = ?";

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private DeliveryDao deliveryDao;

    @Autowired
    private ProductDao productDao;

    @Override
    public void create(Waybill entity) {
        jdbcTemplate.update(QUERY_INSERT,
                entity.getProduct().getId(),
                entity.getDelivery().getId(),
                entity.getAmount(),
                entity.getId());
    }

    @Override
    public void update(Waybill entity) {
        jdbcTemplate.update(QUERY_UPDATE,
                entity.getProduct().getId(),
                entity.getDelivery().getId(),
                entity.getAmount(),
                entity.getId());
    }

    @Override
    public void delete(Waybill entity) {
        jdbcTemplate.update(QUERY_DELETE, entity.getId());
    }

    @Override
    public Waybill findById(int id) {
        String query = QUERY_SELECT + "WHERE id = " + id;
        return jdbcTemplate.queryForObject(query, (resultSet,i) -> toWaybill(resultSet));
    }

    @Override
    public Waybill findByProductId(int id) {
        String query = QUERY_SELECT + " WHERE product_id =" + id;
        return jdbcTemplate.queryForObject(query, (resultSet,i) -> toWaybill(resultSet));
    }

    @Override
    public List<Waybill> findAll() {
        return jdbcTemplate.query(QUERY_SELECT, (resultSet,i) -> toWaybill(resultSet));
    }

    private Waybill toWaybill(ResultSet resultSet) throws SQLException {
        Waybill waybill = new Waybill(){{
            setAmount(resultSet.getInt("amount"));
            setDelivery(deliveryDao.findById(resultSet.getInt("delivery_id")));
            setId(resultSet.getInt("id"));
            setProduct(productDao.findById(resultSet.getInt("product_id")));
        }};
        return waybill;
    }
}
