package dao;

import model.Waybill;

public interface WaybillDao extends GenericDao<Waybill> {

    Waybill findByProductId(int id);
}
