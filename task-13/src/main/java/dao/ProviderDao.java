package dao;

import model.Provider;

public interface ProviderDao extends GenericDao<Provider> {
}
