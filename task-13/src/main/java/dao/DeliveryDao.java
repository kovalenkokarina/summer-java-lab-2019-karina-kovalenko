package dao;

import model.Delivery;

public interface DeliveryDao extends GenericDao<Delivery> {
}
