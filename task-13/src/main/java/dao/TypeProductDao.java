package dao;

import model.TypeProduct;

public interface TypeProductDao extends GenericDao<TypeProduct> {
}
