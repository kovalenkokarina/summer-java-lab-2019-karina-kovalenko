package controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import services.WaybillService;

@Controller
@RequestMapping("/waybill")
@Configuration
@ComponentScan("services")
public class WaybillController {

    @Autowired
    private WaybillService service;

    @RequestMapping(value = "/admin/getAll", method = RequestMethod.GET)
    public String index(Model model){
        model.addAttribute("waybill", service.getAll());
        return "/admin/waybill/index";
    }
}
