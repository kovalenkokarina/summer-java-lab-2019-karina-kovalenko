package controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import services.DeliveryService;

@Controller
@RequestMapping("/delivery")
@Configuration
@ComponentScan({"services"})
public class DeliveryController {

    @Autowired
    private DeliveryService service;

    @RequestMapping(value = "/admin/getAll", method = RequestMethod.GET)
    public String index(Model model){
        model.addAttribute("delivery", service.getAll());
        return "/admin/delivery/index";
    }
}
