package controller;

import model.ItemBasket;
import model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import services.ProductService;
import services.WaybillService;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/products")
@Configuration
@ComponentScan("services")
public class ProductController {

    @Autowired
    private ProductService productService;

    @Autowired
    private WaybillService waybillService;

    @RequestMapping(value = "user/")
    public ModelAndView getProducts(ItemBasket itemBasket){
        ModelAndView modelAndView = new ModelAndView();
        List<Product> products = productService.getAll().stream().filter(i->waybillService.getWaybillByProductId(i.getId()).getAmount()!=0).collect(Collectors.toList());
        modelAndView.addObject("allProduct", products);
        modelAndView.setViewName("user/products");
        return modelAndView;
    }

    @RequestMapping(value ="user/details/{id}")
    public ModelAndView getDetails(@PathVariable("id") Integer id){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("product", productService.getById(id));
        modelAndView.setViewName("user/details");
        return modelAndView;
    }

    @RequestMapping(value = "/admin/getAll")
    public ModelAndView getProducts(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("allProduct", productService.getAll());
        modelAndView.setViewName("admin/product/index");
        return modelAndView;
    }

}
