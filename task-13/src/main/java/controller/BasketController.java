package controller;

import model.ItemBasket;
import model.Waybill;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import services.WaybillService;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/basket")
@Configuration
@ComponentScan("services")
public class BasketController {

    @Autowired
    private WaybillService waybillService;

    @ModelAttribute("basket")
    public List<ItemBasket> listProduct() {
        return new ArrayList<>();
    }

    @RequestMapping(value = "user/buyProduct/{id}")
    public String addToBasket(ItemBasket itemBasket,@PathVariable Integer id, HttpSession httpSession) throws Exception {
        waybillService.addToBasket(itemBasket,httpSession, id);
        return "redirect:/products/user/";
    }

    @RequestMapping(value ={"user/","user/getProductsFromBasket"})
    public ModelAndView getProducts(HttpSession httpSession, ItemBasket itemBasket){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("basket", httpSession.getAttribute("basket"));
        modelAndView.setViewName("/user/basket");
        return modelAndView;
    }

    @RequestMapping(value = "user/delete/{id}")
    public String delete(@PathVariable Integer id, HttpSession httpSession) {
        List<ItemBasket> basket = (List<ItemBasket>) httpSession.getAttribute("basket");
        if(basket==null) basket = new ArrayList<>();
        Optional<ItemBasket> deleteProduct = basket.stream().filter(i->i.getProduct().getId() == id).findAny();
        basket.remove(deleteProduct.get());
        Waybill waybill = waybillService.getWaybillByProductId(deleteProduct.get().getProduct().getId());
        waybill.setAmount(deleteProduct.get().getAmount());
        waybillService.update(waybill);
        httpSession.setAttribute("basket", basket);
        return "redirect:/basket/user/";
    }

}
