package controller;

import model.User;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDate;

@Controller
@Configuration
public class MainController {

    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public String adminPage() {
        return "admin";
    }

    @RequestMapping(value = "/account", method = RequestMethod.GET)
    public ModelAndView viewUserPage() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        ModelAndView modelAndView = new ModelAndView();
        if(auth.getName().equals("user")) modelAndView.addObject("account",user());
        else modelAndView.addObject("account", admin());
        modelAndView.setViewName("/account");
        return modelAndView;
    }

    @Bean
    public User user(){
        User user = new User(){{
            setSurname("Kovalenko");
            setBirthday(LocalDate.of(1999,04,21));
            setName("Karina");
            setPatronymic("Alexandrovna");
            setUsername("user");
            setEmail("Karina.210428@gmail.com");
        }};
        return user;
    }

    @Bean
    public User admin(){
        User user = new User(){{
            setSurname("Admin");
            setBirthday(LocalDate.of(2000,04,21));
            setName("Admin");
            setPatronymic("Admin");
            setUsername("admin");
            setEmail("Admin@gmail.com");
        }};
        return user;
    }

    @RequestMapping(value = "/logout", method = RequestMethod.POST)
    public String about() {
        return "login";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login() {
        return "login";
    }
}
