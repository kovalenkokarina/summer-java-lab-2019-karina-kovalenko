import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Runner {
    public static void main(String[] args) {
        LocalDateTime localDateTime = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MMMM ss k yyyy mm");
        System.out.println("HelloWorld: "+localDateTime.format(formatter)+
                "\nHelloWorld + 1 week: "+localDateTime.plusWeeks(1).format(formatter));

    }
}
