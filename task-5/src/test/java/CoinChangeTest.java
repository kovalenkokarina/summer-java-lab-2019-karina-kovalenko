import coinExchange.CoinChange;
import org.junit.Test;

import java.io.*;

import static junit.framework.TestCase.assertEquals;

public class CoinChangeTest {

    CoinChange coinChange = new CoinChange();
    File file;

    @Test
    public void toOutputNoOneWayError() throws IOException {
        file = createFile("9 4");
        assertEquals("9 4\nError: No one way\n\n",coinChange.getSolution(file.getAbsolutePath()));
    }

    private File createFile(String task) throws IOException {
        file = File.createTempFile("TempFile",".txt");
        try (FileWriter writer = new FileWriter(file,true)){
            writer.write(task);
        }
        return file;
    }

    @Test
    public void toOutputNoOneWayErrorIfSumAndValueAreTheSame() throws IOException {
        file = createFile("7 7");
        assertEquals("7 7\nError: No one way\n\n",coinChange.getSolution(file.getAbsolutePath()));
    }

    @Test
    public void toOutputDuplicatesError() throws IOException {
        file = createFile("5 2 2");
        assertEquals("5 2 2\nError: Duplicates\n\n",coinChange.getSolution(file.getAbsolutePath()));
    }

    @Test
    public void toOutputThereAreNotCoinsIfEmptyLine() throws IOException {
        file = createFile(" ");
        assertEquals(" \nError: There are not coins\n\n",coinChange.getSolution(file.getAbsolutePath()));
    }

    @Test
    public void toOutputThereAreNotCoinsIfOneNumber() throws IOException {
        file = createFile("5");
        assertEquals("5\nError: There are not coins\n\n",coinChange.getSolution(file.getAbsolutePath()));
    }

    @Test
    public void toExchangeFourCoinsInOneAndTwoBill() throws IOException {
        file = createFile("4 2 1");
        assertEquals("4 2 1\n1 1 1 1\n1 1 2\n2 2\n\n",coinChange.getSolution(file.getAbsolutePath()));
    }

    @Test
    public void toExchangeSevenCoinsInThreeTwoFourBill() throws IOException {
        file = createFile("7 3 4 2");
        assertEquals("7 3 4 2\n2 2 3\n3 4\n\n",coinChange.getSolution(file.getAbsolutePath()));
    }

    @Test
    public void toExchangeTwoCoinsInOneBill() throws IOException {
        file = createFile("2 1");
        assertEquals("2 1\n1 1\n\n",coinChange.getSolution(file.getAbsolutePath()));
    }

}