import coinExchange.CoinChange;

public class Runner {
    public static void main(String[] args) {
        CoinChange coinChange = new CoinChange();
        System.out.println(coinChange.getSolution(args[0]));
    }
}
