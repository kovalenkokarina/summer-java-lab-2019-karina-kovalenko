package coinExchange;

import java.util.SortedSet;
import java.util.TreeSet;

public class CoinTask {
    private int sum;
    private SortedSet<Integer> bills;
    private String solution;
    private String initialTask;


    public CoinTask(){
        bills = new TreeSet<>();
    }

    public boolean addBill(int coin){
       return bills.add(coin);
    }

    public int getSum() {
        return sum;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }

    public String getSolution() {
        return solution;
    }

    public void setSolution(String solution) {
        this.solution = solution;
    }

    public SortedSet<Integer> getBills() {
        return bills;
    }

    public void setBills(SortedSet<Integer> bills) {
        this.bills = bills;
    }


    public void setInitialTask(String initialTask) {
        this.initialTask = initialTask;
    }

    @Override
    public String toString() {
        return initialTask + "\n" + solution + "\n\n";
    }
}
