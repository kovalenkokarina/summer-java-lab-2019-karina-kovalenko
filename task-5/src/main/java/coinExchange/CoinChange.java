package coinExchange;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class CoinChange {
    private String result="";
    private final List<CoinTask> taskList;

    public CoinChange(){
        taskList = new ArrayList<>();
    }

    public String getSolution(String filename){
        readFile(filename);
        toExchange();
        StringBuilder stringSolution = new StringBuilder();
        for (CoinTask task:taskList){
            stringSolution.append(task.toString());
        }
        return stringSolution.toString();
    }

    private void readFile(String filename){
        try(BufferedReader br = new BufferedReader(new FileReader(filename))){
            String line;
            while ((line = br.readLine())!=null){
                parseLine(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void parseLine(String line){
        StringTokenizer tokenizer = new StringTokenizer(line," ");
        CoinTask task = new CoinTask();
        task.setInitialTask(line);
        int i=0;
        while (tokenizer.hasMoreTokens()){
            String token = tokenizer.nextToken();
            if(i==0){
                task.setSum(Integer.parseInt(token));
                i++;
            }else {
                if(task.addBill(Integer.parseInt(token))==false) {
                    task.setSolution("Error: Duplicates");
                    break;
                }
            }
        }
        taskList.add(task);
    }

    private boolean checkError(CoinTask task){
        boolean isWrong = true;
        if (task.getBills().isEmpty()) {
            task.setSolution("Error: There are not coins");
            isWrong = false;
        }else {
            if (task.getBills().size() == 1) {
                if (task.getBills().first() == task.getSum()) {
                    task.setSolution("Error: No one way");
                    isWrong = false;
                }
            }
        }
        return isWrong;
    }

    private void toExchange(){
        for(CoinTask task:taskList) {
            if(task.getSolution()==null){
                if (checkError(task)) {
                    int[] bills = task.getBills().stream().mapToInt(i -> i).toArray();
                    calculateVariations(task.getSum(), 0, "", bills);
                    if(result=="") task.setSolution("Error: No one way");
                    else task.setSolution(result.trim());
                    result = "";
                }
            }
        }
    }

    private void calculateVariations(int sum,int startIndex,String s,int [] bills){
        for(int i=startIndex; i< bills.length;i++) {
            int temp = sum - bills[i];
            String tempSolution = s + "" + bills[i] + " ";
            if (temp < 0) {
                break;
            }
            if (temp == 0) {
                result+=tempSolution.trim() + "\n";
                break;
            } else {
                calculateVariations(temp, i,tempSolution, bills);
            }
        }
    }
}
