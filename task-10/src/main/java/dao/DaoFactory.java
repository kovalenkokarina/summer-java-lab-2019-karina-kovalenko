package dao;

import dao.jdbc.*;

public abstract class DaoFactory {
    public abstract JdbcProductDao getProductDao();
    public abstract JdbcDeliveryDao getDeliveryDao();
    public abstract JdbcProviderDao getProviderDao();
    public abstract JdbcWaybillDao getWaybillDao();
    public abstract JdbcSoldProductDao getSoldProductDao();
    public abstract JdbcTypeProductDao getTypeProductDao();
    public static DaoFactory getDaoFactory(){return JdbcDaoFactory.getFactory();}
}
