package dao.jdbc;

import entity.Product;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class JdbcProductDao extends JdbcGenericDao<Product> {

    public static final String PRODUCT_COLUMN_ID = "id";
    public static final String PRODUCT_COLUMN_NAME = "name";
    public static final String PRODUCT_COLUMN_PRICE = "price";
    public static final String PRODUCT_COLUMN_TYPE = "product_type_id";

    @Override
    protected String getSelectQuery() {
        return "SELECT p.*, tp.name AS type_product_name FROM product p LEFT JOIN product_type tp on p.id = tp.id";
    }

    @Override
    protected String getCreateQuery() {
        return "INSERT INTO product (name, price, product_type_id) VALUES (?, ?, ?)";
    }

    @Override
    protected String getDeleteQuery() {
        return "DELETE FROM product WHERE id = ?";
    }

    @Override
    protected String getUpdateQuery() {
        return "UPDATE product SET name = ?, price = ?, product_type_id = ? WHERE id = ?";
    }

    @Override
    protected String getFindById() {
        return getSelectQuery() + " WHERE p.id = ?";
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement statement, Product entity) throws SQLException {
        statement.setInt(4, entity.getId());
        statement.setString(1, entity.getName());
        statement.setBigDecimal(2,entity.getPrice());
        statement.setInt(3, entity.getTypeProduct().getId());
    }

    @Override
    protected void prepareStatementForCreate(PreparedStatement statement, Product entity) throws SQLException {
        statement.setString(2, entity.getName());
        statement.setBigDecimal(3,entity.getPrice());
        statement.setInt(4, entity.getTypeProduct().getId());
    }

    @Override
    protected void prepareStatementForDelete(PreparedStatement statement, Product entity) throws SQLException {
        statement.setInt(1, entity.getId());
    }

    @Override
    protected List<Product> parseResultSet(ResultSet resultSet) throws SQLException {
        List<Product> result = new ArrayList<>();
        while(resultSet.next() ){
            Product product = new Product(){{
                setId(resultSet.getInt(PRODUCT_COLUMN_ID));
                setName(resultSet.getString(PRODUCT_COLUMN_NAME));
                setPrice(resultSet.getBigDecimal(PRODUCT_COLUMN_PRICE));
                setTypeProduct(JdbcDaoFactory.getFactory().getTypeProductDao().findById(resultSet.getInt(PRODUCT_COLUMN_TYPE)));
            }};
            result.add(product);
        }
        return result;
    }
}
