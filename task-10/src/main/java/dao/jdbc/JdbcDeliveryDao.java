package dao.jdbc;

import entity.Delivery;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class JdbcDeliveryDao extends JdbcGenericDao<Delivery> {

    public static final String DELIVERY_COLUMN_ID = "id";
    public static final String DELIVERY_COLUMN_PROVIDER_ID = "provider_id";
    public static final String DELIVERY_COLUMN_DATE = "delivery_date";

    @Override
    protected String getSelectQuery() {
        return "SELECT d.*, p.name AS provider_name FROM delivery d LEFT JOIN provider p ON d.id = p.id";
    }

    @Override
    protected String getCreateQuery() {
        return "INSERT INTO delivery (provider_id, delivery_date) VALUES (?, ?)";
    }

    @Override
    protected String getDeleteQuery() {
        return "DELETE FROM delivery WHERE id = ?";
    }

    @Override
    protected String getUpdateQuery() {
        return "UPDATE delivery SET provider_id = ?, delivery_date = ? WHERE id = ?";
    }

    @Override
    protected String getFindById() {
        return getSelectQuery() + " WHERE d.id = ?";
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement statement, Delivery entity) throws SQLException {
        statement.setInt(3, entity.getId());
        statement.setInt(1,entity.getProvider().getId());
        statement.setDate(2, Date.valueOf(entity.getDateDelivery()));
    }

    @Override
    protected void prepareStatementForCreate(PreparedStatement statement, Delivery entity) throws SQLException {
        statement.setInt(2,entity.getProvider().getId());
        statement.setDate(3, Date.valueOf(entity.getDateDelivery()));
    }

    @Override
    protected void prepareStatementForDelete(PreparedStatement statement, Delivery entity) throws SQLException {
        statement.setInt(1, entity.getId());
    }

    @Override
    protected List<Delivery> parseResultSet(ResultSet resultSet) throws SQLException {
        List<Delivery> result = new ArrayList<>();
        while(resultSet.next() ){
            Delivery delivery = new Delivery(){{
                setId(resultSet.getInt(DELIVERY_COLUMN_ID));
                setProvider(JdbcDaoFactory.getFactory().getProviderDao().findById(resultSet.getInt(DELIVERY_COLUMN_PROVIDER_ID)));
                setDateDelivery(resultSet.getDate(DELIVERY_COLUMN_DATE).toLocalDate());
            }};
            result.add(delivery);
        }
        return result;
    }
}
