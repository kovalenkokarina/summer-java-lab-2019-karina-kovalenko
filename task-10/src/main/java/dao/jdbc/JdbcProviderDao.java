package dao.jdbc;

import entity.Provider;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class JdbcProviderDao extends JdbcGenericDao<Provider> {

    public static final String PROVIDER_COLUMN_ID = "id";
    public static final String PROVIDER_COLUMN_NAME = "name";

    @Override
    protected String getSelectQuery() {
        return "SELECT * FROM provider";
    }

    @Override
    protected String getCreateQuery() {
        return "INSERT INTO provider (name) VALUES (?)";
    }

    @Override
    protected String getDeleteQuery() {
        return "DELETE FROM provider WHERE id = ?";
    }

    @Override
    protected String getUpdateQuery() {
        return "UPDATE provider SET name = ? WHERE id = ?";
    }

    @Override
    protected String getFindById() {
        return getSelectQuery() + " WHERE id = ?";
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement statement, Provider entity) throws SQLException {
        statement.setInt(2,entity.getId());
        statement.setString(1,entity.getName());
    }

    @Override
    protected void prepareStatementForCreate(PreparedStatement statement, Provider entity) throws SQLException {
        statement.setString(2,entity.getName());
    }

    @Override
    protected void prepareStatementForDelete(PreparedStatement statement, Provider entity) throws SQLException {
        statement.setInt(1,entity.getId());
    }

    @Override
    protected List<Provider> parseResultSet(ResultSet resultSet) throws SQLException {
        List<Provider> result = new ArrayList<>();
        while(resultSet.next() ){
            Provider provider = new Provider(){{
                setId(resultSet.getInt(PROVIDER_COLUMN_ID));
                setName(resultSet.getString(PROVIDER_COLUMN_NAME));
            }};
            result.add(provider);
        }
        return result;
    }
}
