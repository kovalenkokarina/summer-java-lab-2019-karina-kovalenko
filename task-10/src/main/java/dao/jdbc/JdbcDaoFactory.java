package dao.jdbc;

import dao.DaoFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JdbcDaoFactory extends DaoFactory {

    private static JdbcDaoFactory instance;

    private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    private static final String DATABASE_URL = "jdbc:mysql://localhost:8083/shop?useUnicode=true&serverTimezone=UTC&useSSL=true&verifyServerCertificate=false";
    private static final String DATABASE_USER = "root";
    private static final String DATABASE_PASSWORD = "root";

    public static JdbcDaoFactory getFactory(){
        return instance == null ? new JdbcDaoFactory() : instance;
    }

    public static Connection getConnection() throws SQLException {
        try {
            Class.forName(JDBC_DRIVER);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return DriverManager.getConnection(DATABASE_URL,DATABASE_USER, DATABASE_PASSWORD);
    }

    public JdbcProductDao getProductDao() {
        return new JdbcProductDao();
    }

    public JdbcDeliveryDao getDeliveryDao() {
        return new JdbcDeliveryDao();
    }

    public JdbcProviderDao getProviderDao() { return new JdbcProviderDao(); }

    public JdbcWaybillDao getWaybillDao() {
        return new JdbcWaybillDao();
    }

    public JdbcSoldProductDao getSoldProductDao() {
        return new JdbcSoldProductDao();
    }

    public JdbcTypeProductDao getTypeProductDao() {
        return new JdbcTypeProductDao();
    }
}
