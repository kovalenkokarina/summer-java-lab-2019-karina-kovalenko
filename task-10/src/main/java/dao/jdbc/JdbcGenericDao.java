package dao.jdbc;

import dao.GenericDao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public abstract class JdbcGenericDao<T> implements GenericDao<T> {

    public JdbcGenericDao(){
    }

    protected abstract  String getSelectQuery();

    protected abstract  String getCreateQuery();

    protected abstract String getDeleteQuery();

    protected abstract String getUpdateQuery();

    protected abstract String getFindById();

    protected abstract void prepareStatementForUpdate(PreparedStatement statement, T entity) throws SQLException;

    protected abstract void prepareStatementForCreate(PreparedStatement statement, T entity) throws SQLException;

    protected abstract void prepareStatementForDelete(PreparedStatement statement, T entity) throws SQLException;

    protected abstract List<T> parseResultSet(ResultSet resultSet) throws SQLException;

    @Override
    public void create(T entity) {
        String sql = getCreateQuery();
        try(Connection connection = JdbcDaoFactory.getConnection();
            PreparedStatement statement = connection.prepareStatement(sql)){
            prepareStatementForCreate(statement,entity);
            if(statement.executeUpdate()>1){
                System.out.println("Добавление произошло успешно");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(T entity) {
        String sql = getUpdateQuery();
        try(Connection connection = JdbcDaoFactory.getConnection();
            PreparedStatement statement = connection.prepareStatement(sql)){
            prepareStatementForUpdate(statement, entity);
            if(statement.executeUpdate()>1){
                System.out.println("Обновление произошло успешно");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(T entity) {
        String sql = getDeleteQuery();
        try(Connection connection = JdbcDaoFactory.getConnection();
            PreparedStatement statement = connection.prepareStatement(sql)){
            prepareStatementForDelete(statement, entity);
            if(statement.executeUpdate()>1){
                System.out.println("Удаление произошло успешно");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public T findById(int id) {
        List<T> list = null;
        String sql = getFindById();
        try(Connection connection = JdbcDaoFactory.getConnection();
            PreparedStatement statement = connection.prepareStatement(sql)){
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            list = parseResultSet(resultSet);
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        if(list.isEmpty() || list.size()==0){
            return null;
        }
        return  list.iterator().next();
    }

    @Override
    public List<T> findAll() {
        List<T> list = null;
        String sql = getSelectQuery();
        try(Connection connection = JdbcDaoFactory.getConnection();
            PreparedStatement statement = connection.prepareStatement(sql)){
            ResultSet resultSet = statement.executeQuery();
            list = parseResultSet(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }
}
