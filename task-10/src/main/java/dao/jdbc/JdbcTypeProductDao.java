package dao.jdbc;

import entity.TypeProduct;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class JdbcTypeProductDao extends JdbcGenericDao<TypeProduct> {

    public static final String TYPE_PRODUCT_COLUMN_ID = "id";
    public static final String TYPE_PRODUCT_COLUMN_NAME = "name";

    @Override
    protected String getSelectQuery() {
        return "SELECT * FROM product_type";
    }

    @Override
    protected String getCreateQuery() {
        return "INSERT INTO product_type (name) VALUES (?)";
    }

    @Override
    protected String getDeleteQuery() {
        return "DELETE FROM product_type WHERE id = ?";
    }

    @Override
    protected String getUpdateQuery() {
        return "UPDATE product_type SET name = ? WHERE id = ?";
    }

    @Override
    protected String getFindById() {
        return getSelectQuery() + " WHERE id = ?";
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement statement, TypeProduct entity) throws SQLException {
        statement.setInt(2, entity.getId());
        statement.setString(1, entity.getName());
    }

    @Override
    protected void prepareStatementForCreate(PreparedStatement statement, TypeProduct entity) throws SQLException {
        statement.setString(1, entity.getName());
    }

    @Override
    protected void prepareStatementForDelete(PreparedStatement statement, TypeProduct entity) throws SQLException {
        statement.setInt(1,entity.getId());
    }

    @Override
    protected List<TypeProduct> parseResultSet(ResultSet resultSet) throws SQLException {
        List<TypeProduct> result = new ArrayList<>();
        while(resultSet.next() ){
            TypeProduct typeProduct = new TypeProduct(){{
                setId(resultSet.getInt(TYPE_PRODUCT_COLUMN_ID));
                setName(resultSet.getString(TYPE_PRODUCT_COLUMN_NAME));
            }};
            result.add(typeProduct);
        }
        return result;
    }
}
