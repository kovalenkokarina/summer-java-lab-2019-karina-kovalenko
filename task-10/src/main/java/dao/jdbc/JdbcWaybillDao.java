package dao.jdbc;

import entity.Waybill;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class JdbcWaybillDao extends JdbcGenericDao<Waybill> {

    public static final String WAYBILL_COLUMN_ID = "id";
    public static final String WAYBILL_COLUMN_PRODUCT_ID = "product_id";
    public static final String WAYBILL_COLUMN_COST = "cost";
    public static final String WAYBILL_COLUMN_DELIVERY_ID = "delivery_id";
    public static final String WAYBILL_COLUMN_AMOUNT = "amount";


    @Override
    protected String getSelectQuery() {
        return "SELECT w.* FROM waybill w LEFT JOIN product p ON w.id = p.id LEFT JOIN delivery d ON w.id = d.id";
    }

    @Override
    protected String getCreateQuery() {
        return "INSERT INTO waybill (product_id, delivery_id, cost, amount) VALUES (?, ?, ?, ?)";
    }

    @Override
    protected String getDeleteQuery() {
        return "UPDATE waybill SET product_id = ?, delivery_id = ?, cost = ?, amount = ? WHERE id = ?";
    }

    @Override
    protected String getUpdateQuery() {
        return "DELETE FROM waybill WHERE id = ?";
    }

    @Override
    protected String getFindById() {
        return getSelectQuery() + " WHERE w.id = ?";
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement statement, Waybill entity) throws SQLException {
        statement.setInt(5,entity.getId());
        statement.setInt(1,entity.getProduct().getId());
        statement.setInt(2,entity.getDelivery().getId());
        statement.setBigDecimal(3,entity.getCost());
        statement.setInt(4,entity.getAmount());
    }

    @Override
    protected void prepareStatementForCreate(PreparedStatement statement, Waybill entity) throws SQLException {
        statement.setInt(1,entity.getProduct().getId());
        statement.setInt(2,entity.getDelivery().getId());
        statement.setBigDecimal(3,entity.getCost());
        statement.setInt(4,entity.getAmount());
    }

    @Override
    protected void prepareStatementForDelete(PreparedStatement statement, Waybill entity) throws SQLException {
        statement.setInt(1,entity.getId());
    }

    @Override
    protected List<Waybill> parseResultSet(ResultSet resultSet) throws SQLException {
        List<Waybill> result = new ArrayList<>();
        while(resultSet.next() ){
            Waybill waybill = new Waybill(){{
                setId(resultSet.getInt(WAYBILL_COLUMN_ID));
                setAmount(resultSet.getInt(WAYBILL_COLUMN_AMOUNT));
                setCost(resultSet.getBigDecimal(WAYBILL_COLUMN_COST));
                setDelivery(JdbcDaoFactory.getFactory().getDeliveryDao().findById(resultSet.getInt(WAYBILL_COLUMN_DELIVERY_ID)));
                setProduct(JdbcDaoFactory.getFactory().getProductDao().findById(resultSet.getInt(WAYBILL_COLUMN_PRODUCT_ID)));
            }};
            result.add(waybill);
        }
        return result;
    }
}
