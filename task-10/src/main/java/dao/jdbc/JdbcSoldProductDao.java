package dao.jdbc;

import entity.SoldProduct;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class JdbcSoldProductDao extends JdbcGenericDao<SoldProduct> {

    public static final String SOLD_PRODUCT_COLUMN_ID = "id";
    public static final String SOLD_PRODUCT_COLUMN_PRODUCT_ID = "product_id";
    public static final String SOLD_PRODUCT_COLUMN_AMOUNT = "amount";

    @Override
    protected String getSelectQuery() {
        return "SELECT sp.* FROM sold_product sp LEFT JOIN product p ON sp.id = p.id";
    }

    @Override
    protected String getCreateQuery() {
        return "INSERT INTO sold_product (product_id, amount) VALUES (?, ?)";
    }

    @Override
    protected String getDeleteQuery() {
        return "UPDATE sold_product SET product_id = ?, amount = ? WHERE id = ?";
    }

    @Override
    protected String getUpdateQuery() {
        return "DELETE FROM sold_product WHERE id = ?";
    }

    @Override
    protected String getFindById() {
        return getSelectQuery() + " WHERE sp.id = ?";
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement statement, SoldProduct entity) throws SQLException {
        statement.setInt(3,entity.getId());
        statement.setInt(1,entity.getProduct().getId());
        statement.setInt(2,entity.getAmount());
    }

    @Override
    protected void prepareStatementForCreate(PreparedStatement statement, SoldProduct entity) throws SQLException {
        statement.setInt(2,entity.getProduct().getId());
        statement.setInt(3,entity.getAmount());
    }

    @Override
    protected void prepareStatementForDelete(PreparedStatement statement, SoldProduct entity) throws SQLException {
        statement.setInt(1,entity.getId());
    }

    @Override
    protected List<SoldProduct> parseResultSet(ResultSet resultSet) throws SQLException {
        List<SoldProduct> result = new ArrayList<>();
        while(resultSet.next() ){
            SoldProduct soldProduct = new SoldProduct(){{
                setId(resultSet.getInt(SOLD_PRODUCT_COLUMN_ID));
                setAmount(resultSet.getInt(SOLD_PRODUCT_COLUMN_AMOUNT));
                setProduct(JdbcDaoFactory.getFactory().getProductDao().findById(resultSet.getInt(SOLD_PRODUCT_COLUMN_PRODUCT_ID)));
            }};
            result.add(soldProduct);
        }
        return result;
    }
}
