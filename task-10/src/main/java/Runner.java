import dao.DaoFactory;
import entity.TypeProduct;

public class Runner {
    public static void main(String[] args) {
        DaoFactory daoFactory = DaoFactory.getDaoFactory();

        System.out.println("Waybill table");
        StringBuilder table = new StringBuilder();
        table.append(String.format("%-3s","id")).append("|");
        table.append(String.format("%-20s","name product")).append("|");
        table.append(String.format("%-20s","price product")).append("|");
        table.append(String.format("%-20s","type product")).append("|");
        table.append(String.format("%-20s","provider")).append("|");
        table.append(String.format("%-15s","delivery date")).append("|");
        table.append(String.format("%-15s","cost")).append("|");
        table.append(String.format("%-15s","amount")).append("|").append("\n");

        daoFactory.getWaybillDao().findAll().forEach(waybill -> table.append(waybill.toString()));
        System.out.println(table.toString());

    }
}
