package entity;

import java.time.LocalDate;

public class Delivery {
    private int id;
    private Provider provider;
    private LocalDate dateDelivery;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDate getDateDelivery() {
        return dateDelivery;
    }

    public void setDateDelivery(LocalDate dateDelivery) {
        this.dateDelivery = dateDelivery;
    }

    public Provider getProvider() {
        return provider;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    @Override
    public String toString() {
        StringBuilder table = new StringBuilder();
        table.append(String.format("%-3s", id)).append("|");
        table.append(String.format("%-20s",provider.getName())).append("|");
        table.append(String.format("%-20s",dateDelivery)).append("|");
        table.append("\n");
        return table.toString();
    }
}
