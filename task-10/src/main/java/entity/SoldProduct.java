package entity;

import java.math.BigDecimal;

public class SoldProduct {
    private int id;
    private Product product;
    private int amount;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        StringBuilder table = new StringBuilder();
        table.append(String.format("%-3s", id)).append("|");
        table.append(String.format("%-20s",product.getName())).append("|");
        table.append(String.format("%-20s",product.getPrice())).append("|");
        table.append(String.format("%-20s", product.getTypeProduct().getName())).append("|");
        table.append(String.format("%-20s", amount)).append("|");
        table.append("\n");
        return table.toString();
    }
}
