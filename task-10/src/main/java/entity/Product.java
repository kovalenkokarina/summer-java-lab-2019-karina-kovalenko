package entity;

import java.math.BigDecimal;

public class Product {
    private int id;
    private String name;
    private BigDecimal price;
    private TypeProduct typeProduct;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public TypeProduct getTypeProduct() {
        return typeProduct;
    }

    public void setTypeProduct(TypeProduct typeProduct) {
        this.typeProduct = typeProduct;
    }

    @Override
    public String toString() {
        StringBuilder table = new StringBuilder();
        table.append(String.format("%-3s", id)).append("|");
        table.append(String.format("%-20s",name)).append("|");
        table.append(String.format("%-20s",price)).append("|");
        table.append(String.format("%-20s", typeProduct.getName())).append("|");
        table.append("\n");
        return table.toString();
    }
}
