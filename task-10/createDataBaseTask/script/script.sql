drop database if exists shop;
create database shop;
create table shop.product_type(
	id int not null auto_increment,
	name varchar(255) not null,
    primary key (id)
);

create table shop.provider(
	id int auto_increment not null,
    name varchar(255) not null,
    primary key(id)
);

create table shop.product(
	id int auto_increment not null,
    name varchar(255) not null,
    price decimal(18,2),
    product_type_id int,
    primary key (id),
    foreign key (product_type_id) references shop.product_type(id)
);

create table shop.sold_product(
	id int not null auto_increment,
    product_id int,
    amount int,
    primary key(id),
    foreign key (product_id) references shop.product(id)
);

create table shop.delivery(
	id int not null auto_increment,
    provider_id int,
    delivery_date Date not null,
    primary key(id),
    foreign key (provider_id) references shop.provider(id)
);

create table shop.waybill(
	id int not null auto_increment,
    product_id int,
    delivery_id int,
	cost decimal(18,2),
	amount int, 
    primary key(id),
    foreign key (product_id) references shop.product(id),
    foreign key (delivery_id) references shop.provider(id)
);

insert into shop.product_type (name) value ("Phone"); 
insert into shop.product_type (name) value ("Laptop"); 
insert into shop.product_type (name) value ("Computer"); 
insert into shop.product_type (name) value ("Clock"); 
insert into shop.product_type (name) value ("Electric scooter"); 

insert into shop.product (name, price, product_type_id) value ("IPhone SE", 800, 1);
insert into shop.product (name, price, product_type_id) value ("Sumsung", 1700, 1);
insert into shop.product (name, price, product_type_id) value ("HP 250", 1000, 2);
insert into shop.product (name, price, product_type_id) value ("ZALMAN A1", 1500, 3);
insert into shop.product (name, price, product_type_id) value ("Apple Watch 4", 899, 4);
insert into shop.product (name, price, product_type_id) value ("Xiaomi Mijia Smart", 699, 5);
insert into shop.product (name, price, product_type_id) value ("Kugoo S3", 389, 5);

insert into shop.provider (name) value ("Apple");
insert into shop.provider (name) value ("Sumsung");
insert into shop.provider (name) value ("Xiaomi");
insert into shop.provider (name) value ("HP");
insert into shop.provider (name) value ("ZALMAN");

insert into shop.delivery (provider_id, delivery_date) value (1, '2010-01-01');
insert into shop.delivery (provider_id, delivery_date) value (2, '2019-02-02');
insert into shop.delivery (provider_id, delivery_date) value (3, '2019-03-03');
insert into shop.delivery (provider_id, delivery_date) value (4, '2019-04-04');
insert into shop.delivery (provider_id, delivery_date) value (5, '2019-05-05');

insert into shop.waybill (product_id, delivery_id, amount, cost) value (1,1,100,80000);
insert into shop.waybill (product_id, delivery_id, amount, cost) value (2,2,10,17000);
insert into shop.waybill (product_id, delivery_id, amount, cost) value (3,4,24,20000);
insert into shop.waybill (product_id, delivery_id, amount, cost) value (4,5,1,1200);
insert into shop.waybill (product_id, delivery_id, amount, cost) value (5,1,98,14600);

insert into shop.sold_product (product_id, amount) value (1,20);
insert into shop.sold_product (product_id, amount) value (2,5);
insert into shop.sold_product (product_id, amount) value (3,23);
insert into shop.sold_product (product_id, amount) value (4,2);
insert into shop.sold_product (product_id, amount) value (5,20);
insert into shop.sold_product (product_id, amount) value (6,20);

