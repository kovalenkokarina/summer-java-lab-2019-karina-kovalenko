import entities.Client;
import menu.decor.*;
import menu.drinks.cold.Fanta;
import menu.drinks.hot.Americano;
import menu.food.Potato;
import storage.Storage;
import entities.Menu;
import entities.Order;

public class Runner {
    public static void main(String[] args) {
        Client.Builder client = Client.newBuilder();
        client.setName("Joi");
        client.setSurname("Tribbiani");
        client.setCity("New-York");
        client.setStreet("Bedford");
        client.setNumberFlat(19);
        client.setHouse("90");

        Order order = new Order();//created a order
        order.setClient(client.build());//created a client and added to the order

        Menu menu = new Potato();
        menu = new Beef(menu);
        menu = new Ketchup(menu);//chose food

        order.setMenu(menu);// added food to the order

        menu = new Americano();
        menu = new Sugar(menu);
        menu = new Chocolate(menu);

        order.setMenu(menu);

        System.out.println("Bill: " + order.toString());

        Storage storage = new Storage();
        storage.setSave(order.save());// save the order to the storage

        order.load(storage.getSave());//order recovery

        menu = new Fanta();

        order.setMenu(menu);//added Fanta to the order

        System.out.println("\nBill: " + order.toString());
    }

}
