package entities;

import java.math.BigDecimal;

public abstract class Menu implements Cloneable{
    private String name;

    protected Menu(){}

    public Menu(String name) {
        this.name = name;
    }

    public abstract BigDecimal getCost();

    public String getName(){
        return name;
    }

    protected void setName(String name){
        this.name = name;
    }
}
