package entities;

import storage.Save;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Order {
    private List<Menu> menuList = new ArrayList<>();
    private Menu menu;
    private Client client;
    private BigDecimal totalCost = new BigDecimal(0);

    public void setClient(Client client){
        this.client = client;
    }

    public void setMenu(Menu menu){
        this.menu = menu;
        menuList.add(menu);
    }

    public Client getClient(){
        return client;
    }

    public Menu getMenu(){
        return menu;
    }

    public List<Menu> getMenuList(){
        return menuList;
    }

    public void load(Save save){
        client = save.getClient();
        menuList = new ArrayList<>(save.getMenuList());
    }

    public Save save(){
        List<Menu> listMenu = new ArrayList<>(menuList);
        return new Save(client,listMenu);
    }

    private BigDecimal toCalculateAmount(){
        BigDecimal sum = new BigDecimal(0);
        for (Menu menu: menuList){
            sum = sum.add(menu.getCost());
        }
        return sum;
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder(client.toString() + "\nOrder:\n");
        for (Menu m: menuList)
            str.append(m.getName()).append(" - ").append(m.getCost()).append(" BYR\n");
        str.append("Total cost: ").append(toCalculateAmount()).append(" BYR");
        return str.toString();
    }

}
