package entities;

import java.util.Objects;

public class Client {
    private String name;
    private String surname;
    private String patronymic;
    private String street;
    private String city;
    private String mobilePhone;
    private String homePhone;
    private String house;
    private int numberFlat;

    public static Builder newBuilder(){
        return new Client().new Builder();
    }

    public class Builder{
        private final Client newClient;

        Builder(){
            newClient = new Client();
        }

        public Builder setName(String name){
            newClient.name = name;
            return this;
        }

        public Builder setSurname(String surname){
            newClient.surname = surname;
            return this;
        }

        public Builder setPatronymic(String patronymic){
            newClient.patronymic = patronymic;
            return this;
        }

        public Builder setCity(String city){
            newClient.city = city;
            return this;
        }

        public Builder setStreet(String street){
            newClient.street = street;
            return this;
        }

        public Builder setHouse(String house){
            newClient.house = house;
            return this;
        }

        public Builder setNumberFlat(int numberFlat){
            newClient.numberFlat = numberFlat;
            return this;
        }

        public Builder setMobilePhone(String mobilePhone){
            newClient.mobilePhone = mobilePhone;
            return this;
        }

        public Builder setHomePhone(String homePhone){
            newClient.homePhone = homePhone;
            return this;
        }

        public Client build(){
            return newClient;
        }
    }

    @Override
    public String toString() {
        return Objects.toString(surname,"") + " " + Objects.toString(name,"") + " " + Objects.toString(patronymic,"")
                + "\nAddress: "+ Objects.toString(city,"") + " " + Objects.toString(city,"") + " " + Objects.toString(house,"")
                + "" + Objects.toString(numberFlat,"") + "\nMobile phone: " + Objects.toString(mobilePhone,"")
                + "\nHome phone: " + Objects.toString(homePhone,"");
    }
}
