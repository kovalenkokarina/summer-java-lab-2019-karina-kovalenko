package storage;
import entities.Client;
import entities.Menu;

import java.util.ArrayList;
import java.util.List;

public class Save {
    private final Client client;
    private final List<Menu> menuList;

    public Save(Client client,List<Menu> menuList) {
        this.client = client;
        this.menuList = new ArrayList<>(menuList);
    }

    public Client getClient() {
        return client;
    }

    public List<Menu> getMenuList() {
        return menuList;
    }


}

