package storage;

import storage.Save;

import java.util.LinkedList;
import java.util.NoSuchElementException;

public class Storage {
    private LinkedList<Save> saves = new LinkedList<>();

    public Save getSave(){
        return saves.getLast();
    }

    public void setSave(Save save){
        saves.add(save);
    }
}
