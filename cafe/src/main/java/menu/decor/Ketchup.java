package menu.decor;

import entities.Menu;
import menu.Decorator;

import java.math.BigDecimal;

public class Ketchup extends Decorator {
    public Ketchup(Menu menu) {
        super(menu);
    }

    @Override
    public BigDecimal getCost() {
        return super.menu.getCost().add(BigDecimal.valueOf(0.4));
    }

    @Override
    public String getName() {
        return this.menu.getName() + ", ketchup";
    }
}
