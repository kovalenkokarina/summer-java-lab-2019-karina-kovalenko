package menu.decor;

import entities.Menu;
import menu.Decorator;

import java.math.BigDecimal;

public class Tomato extends Decorator {
    public Tomato(Menu menu) {
        super(menu);
    }

    @Override
    public BigDecimal getCost() {
        return super.menu.getCost().add(BigDecimal.valueOf(0.8));
    }

    @Override
    public String getName() {
        return this.menu.getName() + ", tomato";
    }
}
