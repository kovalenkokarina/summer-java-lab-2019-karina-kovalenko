package menu.decor;

import entities.Menu;
import menu.Decorator;

import java.math.BigDecimal;

public class Ice extends Decorator {

    public Ice(Menu menu) {
        super(menu);
    }

    public BigDecimal getCost() {
        return super.menu.getCost().add(BigDecimal.valueOf(0.1));
    }

    public String getName() {
        return super.menu.getName() + ", ice";
    }
}
