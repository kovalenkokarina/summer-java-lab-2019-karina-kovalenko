package menu.decor;

import entities.Menu;
import menu.Decorator;

import java.math.BigDecimal;

public class Cinnamon extends Decorator {
    public Cinnamon(Menu menu){
        super(menu);
    }
    public BigDecimal getCost() {
        return super.menu.getCost().add(BigDecimal.valueOf(0.15));
    }

    public String getName() {
        return super.menu.getName() + ", cinnamon";
    }
}
