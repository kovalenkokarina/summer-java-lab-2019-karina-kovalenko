package menu.decor;

import entities.Menu;
import menu.Decorator;

import java.math.BigDecimal;

public class Chicken extends Decorator {

    public Chicken(Menu menu) {
        super(menu);
    }

    @Override
    public BigDecimal getCost() {
        return super.menu.getCost().add(BigDecimal.valueOf(2.45));
    }

    @Override
    public String getName() {
        return this.menu.getName() + ", chicken";
    }
}
