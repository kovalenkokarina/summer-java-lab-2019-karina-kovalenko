package menu.decor;

import entities.Menu;
import menu.Decorator;

import java.math.BigDecimal;

public class Syrup extends Decorator {
    public Syrup(Menu menu){
        super(menu);
    }
    public BigDecimal getCost() {
        return super.menu.getCost().add(BigDecimal.valueOf(0.35));
    }

    public String getName() {
        return super.menu.getName() + ", any syrup";
    }
}
