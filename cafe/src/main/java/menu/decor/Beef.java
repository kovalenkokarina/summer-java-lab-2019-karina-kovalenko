package menu.decor;

import entities.Menu;
import menu.Decorator;

import java.math.BigDecimal;

public class Beef extends Decorator {

    public Beef(Menu menu) {
        super(menu);
    }

    @Override
    public BigDecimal getCost() {
        return super.menu.getCost().add(BigDecimal.valueOf(3.5));
    }

    @Override
    public String getName() {
        return this.menu.getName() + ", beef";
    }
}
