package menu.decor;

import entities.Menu;
import menu.Decorator;

import java.math.BigDecimal;

public class Mushrooms extends Decorator {
    public Mushrooms(Menu menu) {
        super(menu);
    }

    @Override
    public BigDecimal getCost() {
        return super.menu.getCost().add(BigDecimal.valueOf(0.75));
    }

    @Override
    public String getName() {
        return this.menu.getName() + ", mushrooms";
    }
}
