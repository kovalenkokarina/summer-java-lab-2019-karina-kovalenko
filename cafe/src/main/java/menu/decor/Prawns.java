package menu.decor;

import entities.Menu;
import menu.Decorator;

import java.math.BigDecimal;

public class Prawns extends Decorator {
    public Prawns(Menu menu) {
        super(menu);
    }

    @Override
    public BigDecimal getCost() {
        return super.menu.getCost().add(BigDecimal.valueOf(2.46));
    }

    @Override
    public String getName() {
        return this.menu.getName() + ", prawns";
    }
}
