package menu.decor;

import entities.Menu;
import menu.Decorator;

import java.math.BigDecimal;

public class Sugar extends Decorator {

    public Sugar(Menu menu){
        super(menu);
    }
    public BigDecimal getCost() {
        return super.menu.getCost().add(BigDecimal.valueOf(0.16));
    }

    public String getName() {
        return super.menu.getName() + ", sugar";
    }
}
