package menu.decor;

import entities.Menu;
import menu.Decorator;

import java.math.BigDecimal;

public class Chocolate extends Decorator {

    public Chocolate(Menu menu) {
        super(menu);
    }

    public BigDecimal getCost() {
        return super.menu.getCost().add(BigDecimal.valueOf(0.12));
    }

    public String getName() {
        return super.menu.getName() + ", chocolate";
    }
}
