package menu;
import entities.Menu;

public abstract class Decorator extends Menu {
    protected Menu menu;

    protected Decorator(Menu menu){
        this.menu = menu;
    }

    public abstract String getName();
}
