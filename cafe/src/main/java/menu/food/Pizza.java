package menu.food;

import entities.Menu;

import java.math.BigDecimal;

public class Pizza extends Menu {

    public Pizza(){
        setName("Pizza");
    }

    @Override
    public BigDecimal getCost() {
        return BigDecimal.valueOf(8);
    }
}
