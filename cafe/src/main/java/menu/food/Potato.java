package menu.food;

import entities.Menu;

import java.math.BigDecimal;

public class Potato extends Menu {

    public Potato(){
        setName("Potato");
    }

    @Override
    public BigDecimal getCost() {
        return BigDecimal.valueOf(3);
    }
}
