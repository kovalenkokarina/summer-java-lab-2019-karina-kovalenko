package menu.food;

import entities.Menu;

import java.math.BigDecimal;

public class Pasta extends Menu {

    public Pasta(){
        setName("Pasta");
    }

    @Override
    public BigDecimal getCost() {
        return BigDecimal.valueOf(5);
    }
}
