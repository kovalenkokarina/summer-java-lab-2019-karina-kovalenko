package menu.drinks.hot;

import entities.Menu;

import java.math.BigDecimal;

public class Tea extends Menu {

    public Tea(){
        setName("Tea");
    }

    @Override
    public BigDecimal getCost() {
        return BigDecimal.valueOf(0.6);
    }
}
