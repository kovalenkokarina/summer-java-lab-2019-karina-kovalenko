package menu.drinks.hot;

import entities.Menu;

import java.math.BigDecimal;

public class Latte  extends Menu {

    public Latte(){
        setName("Latte");
    }

    @Override
    public BigDecimal getCost() {
        return BigDecimal.valueOf(1.45);
    }
}
