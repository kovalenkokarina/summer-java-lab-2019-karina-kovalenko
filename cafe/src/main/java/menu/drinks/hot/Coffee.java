package menu.drinks.hot;

import entities.Menu;

import java.math.BigDecimal;

public class Coffee extends Menu {

    public Coffee(){
        setName("Coffee");
    }

    @Override
    public BigDecimal getCost() {
        return BigDecimal.valueOf(1);    }
}
