package menu.drinks.hot;

import entities.Menu;

import java.math.BigDecimal;

public class Espresso extends Menu {

    public Espresso(){
        setName("Espresso");
    }

    @Override
    public BigDecimal getCost() {
        return BigDecimal.valueOf(1.15);
    }

}
