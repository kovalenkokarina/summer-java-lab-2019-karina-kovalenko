package menu.drinks.hot;
import entities.Menu;

import java.math.BigDecimal;

public class Cappuccino extends Menu {

    public Cappuccino(){
        setName("Cappuccino");
    }

    @Override
    public BigDecimal getCost() { return BigDecimal.valueOf(1.4); }
}
