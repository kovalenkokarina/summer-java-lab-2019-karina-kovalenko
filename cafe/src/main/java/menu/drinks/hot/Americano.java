package menu.drinks.hot;

import entities.Menu;

import java.math.BigDecimal;

public class Americano extends Menu {

    public Americano(){
        setName("Americano");
    }

    public BigDecimal getCost() {
        return BigDecimal.valueOf(1.4);
    }}
