package menu.drinks.hot;

import entities.Menu;

import java.math.BigDecimal;

public class Mocaccino extends Menu {

    public Mocaccino(){
        setName("Mocaccino");
    }

    @Override
    public BigDecimal getCost() {
        return BigDecimal.valueOf(1.45);
    }
}
