package menu.drinks.cold;

import entities.Menu;

import java.math.BigDecimal;

public class Sprite extends Menu {

    public Sprite(){
        setName("Sprite");
    }

    public BigDecimal getCost() {
        return BigDecimal.valueOf(1.22);
    }
}
