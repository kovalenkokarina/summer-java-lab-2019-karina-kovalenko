package menu.drinks.cold;

import entities.Menu;

import java.math.BigDecimal;

public class ColaCola extends Menu {

    public ColaCola() {
        setName("Coca-Cola");
    }

    public BigDecimal getCost() {
        return BigDecimal.valueOf(1.22);
    }
}


