package menu.drinks.cold;

import entities.Menu;

import java.math.BigDecimal;

public class BonAqua extends Menu {

    public BonAqua(){
        setName("Water BonAqua");
    }

    public BigDecimal getCost() {
        return BigDecimal.valueOf(1);
    }
}
