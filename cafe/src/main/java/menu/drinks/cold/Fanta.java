package menu.drinks.cold;

import entities.Menu;

import java.math.BigDecimal;

public class Fanta extends Menu {

    public Fanta(){
        setName("Fanta");
    }
    public BigDecimal getCost() {
        return BigDecimal.valueOf(1.22);
    }
}
