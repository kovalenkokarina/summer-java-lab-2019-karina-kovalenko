drop database if exists user_db;
create database user_db;

create table user_db.user(
    `id` int not null auto_increment,
    `name` varchar(50) default null,
    `age` int default null,
    primary key (`id`)
);

insert into user_db.user (name, age) value("Karina",20);
insert into user_db.user (name, age) value("Dmitry",20);
insert into user_db.user (name, age) value("Zenya",22);
insert into user_db.user (name, age) value("Yana",20);
insert into user_db.user (name, age) value("Vika",18);
insert into user_db.user (name, age) value("Alex",20);
insert into user_db.user (name, age) value("Maxim",21);
insert into user_db.user (name, age) value("Masha",20);