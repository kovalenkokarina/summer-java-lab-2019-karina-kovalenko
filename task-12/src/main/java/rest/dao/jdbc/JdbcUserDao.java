package rest.dao.jdbc;

import rest.entity.User;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class JdbcUserDao extends JdbcGenericDao<User>{

    public JdbcUserDao(){}

    public static final String USER_COLUMN_ID = "id";
    public static final String USER_COLUMN_NAME = "name";
    public static final String USER_COLUMN_AGE = "age";

    @Override
    protected String getSelectQuery() {
        return "SELECT * from user";
    }

    @Override
    protected String getCreateQuery() { return "INSERT INTO user (name, age) VALUES (?, ?)"; }

    @Override
    protected String getDeleteQuery() {
        return "DELETE FROM user WHERE id = ?";
    }

    @Override
    protected String getUpdateQuery() {
        return "UPDATE user SET name = ?, age = ? WHERE id = ?";
    }

    @Override
    protected String getFindById() {
        return getSelectQuery() + " WHERE id = ?";
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement statement, User entity) throws SQLException {
        statement.setInt(3, entity.getId());
        statement.setString(1,entity.getName());
        statement.setInt(2, entity.getAge());
    }

    @Override
    protected void prepareStatementForCreate(PreparedStatement statement, User entity) throws SQLException {
        statement.setString(1,entity.getName());
        statement.setInt(2, entity.getAge());
    }

    @Override
    protected void prepareStatementForDelete(PreparedStatement statement, User entity) throws SQLException {
        statement.setInt(1, entity.getId());
    }

    @Override
    protected List<User> parseResultSet(ResultSet resultSet) throws SQLException {
        List<User> result = new ArrayList<>();
        while(resultSet.next() ){
            User user = new User(){{
                setId(resultSet.getInt(USER_COLUMN_ID));
                setName(resultSet.getString(USER_COLUMN_NAME));
                setAge(resultSet.getInt(USER_COLUMN_AGE));
            }};
            result.add(user);
        }
        return result;
    }
}
