package rest.dao.jdbc;

import rest.dao.DaoFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JdbcDaoFactory extends DaoFactory {

    private static JdbcDaoFactory instance;

    private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    private static final String DATABASE_URL = "jdbc:mysql://host.docker.internal:3306/user_db?useSSL=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private static final String DATABASE_USER = "root";
    private static final String DATABASE_PASSWORD = "root";

    public static JdbcDaoFactory getFactory(){
        return instance == null ? new JdbcDaoFactory() : instance;
    }

    public static Connection getConnection() throws SQLException {
        try{
            Class.forName(JDBC_DRIVER);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return DriverManager.getConnection(DATABASE_URL,DATABASE_USER, DATABASE_PASSWORD);
    }

    public JdbcUserDao getUserDao() {
        return new JdbcUserDao();
    }
}
