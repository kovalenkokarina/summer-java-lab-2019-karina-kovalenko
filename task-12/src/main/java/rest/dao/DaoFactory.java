package rest.dao;

import rest.dao.jdbc.JdbcDaoFactory;
import rest.dao.jdbc.JdbcUserDao;

public abstract class DaoFactory {
    public abstract JdbcUserDao getUserDao();

    public static DaoFactory getDaoFactory(){
        return JdbcDaoFactory.getFactory();
    }
}
