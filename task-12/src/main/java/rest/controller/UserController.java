package rest.controller;

import org.springframework.web.bind.annotation.*;
import rest.dao.DaoFactory;
import rest.entity.User;
import rest.exception.UserNotFoundException;

import java.util.List;
import java.util.Optional;

@RestController
public class UserController {

    private DaoFactory daoFactory = DaoFactory.getDaoFactory();

    @PostMapping("/user")
    public User createUser(@RequestBody User user){
        daoFactory.getUserDao().create(user);
        List<User> users = daoFactory.getUserDao().findAll();
        return users.get(users.size()-1);
    }

    @PutMapping("/user/{id}")
    public User updateUser(@PathVariable("id") int id, @RequestBody User user) throws UserNotFoundException {
        if(Optional.ofNullable(daoFactory.getUserDao().findById(id)).isPresent()) {
            User userUpdate = daoFactory.getUserDao().findById(id);
            userUpdate.setName(user.getName());
            userUpdate.setAge(user.getAge());
            daoFactory.getUserDao().update(userUpdate);
            return daoFactory.getUserDao().findById(id);
        }else throw new UserNotFoundException();
    }

    @GetMapping("/user/{id}")
    public User getById(@PathVariable("id") int id) throws UserNotFoundException {
        Optional<User> user = Optional.ofNullable(daoFactory.getUserDao().findById(id));
        return user.orElseThrow(UserNotFoundException::new);
    }

    @GetMapping("/users")
    public List<User> getUsers(){
        return daoFactory.getUserDao().findAll();
    }

    @DeleteMapping("/user/{id}")
    public void deleteUser(@PathVariable("id") int id){
        daoFactory.getUserDao().delete(daoFactory.getUserDao().findById(id));
    }
}
