package entity;

public class Expression {
    private String notation;
    private int countStep;

    public Expression(String notation, int countStep){
        this.notation = notation;
        this.countStep = countStep;
    }

    public String getNotation() {
        return notation;
    }

    public int getCountStep() {
        return countStep;
    }
}
