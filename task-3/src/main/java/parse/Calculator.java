package parse;

import entity.Expression;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Calculator {
    private final ParserExpression parser;
    private final List<Expression> expressionList;
    private int i=0;

    public Calculator() {
        parser = new ParserExpression();
        expressionList = new ArrayList<>();
    }

    public void toCalculate(String filePath){
        try(BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            System.out.println("Result");
            String stringExpression;
            while ((stringExpression=br.readLine())!=null){
                if (stringExpression.isEmpty()) continue;
                if(parser.checkCorrectness(stringExpression)) {
                   toCalculateExpression(stringExpression);
                   expressionList.add(new Expression(stringExpression,parser.getCountStep()));
                }else{
                   System.out.format("%d.Incorrect expression\n",++i);
                   expressionList.add(new Expression("Incorrect expression",0));
                }
            }
            getResultAtSomeStep();
        } catch (FileNotFoundException e) {
            System.out.println("Incorrect path");
        } catch (IOException e) {
           System.out.println(e.getMessage());
        }
    }

    private void toCalculateExpression(String expression){
        String result = parser.toCalculate(expression);
        System.out.format("%d.%-3d",++i,parser.getCountStep());
        System.out.format("steps; %-4.4sresult; ",result);
        System.out.println(expression.replace(" ",""));
    }

    private void getResultAtSomeStep(){
        System.out.println("Enter expression number and step");
        Scanner scanner = new Scanner(System.in);
        int numberExpression = scanner.nextInt();
        int numberStep = scanner.nextInt();

        Expression expression = expressionList.get(numberExpression-1);
        if (expression.getNotation().equals("Incorrect expression"))
            System.out.println(numberExpression+".Incorrect value");
        else if (numberStep>expression.getCountStep() || numberStep<=0 )
            System.out.println(numberStep+".Incorrect value");
        else System.out.println(numberStep + ". "+parser.getSolutionInStep(expression.getNotation(),numberStep));
    }
}
