package parse;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class ParserExpression {
    private final String operators = "/*+-";
    private final Stack<String> stackOperations;
    private final Stack<String> stackPostfix;
    private final Stack<String> stackAnswer;
    private final Stack<String> stackForBuildStep;
    private final Map<Integer, String> stackSteps;
    private int countStep;

    public ParserExpression(){
        stackOperations = new Stack();
        stackPostfix = new Stack();
        stackAnswer = new Stack();
        stackForBuildStep = new Stack();
        stackSteps = new HashMap();
    }

    public int getCountStep() {
        return countStep;
    }

    private boolean isOperator(String token){
        return operators.contains(token);
    }

    private boolean isNumber(String token) {
        try {
            Double.parseDouble(token);
        }catch (Exception e) {
            return false;
        }
        return true;
    }

    private int getPriority(String token){
        if(token.equals("(")) return 1;
        if (token.equals("+") || token.equals("-")) return 2;
        if (token.equals("*") || token.equals("/")) return 3;
        return 4;
    }

    private boolean isOpenBracket(String token) {
        return token.equals("(");
    }

    private boolean isCloseBracket(String token) {
        return token.equals(")");
    }

    private void parse(String infixNotation){
        infixNotation = infixNotation.replace(" ", "").replace("(-", "(0-");
        if (infixNotation.charAt(0) == '-') {
            infixNotation = "0" + infixNotation;
        }
        StringTokenizer stringTokenizer = new StringTokenizer(infixNotation, operators + "()", true);
        while (stringTokenizer.hasMoreTokens()) {
            String token = stringTokenizer.nextToken();
            if (isOpenBracket(token)) {
                stackOperations.push(token);
            } else if (isCloseBracket(token)) {
                while (!stackOperations.empty() && !isOpenBracket(stackOperations.lastElement())) {
                    stackPostfix.push(stackOperations.pop());
                }
                stackOperations.pop();
            } else if (isNumber(token)) {
                    stackPostfix.push(token);
            }
            else if (isOperator(token)) {
                while (!stackOperations.empty()
                        && isOperator(stackOperations.lastElement())
                        && getPriority(token) <= getPriority(stackOperations
                        .lastElement())) {
                    stackPostfix.push(stackOperations.pop());
                }
                stackOperations.push(token);
            }
        }
        while (!stackOperations.empty()) {
            stackPostfix.push(stackOperations.pop());
        }
        Collections.reverse(stackPostfix);
    }

    public String toCalculate(String infixNotation) {
        stackPostfix.clear();
        stackAnswer.clear();
        stackForBuildStep.clear();
        stackOperations.clear();
        stackSteps.clear();
        countStep=0;
        parse(infixNotation);
        Stack<String> stackOfExpressionOnStep = new Stack<>();
        while (!stackPostfix.empty()){
            String token = stackPostfix.pop();
            if(isNumber(token)){
                stackAnswer.push(token);
            }else if(isOperator(token)){
                double operand1 = Double.parseDouble(stackAnswer.pop());
                double operand2 = Double.parseDouble(stackAnswer.pop());
                switch (token) {
                    case "+":
                        stackAnswer.push(convertDoubleOrIntegerToString(operand2+operand1));
                        addCurrentStepToStack(stackOfExpressionOnStep);
                        break;
                    case "-":
                        stackAnswer.push(convertDoubleOrIntegerToString(operand2-operand1));
                        if (operand2 == 0) countStep--;
                        addCurrentStepToStack(stackOfExpressionOnStep);
                        break;
                    case "*":
                        stackAnswer.push(convertDoubleOrIntegerToString(operand2*operand1));
                        addCurrentStepToStack(stackOfExpressionOnStep);
                        break;
                    case "/":
                        stackAnswer.push(convertDoubleOrIntegerToString(operand2/operand1));
                        addCurrentStepToStack(stackOfExpressionOnStep);
                        break;
                }
            }
        }
        return convertDoubleOrIntegerToString(Double.parseDouble(stackAnswer.pop()));
    }

    private void addCurrentStepToStack(Stack<String> stackOfExpressionOnStep){
        if(!stackPostfix.empty()) {
            stackOfExpressionOnStep.clear();
            if (stackAnswer.lastElement().equalsIgnoreCase("0")) stackOfExpressionOnStep.push("0");
            stackOfExpressionOnStep.addAll(stackAnswer);
            String stepAnswer = getInfixStep(stackOfExpressionOnStep, stackPostfix);
            stackSteps.put(++countStep, stepAnswer);
        }else stackSteps.put(++countStep,stackAnswer.lastElement());
    }

    public String getSolutionInStep(String expression, int stepNumber){
        toCalculate(expression);
        if(stepNumber!=0 && stepNumber<=stackSteps.size()) {
            return stackSteps.get(stepNumber);
        }else return expression.replaceAll(" ","");
    }

    private String convertDoubleOrIntegerToString(double number){
        if(number%1==0){
            int numberInt = (int) Math.round(number);
            return String.valueOf(numberInt);
        }
        return String.valueOf(number);
    }

    private String convertPostfixToInfix(){
        String token;
        String str="";
        if(!stackForBuildStep.empty()) {
             token = stackForBuildStep.pop();
        }else return str;
        if (isNumber(token)) {
            return token;
        } else {
            String x = convertPostfixToInfix();
            String y = convertPostfixToInfix();
            str = "".concat("(" + y + token + x + ")");
            return str;
        }
    }

    private String getInfixStep(Stack<String> prefix, Stack<String> s){
        stackForBuildStep.clear();
        Collections.reverse(prefix);
        stackForBuildStep.addAll(s);
        for (String str : prefix) {
            stackForBuildStep.push(str);
        }
        Collections.reverse(stackForBuildStep);
        return convertPostfixToInfix().replaceAll("\\(0-","(-")
                .replaceAll("^.|.$", "")
                .replaceAll("\\+-","+")
                .replaceAll("--","+");
    }

    public boolean checkCorrectness(String infixNotation){
        infixNotation = infixNotation.replaceAll(" ","");
        Matcher matcherDoubleOperations =  Pattern.compile("[*/+-][*/+-]+").matcher(infixNotation);
        Matcher matcherBracket =  Pattern.compile("[(][)]+").matcher(infixNotation);
        Matcher matcherDivisionByZero = Pattern.compile("[/][0]+").matcher(infixNotation);
        Matcher matcherNoSignOpenBracket = Pattern.compile("[0-9][(][0-9]").matcher(infixNotation);
        Matcher matcherNoSignCloseBracket = Pattern.compile("[0-9][)][0-9]").matcher(infixNotation);
        StringTokenizer stringTokenizer = new StringTokenizer(infixNotation, operators + "()", true);
        int count = 0;
        if(infixNotation.matches("[0-9-()*/.+ ]+")){
            if (!matcherDoubleOperations.find() && !matcherBracket.find()
                    && !matcherDivisionByZero.find() && !matcherNoSignCloseBracket.find() && !matcherNoSignOpenBracket.find()) {
                while (stringTokenizer.hasMoreTokens()) {
                    String token = stringTokenizer.nextToken();
                    if (token.equals("("))
                        count++;
                    else if (token.equals(")")) count--;
                }
                if (count == 0) return true;
                else return false;
            }else return false;
        }else return false;
    }
}