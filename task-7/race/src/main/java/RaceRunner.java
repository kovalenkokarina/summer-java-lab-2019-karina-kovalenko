import java.util.Scanner;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.stream.Stream;

public class RaceRunner {

    private static CountDownLatch countStart;
    private static Logger LOGGER = LogManager.getLogger(RaceRunner.class.getName());

    public static void main(String[] args) throws InterruptedException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number of horse:");
        int horseNumber = scanner.nextInt();
        countStart = new CountDownLatch(horseNumber+1);

        System.out.println("Enter length of track:");
        int lengthTrack = scanner.nextInt();

        ExecutorService service = Executors.newFixedThreadPool(horseNumber);

        Stream.iterate(0, i->i+1).limit(horseNumber).forEach(i -> {
            Horse horse = new Horse(lengthTrack, i, countStart, service);
            horse.go();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        while (countStart.getCount()>1){
            Thread.sleep(100);
        }

        Thread.sleep(1000);
        LOGGER.info("Ready to go! Attention! Go!\n");
        countStart.countDown();

        service.shutdown();

    }
}