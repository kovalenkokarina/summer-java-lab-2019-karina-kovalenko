import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Horse extends Thread {
    private int speed;
    private int lengthOfTrack;
    private ExecutorService service;
    private int horseNumber;
    private static Logger LOGGER = LogManager.getLogger(Horse.class.getName());
    private CountDownLatch count;

    public Horse(int lengthOfTrack, int horseNumber, CountDownLatch countDownLatch, ExecutorService service) {
        Random random = new Random();
        speed = random.ints(0,88).findFirst().getAsInt();
        this.lengthOfTrack = lengthOfTrack;
        this.count = countDownLatch;
        this.service = service;
        this.horseNumber = horseNumber;
    }

    @Override
    public void run() {
        Timer timer = new Timer();
        int time = (lengthOfTrack/speed)*1000;
        LOGGER.info("Horse №" + horseNumber + " has arrived, speed: " + speed + " m/s");
        count.countDown();
        try {
            count.await();
            TimerTask timerTask = new TimerTask() {
                @Override
                public void run() {
                    lengthOfTrack = lengthOfTrack - speed;
                    if(lengthOfTrack>0)
                        LOGGER.info("The horse №" +horseNumber + " has " + lengthOfTrack + " meters to the finish line.\n");
                }
            };
            timer.scheduleAtFixedRate(timerTask, 0, 1000);
            sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        LOGGER.info("Horse №" + horseNumber + " came to the finish!\n");
        timer.cancel();
    }

    public void go() {
        service.submit(this);
    }
}
