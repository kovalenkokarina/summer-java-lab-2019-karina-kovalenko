package consumer;

import task.TaskA;
import task.TaskB;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

public class Consumer implements Runnable{
    private final BlockingQueue queue;
    private final ExecutorService service;

    public Consumer(BlockingQueue queue, ExecutorService service){
        this.queue = queue;
        this.service = service;
    }

    @Override
    public void run() {
        while (!queue.isEmpty()) {
            try {
                Integer number = (Integer) queue.poll(2, TimeUnit.SECONDS);
                if (number == 0) {
                    TaskA taskA = new TaskA();
                    System.out.println("ConsumerA took: " + taskA.returnTaskName() + " from queue");
                } else {
                    TaskB taskB = new TaskB();
                    taskB.toDisplayTaskName();
                }
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void consume() {
        service.execute(new Thread(this));
    }
}
