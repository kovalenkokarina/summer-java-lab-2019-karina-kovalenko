import consumer.Consumer;
import producer.Producer;

import java.util.Scanner;
import java.util.concurrent.*;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Runner {

    public static void main(String[] args) throws InterruptedException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number of producers:");
        int producerNumber = scanner.nextInt();

        System.out.println("Enter the number of consumers:");
        int consumerNumber = scanner.nextInt();

        BlockingQueue<Integer> queue = new ArrayBlockingQueue<>(10);

        ExecutorService producerService = Executors.newFixedThreadPool(producerNumber);
        ExecutorService consumerService = Executors.newFixedThreadPool(consumerNumber);

        Producer producer = new Producer(queue,producerService);
        producer.produce();

        producerService.awaitTermination(1000,TimeUnit.MILLISECONDS);
        IntStream.range(0,consumerNumber).forEach(i -> {
            Consumer consumer = new Consumer(queue,consumerService);
            consumer.consume();
        });

        consumerService.shutdown();
    }
}
