package consumer;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

public class Consumer implements Runnable {
    private final BlockingQueue queue;
    private ExecutorService service;

    public Consumer(BlockingQueue queue, ExecutorService service){
        this.queue = queue;
        this.service = service;
    }

    @Override
    public void run() {
        while (!queue.isEmpty()){
            try {
                Integer number = (Integer) queue.poll(2, TimeUnit.SECONDS);
                System.out.println("Consumer took " + number + " from queue");
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void consume(){
        service.execute(new Thread(this));
    }
}
