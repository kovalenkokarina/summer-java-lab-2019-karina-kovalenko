package producer;

import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.stream.IntStream;

public class Producer implements Runnable {
    private final BlockingQueue queue;
    private ExecutorService service;

    public Producer(BlockingQueue queue, ExecutorService service){
        this.queue = queue;
        this.service = service;
    }

    @Override
    public void run() {
        Random random = new Random();
        int i = random.ints(0, 100).findFirst().getAsInt();
        System.out.println("Producer put " + i + " to queue");
        try {
            queue.put(i);
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void produce(){
        IntStream.range(0,10).forEach(i-> service.execute(new Thread(this)));
        service.shutdown();
    }
}
