package impl;

import annotations.After;
import annotations.Before;
import annotations.Ignore;
import annotations.ThrowException;
import interfaces.IMetaService;

@Before(message = "MetaService Before 1")
@Before(message = "MetaService Before 2")
public class MetaService implements IMetaService{

    @Override
    @After(message = "MetadataInfo After 1")
    public void printMetadataInfo() {
        System.out.println("MetadataInfo");
    }

    @Override
    @Before(message = "MetadataExpiredTime Before 1")
    @Before(message = "MetadataExpiredTime Before 2")
    public void printMetadataExpiredTime() {
        System.out.println("MetadataExpiredTime");
    }

    @Override
    @Ignore
    @After(message = "MetadataUser After 1")
    public void printMetadataUser() {
        System.out.println("MetadataUser");
    }


    @Override
    @ThrowException
    public void printMetadataBuilder() {
        System.out.println("MetadataBuilder");
    }
}
