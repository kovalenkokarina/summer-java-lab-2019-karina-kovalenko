package impl;

import annotations.After;
import annotations.Before;
import annotations.Ignore;
import interfaces.ITimeService;

@Ignore
@After(message = "TimeService After 1")
@Before(message = "TimeService Before 1")
public class TimeService implements ITimeService {

    @Override
    @After(message = "TimeInfo After 1")
    public void printTimeInfo() {
        System.out.println("TimeInfo");
    }

    @Override
    public void printTimeExpiredTime() {
        System.out.println("TimeExpiredTime");
    }

    @Override
    @Ignore
    @After(message = "TimeInfo User 1")
    public void printTimeUser() {
        System.out.println("TimeUser");
    }

    @Override
    public void printTimeBuilder() {
        System.out.println("TimeBuilder");
    }
}
