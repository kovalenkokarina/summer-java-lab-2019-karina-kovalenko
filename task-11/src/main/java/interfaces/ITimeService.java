package interfaces;

public interface ITimeService {
    void printTimeInfo();
    void printTimeExpiredTime();
    void printTimeUser();
    void printTimeBuilder();
}
