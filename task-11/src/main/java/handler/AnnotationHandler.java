package handler;

import annotations.*;
import handler.exceptions.NotSupportedMethodException;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

public class AnnotationHandler implements InvocationHandler {

    private Object obj;

    public AnnotationHandler(Object obj){
        this.obj = obj;
    }

    @Override
    public Object invoke(Object o, Method method, Object[] objects) throws NotSupportedMethodException, InvocationTargetException, IllegalAccessException {
        Object result;
        if(obj.getClass().isAnnotationPresent(Ignore.class)) {
            result = method.invoke(obj, objects);
        }else {
            Method printMethod = Arrays.stream(obj.getClass().getMethods()).filter(m -> m.getName().equals(method.getName())).findFirst().get();
            if(!printMethod.isAnnotationPresent(ThrowException.class)) {
                if(!printMethod.isAnnotationPresent(Ignore.class)) {
                    if (obj.getClass().isAnnotationPresent(Before.class)
                            || obj.getClass().isAnnotationPresent(BeforeRepeatable.class)) {
                        Arrays.stream(obj.getClass().getAnnotationsByType(Before.class)).forEach(m -> System.out.println(m.message()));
                    }
                    if ((printMethod.isAnnotationPresent(Before.class) || printMethod.isAnnotationPresent(BeforeRepeatable.class))) {
                        printBeforeAnnotationMessage(printMethod);
                    }
                    result = method.invoke(obj, objects);
                    if (obj.getClass().isAnnotationPresent(After.class)
                            || obj.getClass().isAnnotationPresent(AfterRepeatable.class)) {
                        Arrays.stream(obj.getClass().getAnnotationsByType(After.class)).forEach(m -> System.out.println(m.message()));
                    }
                    if ((printMethod.isAnnotationPresent(After.class) || printMethod.isAnnotationPresent(AfterRepeatable.class))) {
                        printAfterAnnotationMessage(printMethod);
                    }
                }else result = method.invoke(obj, objects);
            }else throw new NotSupportedMethodException("handler.exceptions.NotSupportedMethodException");
        }
        return result;
    }

    private void printBeforeAnnotationMessage(Method method) {
        Arrays.stream(method.getAnnotationsByType(Before.class)).forEach(a -> System.out.println(a.message()));
    }

    private void printAfterAnnotationMessage(Method method) {
        Arrays.stream(method.getAnnotationsByType(After.class)).forEach(a -> System.out.println(a.message()));
    }
}
