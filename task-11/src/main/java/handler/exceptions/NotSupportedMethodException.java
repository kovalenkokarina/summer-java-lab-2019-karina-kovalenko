package handler.exceptions;

public class NotSupportedMethodException extends RuntimeException{
    public NotSupportedMethodException(String message){
        super(message);
    }
}
