package handler.factory;

import handler.AnnotationHandler;
import interfaces.IMetaService;
import interfaces.ITimeService;

import java.lang.reflect.Proxy;

public class ProxyFactory {

    public static IMetaService newInstance(IMetaService obj){
        return (IMetaService) Proxy.newProxyInstance(obj.getClass().getClassLoader(),
                obj.getClass().getInterfaces(),
                new AnnotationHandler(obj));
    }

    public static ITimeService newInstance(ITimeService obj){
        return (ITimeService) Proxy.newProxyInstance(obj.getClass().getClassLoader(),
                obj.getClass().getInterfaces(),
                new AnnotationHandler(obj));
    }

}
