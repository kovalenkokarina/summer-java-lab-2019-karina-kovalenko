import handler.factory.ProxyFactory;
import impl.MetaService;
import impl.TimeService;
import interfaces.IMetaService;
import interfaces.ITimeService;

public class Runner {
    public static void main(String[] args) {
        IMetaService iMetaService = ProxyFactory.newInstance(new MetaService());
        iMetaService.printMetadataExpiredTime();
        iMetaService.printMetadataInfo();
        iMetaService.printMetadataUser();
        iMetaService.printMetadataBuilder();

        ITimeService iTimeService = ProxyFactory.newInstance(new TimeService());
        iTimeService.printTimeBuilder();
        iTimeService.printTimeExpiredTime();
        iTimeService.printTimeInfo();
        iTimeService.printTimeUser();


    }
}
