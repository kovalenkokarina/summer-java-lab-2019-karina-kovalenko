package annotations;

import java.lang.annotation.*;

@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Repeatable(value = BeforeRepeatable.class)
public @interface Before {
    String message() default "";
}
