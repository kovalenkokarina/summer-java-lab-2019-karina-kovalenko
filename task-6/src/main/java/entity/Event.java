package entity;

import entity.enums.Currency;

import java.math.BigDecimal;
import java.time.LocalDate;

public class Event {
    private int id;
    private Currency currency;
    private BigDecimal cost;
    private LocalDate date;

    public Event(){}

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
