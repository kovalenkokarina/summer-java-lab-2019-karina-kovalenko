package entity;

import entity.enums.TypeShowFor;
import java.util.List;

public class Show {
    private TypeShowFor type;
    private List<String> users;

    public List<String> getUsers() {
        return users;
    }

    public void setUsers(List<String> users) {
        this.users = users;
    }

    public TypeShowFor getType() {
        return type;
    }

    public void setType(TypeShowFor type) {
        this.type = type;
    }
}
