package entity;

import entity.enums.Period;
import entity.enums.Status;

import java.math.BigDecimal;
import java.time.LocalDate;

public class Info {
    private String idCredit;
    private String idUser;
    private BigDecimal money;
    private LocalDate deptRepaymentDate;
    private int amountTransactions;
    private String name;
    private Status status;
    private LocalDate birthday;
    private Period period;

    public Info(){}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public String toString(){
        StringBuilder table = new StringBuilder();
            table.append(String.format("%-9s", idCredit == null? "null" : idCredit)).append("|");
        table.append(String.format("%-7s",idUser == null ? "null" : idUser)).append("|");
        table.append(String.format("%-20s",name)).append("|");
        table.append(String.format("%-12s", idCredit==null ? "null" : amountTransactions)).append("|");
        table.append(String.format("%-20s", money)).append("|");
        table.append(String.format("%-6s", period)).append("|");
        if(status == null) {
            table.append(String.format("%-17s","null")).append("|");
        }else {
            if (status.equals(Status.DONE))
                table.append(String.format("%-17s", status + " - " + deptRepaymentDate)).append("|");
            else table.append(String.format("%-17s", status)).append("|");
        }
        table.append("\n");
        return table.toString();
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public LocalDate getDeptRepaymentDate() {
        return deptRepaymentDate;
    }

    public void setDeptRepaymentDate(LocalDate deptRepaymentDate) {
        this.deptRepaymentDate = deptRepaymentDate;
    }

    public int getAmountTransactions() {
        return amountTransactions;
    }

    public void setAmountTransactions(int amountTransactions) {
        this.amountTransactions = amountTransactions;
    }

    public String getIdCredit() {
        return idCredit;
    }

    public void setIdCredit(String idCredit) {
        this.idCredit = idCredit;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public Period getPeriod() {
        return period;
    }

    public void setPeriod(Period period) {
        this.period = period;
    }
}
