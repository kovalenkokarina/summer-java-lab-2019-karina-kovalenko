package entity;

import entity.enums.Sort;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

public class Settings {
    private LocalDate dateFrom;
    private LocalDate dateTo;
    private Show showFor;

    private Sort sortBy;
    private List<String> useDepartments;

    private BigDecimal startCostEUR;
    private BigDecimal startCostUSD;

    public Settings(){}

    public Settings(LocalDate dateFrom, LocalDate dateTo, Show showFor, Sort sortBy, List<String> useDepartments, BigDecimal startCostEUR, BigDecimal startCostUSD) {
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.showFor = showFor;
        this.sortBy = sortBy;
        this.useDepartments = useDepartments;
        this.startCostEUR = startCostEUR;
        this.startCostUSD = startCostUSD;
    }

    public LocalDate getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(LocalDate dateFrom) {
        this.dateFrom = dateFrom;
    }

    public LocalDate getDateTo() {
        return dateTo;
    }

    public void setDateTo(LocalDate dateTo) {
        this.dateTo = dateTo;
    }

    public Show getShowFor() {
        return showFor;
    }

    public void setShowFor(Show showFor) {
        this.showFor = showFor;
    }

    public Sort getSortBy() {
        return sortBy;
    }

    public void setSortBy(Sort sortBy) {
        this.sortBy = sortBy;
    }

    public List<String> getUseDepartments() {
        return useDepartments;
    }

    public void setUseDepartments(List<String> useDepartments) {
        this.useDepartments = useDepartments;
    }

    public BigDecimal getStartCostEUR() {
        return startCostEUR;
    }

    public void setStartCostEUR(BigDecimal startCostEUR) {
        this.startCostEUR = startCostEUR;
    }

    public BigDecimal getStartCostUSD() {
        return startCostUSD;
    }

    public void setStartCostUSD(BigDecimal startCostUSD) {
        this.startCostUSD = startCostUSD;
    }
}
