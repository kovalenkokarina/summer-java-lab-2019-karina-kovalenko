package entity;

import entity.enums.Sex;

import java.time.LocalDate;

public class User {
    private int id;
    private String name;
    private String secondName;
    private Sex sex;
    private LocalDate birthday;

    public User(){

    }

    public User(int id, String name, String secondName, Sex sex, LocalDate birthday) {
        this.id = id;
        this.name = name;
        this.secondName = secondName;
        this.sex = sex;
        this.birthday = birthday;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }
}
