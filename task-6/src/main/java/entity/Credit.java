package entity;
import entity.enums.Period;

import java.math.BigDecimal;
import java.time.LocalDate;


public class Credit {
    private int id;
    private int userId;
    private LocalDate date;
    private Period period;
    private BigDecimal money;
    private BigDecimal rate;
    private LocalDate deptRepaymentDate;
    private int amountTransactions;

    public Credit(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Period getPeriod() {
        return period;
    }

    public void setPeriod(Period period) {
        this.period = period;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public LocalDate getDeptRepaymentDate() {
        return deptRepaymentDate;
    }

    public void setDeptRepaymentDate(LocalDate deptRepaymentDate) {
        this.deptRepaymentDate = deptRepaymentDate;
    }

    public int getAmountTransactions() {
        return amountTransactions;
    }

    public void setAmountTransactions(int amountTransactions) {
        this.amountTransactions = amountTransactions;
    }
}
