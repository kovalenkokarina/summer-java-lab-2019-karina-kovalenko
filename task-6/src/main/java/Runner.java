import bankWorker.CreditCalculation;
import bankWorker.DepartmentsReader;

import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Paths;

public class Runner {
    public static void main(String[] args) throws IOException, URISyntaxException {
        URI uri = Runner.class.getResource("/").toURI();
        String path = Paths.get(uri).toString();
        CreditCalculation creditCalculation = new CreditCalculation();
        DepartmentsReader departmentsReader = new DepartmentsReader(path+"\\");
        if(departmentsReader.read()){
            creditCalculation.toCalculate();
        }
    }
}
