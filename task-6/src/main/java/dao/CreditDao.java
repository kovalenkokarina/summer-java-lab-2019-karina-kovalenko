package dao;

import entity.Credit;

import java.util.List;

public interface CreditDao extends Dao<Credit> {

    List<Credit> getCreditsByUserId(long id);
}
