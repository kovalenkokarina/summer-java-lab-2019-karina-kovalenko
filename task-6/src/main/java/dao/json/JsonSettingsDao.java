package dao.json;

import com.google.gson.*;
import dao.SettingsDao;
import entity.Settings;
import entity.Show;
import entity.enums.Sort;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class JsonSettingsDao implements SettingsDao {

    private final String path = JsonWorker.getPathToSettings();

    @Override
    public Settings getSettings() throws FileNotFoundException {
        JsonObject jsonObject = new JsonParser().parse(new FileReader(path)).getAsJsonObject();
        JsonObject object = jsonObject.getAsJsonObject("settings");
        Settings settings = new Settings();
        settings.setDateFrom(!object.get("dateFrom").isJsonNull() ? LocalDate.parse(object.get("dateFrom").getAsString()) : null);
        settings.setDateTo(!object.get("dateTo").isJsonNull() ? LocalDate.parse(object.get("dateTo").getAsString()) : null);
        if(!object.get("useDepartments").isJsonNull()){
            List<String> departments = new ArrayList<>();
            object.get("useDepartments").getAsJsonArray().forEach(i -> departments.add(i.getAsString()));
            settings.setUseDepartments(departments);
        }
        settings.setStartCostUSD(object.get("startCostUSD").getAsBigDecimal());
        settings.setStartCostEUR(object.get("startCostEUR").getAsBigDecimal());
        settings.setSortBy(new Gson().fromJson(object.get("sortBy").toString(), Sort.class));
        settings.setShowFor(!object.get("showFor").isJsonNull() ? new Gson().fromJson(object.get("showFor").toString(), Show.class) : null);
        return settings;
    }

    @Override
    public boolean checkSettings() throws FileNotFoundException {
        boolean isWrong = true;
        JsonObject jsonObject = new JsonParser().parse(new FileReader(path)).getAsJsonObject();
        JsonObject object = jsonObject.getAsJsonObject("settings");
        if(!object.has("dateFrom")) {
            System.out.println(" dateFrom not found");
            isWrong = false;
        }if(!object.has("dateTo")) {
            System.out.println("dateTo not found");
            isWrong = false;
        }if(!object.has("useDepartments")) {
            System.out.println("useDepartments not found");
            isWrong = false;
        }if(!object.has("startCostUSD")) {
            System.out.println("startCostUSD not found");
            isWrong = false;
        }if(!object.has("startCostEUR")) {
            System.out.println("startCostEUR not found");
            isWrong = false;
        }if(!object.has("sortBy")) {
            System.out.println("sortBy not found");
            isWrong = false;
        }if(!object.has("showFor")) {
            System.out.println("showFor not found");
            isWrong = false;
        }else if(!object.get("showFor").isJsonNull()){
            if(!object.get("showFor").getAsJsonObject().has("users")){
                System.out.println("users not found");
                isWrong = false;
            }if(!object.get("showFor").getAsJsonObject().has("type")){
                System.out.println("type not found");
                isWrong = false;
            }
        }
        return isWrong;
    }
}
