package dao.json;

import com.google.gson.*;
import dao.CreditDao;
import entity.Credit;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class JsonCreditDao implements CreditDao {

    private final String KEY = "credits";

    @Override
    public List<Credit> getAll()  {
        JsonArray array = JsonWorker.read(KEY);
        Gson gson = new GsonBuilder().registerTypeAdapter(LocalDate.class, (JsonDeserializer<LocalDate>) (jsonElement, type, jsonDeserializationContext)
                -> LocalDate.parse(jsonElement.getAsJsonPrimitive().getAsString())).create();
        return IntStream.range(0,array.size()).mapToObj(i -> gson.fromJson(array.get(i).toString(),Credit.class)).collect(Collectors.toList());
    }

    @Override
    public void update(Credit credit){
        JsonWorker.update(KEY,convertToJson(credit));
    }

    private JsonObject convertToJson(Credit credit){
        JsonObject object = new JsonObject();
        object.addProperty("id",credit.getId());
        object.addProperty("userId",credit.getUserId());
        object.addProperty("date",credit.getDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        object.addProperty("period",credit.getPeriod().toString());
        object.addProperty("money",credit.getMoney());
        object.addProperty("rate",credit.getRate());
        return object;
    }

    @Override
    public List<Credit> getCreditsByUserId(long id) {
        return getAll().stream().filter(credit -> credit.getUserId() == id).collect(Collectors.toList());
    }
}
