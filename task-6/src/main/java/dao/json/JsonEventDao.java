package dao.json;

import com.google.gson.*;
import dao.EventDao;
import entity.Event;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import entity.enums.Currency;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class JsonEventDao implements EventDao {

    private final String KEY = "events";

    @Override
    public List<Event> getAll() {
        JsonArray array = JsonWorker.read(KEY);
        Gson gson = new GsonBuilder().registerTypeAdapter(LocalDate.class, (JsonDeserializer<LocalDate>) (jsonElement, type, jsonDeserializationContext)
                -> LocalDate.parse(jsonElement.getAsJsonPrimitive().getAsString())).create();
        return IntStream.range(0,array.size()).mapToObj(i -> gson.fromJson(array.get(i).toString(),Event.class)).collect(Collectors.toList());
    }

    @Override
    public void update(Event event) {
        JsonWorker.update(KEY,convertToJson(event));
    }

    private JsonObject convertToJson(Event event){
        JsonObject object = new JsonObject();
        object.addProperty("id",event.getId());
        object.addProperty("currency",event.getCurrency().toString());
        object.addProperty("cost",event.getCost());
        object.addProperty("date",event.getDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        return object;
    }

    @Override
    public Optional<Event> getEventByDateAndCurrency(LocalDate date, Currency currency) {
        return getAll().stream().sorted(Comparator.comparing(Event::getDate).reversed())
                .filter(e->e.getCurrency().equals(currency))
                .filter(e-> e.getDate().isBefore(date.plusDays(1))).findFirst();
    }
}
