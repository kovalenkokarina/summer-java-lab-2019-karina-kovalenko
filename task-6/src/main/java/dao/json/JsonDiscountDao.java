package dao.json;

import com.google.gson.*;
import dao.DiscountDao;
import entity.Discount;
import entity.enums.Type;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class JsonDiscountDao implements DiscountDao {

    private final String KEY = "discounts";

    @Override
    public List<Discount> getAll()  {
        JsonArray array = JsonWorker.read(KEY);
        Gson gson = new GsonBuilder().registerTypeAdapter(LocalDate.class, (JsonDeserializer<LocalDate>) (jsonElement, type, jsonDeserializationContext)
                -> LocalDate.parse(jsonElement.getAsJsonPrimitive().getAsString())).create();
        return IntStream.range(0,array.size()).mapToObj(i -> gson.fromJson(array.get(i).toString(),Discount.class)).collect(Collectors.toList());
    }

    @Override
    public void update(Discount discount){
        JsonWorker.update(KEY,convertToJson(discount));
    }

    private JsonObject convertToJson(Discount discount){
        JsonObject object = new JsonObject();
        object.addProperty("id",discount.getId());
        object.addProperty("type",discount.getType().toString());
        if(discount.getType().equals(Type.MANY)){
            object.addProperty("dateFrom",discount.getDateFrom().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
            object.addProperty("dateTo",discount.getDateTo().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        }else {
            object.addProperty("date",discount.getDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        }
        object.addProperty("discount",discount.getDiscount());
        return object;
    }

    @Override
    public List<Discount> getDiscountsByDate(LocalDate date) {
        List<Discount> discounts = new ArrayList<>();
        List<Discount> discountsOne = getAll().stream().filter(d->d.getType().equals(Type.ONE)).filter(d->d.getDate().equals(date)).collect(Collectors.toList());
        List<Discount> discountsMany = getAll().stream().filter(d -> d.getType().equals(Type.MANY))
                    .filter(d -> date.isAfter(d.getDateFrom().minusDays(1)) && date.isBefore(d.getDateTo().plusDays(1))).collect(Collectors.toList());
        discounts.addAll(discountsOne);
        discounts.addAll(discountsMany);
        return discounts;
    }
}
