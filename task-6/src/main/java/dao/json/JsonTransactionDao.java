package dao.json;

import com.google.gson.*;
import dao.TransactionDao;
import entity.Transaction;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class JsonTransactionDao implements TransactionDao {

    private final String KEY = "transactions";

    @Override
    public List<Transaction> getAll() {
        JsonArray array = JsonWorker.read(KEY);
        Gson gson = new GsonBuilder().registerTypeAdapter(LocalDate.class, (JsonDeserializer<LocalDate>) (jsonElement, type, jsonDeserializationContext)
                -> LocalDate.parse(jsonElement.getAsJsonPrimitive().getAsString())).create();
        return IntStream.range(0,array.size()).mapToObj(i -> gson.fromJson(array.get(i).toString(),Transaction.class)).collect(Collectors.toList());
    }

    @Override
    public void update(Transaction transaction) {
        JsonWorker.update(KEY,convertToJson(transaction));
    }

    private JsonObject convertToJson(Transaction transaction){
        JsonObject object = new JsonObject();
        object.addProperty("id",transaction.getId());
        object.addProperty("date",transaction.getDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        object.addProperty("userId",transaction.getUserId());
        object.addProperty("creditId",transaction.getCreditId());
        object.addProperty("currency",transaction.getCurrency().toString());
        object.addProperty("money",transaction.getMoney());
        return object;
    }

    @Override
    public List<Transaction> getTransactionsByUserIdAndCreditId(long userId, long creditId) {
        return getAll().stream().filter(t-> (t.getCreditId()==creditId && t.getUserId() == userId)).collect(Collectors.toList());
    }

    @Override
    public List<Transaction> getTransactionsByDate(LocalDate date) {
        return getAll().stream().filter(t-> t.getDate().equals(date)).collect(Collectors.toList());
    }
}
