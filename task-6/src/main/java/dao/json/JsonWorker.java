package dao.json;

import com.google.gson.*;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;

public class JsonWorker {

    private static String pathDB;

    private static String pathToSettings;

    private static JsonWorker instance;

    private JsonWorker(){}

    public static JsonWorker getInstance(String path){
        if(instance==null){
            JsonWorker.pathDB = path + "db.json";
            JsonWorker.pathToSettings = path + "settings.json";
            return new JsonWorker();
        }else return instance;
    }

    public static JsonArray read(String key){
        JsonArray array = null;
        try (FileReader reader = new FileReader(pathDB)) {
            JsonObject jsonObject = new JsonParser().parse(reader).getAsJsonObject();
            JsonObject data = jsonObject.getAsJsonObject("data");
            array = data.getAsJsonArray(key);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return array;
    }

    public static void update(String key, JsonObject newObject)  {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        try (FileReader reader = new FileReader(pathDB)) {
            JsonObject jsonObject = new JsonParser().parse(reader).getAsJsonObject();
            JsonObject data = jsonObject.getAsJsonObject("data");
            JsonArray array = data.getAsJsonArray(key);
            try (FileWriter fileWriter = new FileWriter(pathDB)) {
                Iterator<JsonElement> iterator = array.iterator();
                int i = 0;
                while (iterator.hasNext()) {
                    JsonObject oldObject = (JsonObject) iterator.next();
                    if (oldObject.get("id").equals(newObject.get("id")))
                        array.set(i, newObject);
                    i++;
                }
                fileWriter.write(gson.toJson(jsonObject));
                fileWriter.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getPathToSettings() {
        return pathToSettings;
    }
}
