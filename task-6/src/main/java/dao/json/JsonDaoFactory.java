package dao.json;

import dao.*;

public class JsonDaoFactory extends DaoFactory {

    private static JsonDaoFactory instance;

    private JsonDaoFactory() {

    }

    public static JsonDaoFactory getFactory(){
        return instance == null ? new JsonDaoFactory() : instance;
    }

    @Override
    public UserDao getUserDao() {
        return new JsonUserDao();
    }

    @Override
    public SettingsDao getSettingsDao() {
        return new JsonSettingsDao();
    }

    @Override
    public DiscountDao getDiscountDao() {
        return new JsonDiscountDao();
    }

    @Override
    public TransactionDao getTransactionDao() {
        return new JsonTransactionDao();
    }

    @Override
    public CreditDao getCreditDao() {
        return new JsonCreditDao();
    }

    @Override
    public EventDao getEventDao() {
        return new JsonEventDao();
    }

}
