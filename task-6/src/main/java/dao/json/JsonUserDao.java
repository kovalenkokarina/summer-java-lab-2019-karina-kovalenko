package dao.json;

import com.google.gson.*;
import dao.UserDao;
import entity.User;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class JsonUserDao implements UserDao {

    private final String KEY = "users";

    public JsonUserDao(){
    }

    @Override
    public Optional<User> getById(long id) {
        return getAll().stream().filter((p) -> p.getId() == id).findFirst();
    }

    @Override
    public Optional<User> getByName(String name) {
        return getAll().stream().filter((p) -> (p.getName()+" "+p.getSecondName()).equalsIgnoreCase(name)).findFirst();
    }

    @Override
    public List<User> getAll() {
        JsonArray array = JsonWorker.read(KEY);
        Gson gson = new GsonBuilder().registerTypeAdapter(LocalDate.class, (JsonDeserializer<LocalDate>) (jsonElement, type, jsonDeserializationContext)
                -> LocalDate.parse(jsonElement.getAsJsonPrimitive().getAsString())).create();
        return IntStream.range(0,array.size()).mapToObj(i -> gson.fromJson(array.get(i).toString(),User.class)).collect(Collectors.toList());
    }

    @Override
    public void update(User user)  {
        JsonWorker.update(KEY,convertToJson(user));
    }


    private JsonObject convertToJson(User user){
        JsonObject object = new JsonObject();
        object.addProperty("id",user.getId());
        object.addProperty("name",user.getName());
        object.addProperty("secondName",user.getSecondName());
        object.addProperty("sex",user.getSex().toString());
        object.addProperty("birthday",user.getBirthday().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        return object;
    }
}
