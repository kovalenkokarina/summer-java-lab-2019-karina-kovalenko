package dao;

import dao.json.JsonDaoFactory;

public abstract class DaoFactory {

    public abstract DiscountDao getDiscountDao();

    public abstract TransactionDao getTransactionDao();

    public abstract CreditDao getCreditDao();

    public abstract EventDao getEventDao();

    public abstract UserDao getUserDao();

    public abstract SettingsDao getSettingsDao();

    public static DaoFactory getDaoFactory(){
        return JsonDaoFactory.getFactory();
    }
}
