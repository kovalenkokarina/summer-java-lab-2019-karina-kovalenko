package dao;

import entity.User;

import java.util.Optional;

public interface UserDao extends Dao<User> {

    Optional<User> getById(long id);

    Optional<User> getByName(String name);

}
