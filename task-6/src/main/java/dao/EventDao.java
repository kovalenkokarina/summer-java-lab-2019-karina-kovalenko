package dao;

import entity.Event;

import java.time.LocalDate;
import entity.enums.Currency;
import java.util.Optional;

public interface EventDao extends Dao<Event> {

    Optional<Event> getEventByDateAndCurrency(LocalDate date, Currency currency);
}
