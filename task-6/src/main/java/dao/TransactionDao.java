package dao;

import entity.Transaction;
import java.time.LocalDate;
import java.util.List;

public interface TransactionDao extends Dao<Transaction>{

    List<Transaction> getTransactionsByUserIdAndCreditId(long userId, long creditId);

    List<Transaction> getTransactionsByDate(LocalDate date);
}
