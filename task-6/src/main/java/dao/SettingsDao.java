package dao;

import entity.Settings;

import java.io.FileNotFoundException;

public interface SettingsDao {

    Settings getSettings() throws FileNotFoundException;

    boolean checkSettings() throws FileNotFoundException;
}
