package dao;

import entity.Discount;

import java.time.LocalDate;
import java.util.List;

public interface DiscountDao extends Dao<Discount> {

    List<Discount> getDiscountsByDate(LocalDate date);
}
