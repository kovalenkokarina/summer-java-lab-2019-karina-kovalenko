package bankWorker;

import dao.DaoFactory;
import entity.*;
import entity.enums.*;
import entity.enums.Currency;
import exсeptions.ThrowableConsumer;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Month;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CreditCalculation {

    private final DaoFactory daoFactory = DaoFactory.getDaoFactory();
    private final List<Info> infoList;

    public CreditCalculation(){
        infoList = new ArrayList<>();
    }

    private LocalDate checkDateFromByNull(LocalDate date) {
        List<LocalDate> localDates = new ArrayList<>();
        if(date == null) {
            daoFactory.getCreditDao().getAll().forEach(c->localDates.add(c.getDate()));
            return Collections.min(localDates);
        }
        return date;
    }

    public void toCalculate() throws FileNotFoundException {
        LocalDate from = checkDateFromByNull(daoFactory.getSettingsDao().getSettings().getDateFrom());
        LocalDate to = daoFactory.getSettingsDao().getSettings().getDateTo();
        daoFactory.getUserDao().getAll().forEach(ThrowableConsumer.check(u->
            daoFactory.getCreditDao().getCreditsByUserId(u.getId()).stream()
                    .filter(c-> to==null? c.getDate().plusDays(1).isAfter(from) : c.getDate().plusDays(1).isAfter(from) && c.getDate().minusDays(1).isBefore(to))
                    .forEach(ThrowableConsumer.check(c -> {
                        if(c.getMoney().intValue()!=0){
                            List<Transaction> list = daoFactory.getTransactionDao().getTransactionsByUserIdAndCreditId(u.getId(), c.getId())
                                    .stream().sorted(Comparator.comparing(Transaction::getDate)).collect(Collectors.toList());
                            if(!list.isEmpty()) {
                                LocalDate date = to==null ? list.stream().max(Comparator.comparing(Transaction::getDate)).get().getDate() : to;
                                Credit credit = toCalculateCredit(list, c, date);
                                addUserWhoInTimeFrame(credit, u);
                            }
                        }else {
                            c.setDeptRepaymentDate(c.getDate());
                            addUserWhoInTimeFrame(c,u);
                        }
                    }))));
        addUserWhoIsNotInTimeFrame();
        showTable();
    }

    private void addUserWhoInTimeFrame(Credit credit, User user) throws FileNotFoundException {
        Show show = daoFactory.getSettingsDao().getSettings().getShowFor();
        if(show==null){
            addInfoInList(credit, Optional.ofNullable(user));
        }else {
            if (show.getType().equals(TypeShowFor.ID)) {
                daoFactory.getSettingsDao().getSettings().getShowFor().getUsers().stream().filter(u -> user.getId() == Long.parseLong(u))
                        .forEach(ThrowableConsumer.check(u -> {
                            Optional<User> userById = daoFactory.getUserDao().getById(Long.parseLong(u));
                            addInfoInList(credit, userById);
                        }));
            } else if (show.getType().equals(TypeShowFor.NAME)) {
                String name = user.getName() + " " + user.getSecondName();
                daoFactory.getSettingsDao().getSettings().getShowFor().getUsers().stream().filter(u -> u.equalsIgnoreCase(name))
                        .forEach(ThrowableConsumer.check(u -> {
                            Optional<User> userByName = daoFactory.getUserDao().getByName(u);
                            addInfoInList(credit, userByName);
                        }));

            }
        }
    }

    private void addInfoInList(Credit credit, Optional<User> user){
        if(user.isPresent()) {
            Info info = new Info();
            info.setName(user.get().getName() + " " + user.get().getSecondName());
            info.setBirthday(user.get().getBirthday());
            info.setAmountTransactions(credit.getAmountTransactions());
            info.setDeptRepaymentDate(credit.getDeptRepaymentDate());
            info.setIdCredit(String.valueOf(credit.getId()));
            info.setIdUser(String.valueOf(credit.getUserId()));
            info.setMoney(credit.getMoney());
            info.setPeriod(credit.getPeriod());
            if (credit.getMoney().intValue() == 0) info.setStatus(Status.DONE);
            else info.setStatus(Status.IN_PROGRESS);
            infoList.add(info);
        }
    }

    private void addUserWhoIsNotInTimeFrame() throws FileNotFoundException {
        if(daoFactory.getSettingsDao().getSettings().getShowFor()!=null) {
            if (daoFactory.getSettingsDao().getSettings().getShowFor().getType().equals(TypeShowFor.ID)) {
                daoFactory.getSettingsDao().getSettings().getShowFor().getUsers().forEach(ThrowableConsumer.check(u -> {
                    if (!containsId(infoList, u)) {
                        Optional<User> userById = daoFactory.getUserDao().getById(Long.parseLong(u));
                        if (userById.isPresent()) {
                            Info info = new Info();
                            info.setName(userById.get().getName() + " " + userById.get().getSecondName());
                            infoList.add(info);
                        }
                    }
                }));
            } else {
                daoFactory.getSettingsDao().getSettings().getShowFor().getUsers().forEach(ThrowableConsumer.check(u -> {
                    if (!containsName(infoList, u)) {
                        Optional<User> userByName = daoFactory.getUserDao().getByName(u);
                        if (userByName.isPresent()) {
                            Info info = new Info();
                            info.setName(userByName.get().getName() + " " + userByName.get().getSecondName());
                            infoList.add(info);
                        }
                    }
                }));
            }
        }else {
            daoFactory.getUserDao().getAll().forEach(ThrowableConsumer.check(u -> {
                if (!containsId(infoList, String.valueOf(u.getId()))) {
                    Info info = new Info();
                    info.setName(u.getName() + " " + u.getSecondName());
                    infoList.add(info);
                }
            }));
        }
    }

    private boolean containsName(final List<Info> list, final String name){
        return list.stream().filter(i->Objects.nonNull(i.getName())).map(Info::getName).anyMatch(name::equalsIgnoreCase);
    }

    private boolean containsId(final List<Info> list, final String id){
        return list.stream().filter(i->Objects.nonNull(i.getIdUser())).map(Info::getIdUser).anyMatch(id::equals);
    }

    private Credit setMoneyForCredit(List<Transaction> list, Credit credit,LocalDate date, LocalDate minusPeriodDate, LocalDate plusPeriodDate, LocalDate end){
        List<Transaction> transactionsInRateDay = list.stream().filter(d-> d.getDate().equals(date)).collect(Collectors.toList());
        List<Transaction> transactionsBeforeRateDay = list.stream().filter(d-> d.getDate().isBefore(date) && d.getDate().isAfter(minusPeriodDate)).collect(Collectors.toList());
        if(credit.getMoney().intValue()!=0) {
            if (!transactionsBeforeRateDay.isEmpty()) {
                toCalculateTransactions(transactionsBeforeRateDay, credit);
            }
            if (!transactionsInRateDay.isEmpty()) {
                credit.setMoney(setMoneyWithRate(credit.getMoney(), credit.getRate(), date));
                toCalculateTransactions(transactionsInRateDay, credit);
            }
            if (credit.getMoney().intValue() != 0) {
                credit.setMoney(setMoneyWithRate(credit.getMoney(), credit.getRate(), date));
            }
            if(plusPeriodDate.isAfter(end)){
                List<Transaction> transactionsAfterLastRateDay = list.stream().filter(d->d.getDate().isAfter(date) && d.getDate().isBefore(end.plusDays(1))).collect(Collectors.toList());
                toCalculateTransactions(transactionsAfterLastRateDay, credit);
            }
        }
        return credit;
    }

    private Credit toCalculateTransactions(List<Transaction> transactions, Credit credit){
        transactions.forEach(ThrowableConsumer.check(t -> {
            credit.setMoney(credit.getMoney().subtract(repayCredit(t)));
            credit.setAmountTransactions(credit.getAmountTransactions() + 1);
            if(credit.getMoney().intValue()<=0) {
                credit.setMoney(new BigDecimal("0"));
                if(credit.getDeptRepaymentDate()==null)
                    credit.setDeptRepaymentDate(t.getDate());
            }
        }));
        return credit;
    }

    private Credit toCalculateCredit(List<Transaction> transactions, Credit credit, LocalDate end)  {
        if(credit.getPeriod().equals(Period.WEEK)){
            Stream.iterate(credit.getDate().plusWeeks(1),date -> date.plusWeeks(1)).limit(ChronoUnit.WEEKS.between(credit.getDate().plusWeeks(1),end)+1)
                    .forEach(ThrowableConsumer.check(date ->
                            daoFactory.getCreditDao().update(setMoneyForCredit(transactions, credit, date, date.minusWeeks(1),date.plusWeeks(1),end))));
        }else if(credit.getPeriod().equals(Period.DAY)){
            Stream.iterate(credit.getDate().plusDays(1), date ->date.plusDays(1)).limit(ChronoUnit.DAYS.between(credit.getDate().plusDays(1),end)+1)
                    .forEach(ThrowableConsumer.check(date ->
                            daoFactory.getCreditDao().update(setMoneyForCredit(transactions, credit, date, date.minusDays(1),date.plusDays(1),end))));
        }else if(credit.getPeriod().equals(Period.YEAR)){
            LocalDate start = checkLeapYear(credit.getDate().plusYears(1));
            Stream.iterate(start, date ->date = checkLeapYear(date.plusYears(1))).limit(ChronoUnit.YEARS.between(start,end)+1)
                    .forEach(ThrowableConsumer.check(date ->
                            daoFactory.getCreditDao().update(setMoneyForCredit(transactions, credit, date, checkLeapYear(date.minusYears(1)),
                                    checkLeapYear(date.plusYears(1)), end))));
        }else if(credit.getPeriod().equals(Period.MONTH)){
            LocalDate start = checkLastDayOfMonth(credit.getDate());
            Stream.iterate(start, date -> date = checkLastDayOfMonth(date)).limit(ChronoUnit.MONTHS.between(start,end)+1)
                    .forEach(ThrowableConsumer.check(date ->
                        daoFactory.getCreditDao().update(setMoneyForCredit(transactions,credit,date,checkLastDayOfMonth(date.minusMonths(2)),
                                checkLastDayOfMonth(date),end))));
        }
        return credit;
    }

    private LocalDate checkLeapYear(LocalDate date){
        GregorianCalendar calendar = new GregorianCalendar();
        LocalDate nextYear;
        if (date.getMonth().equals(Month.FEBRUARY)){
            if(calendar.isLeapYear(date.getYear())){
                if(date.getDayOfMonth() == date.getMonth().maxLength() || date.getDayOfMonth() == date.getMonth().minLength())
                    nextYear = LocalDate.of(date.getYear(), date.getMonth(), date.getMonth().maxLength());
                else nextYear = date;
            }else if(date.getDayOfMonth() == date.getMonth().minLength()){
                nextYear = LocalDate.of(date.getYear(), date.getMonth(), date.getMonth().minLength());
            }else nextYear = date;
        }else {
            nextYear = date;
        }
        return nextYear;
    }

    private LocalDate checkLastDayOfMonth(LocalDate date){
        GregorianCalendar calendar = new GregorianCalendar();
        LocalDate nextMonth;
        if(date.getMonth().equals(Month.FEBRUARY) || date.plusMonths(1).getMonth().equals(Month.FEBRUARY)){
            if(calendar.isLeapYear(date.getYear())){
                if(date.getDayOfMonth() == date.getMonth().maxLength() || date.getDayOfMonth() == date.getMonth().minLength())
                    nextMonth = LocalDate.of(date.plusMonths(1).getYear(), date.plusMonths(1).getMonth(), date.plusMonths(1).getMonth().maxLength());
                else nextMonth = date.plusMonths(1);
            }else if(date.getDayOfMonth() == date.getMonth().minLength()){
                nextMonth = LocalDate.of(date.plusMonths(1).getYear(), date.plusMonths(1).getMonth(), date.plusMonths(1).getMonth().minLength());
            }else nextMonth = date.plusMonths(1);
        }else {
            if (date.getDayOfMonth()==date.getMonth().maxLength()){
                nextMonth = LocalDate.of(date.plusMonths(1).getYear(), date.plusMonths(1).getMonth(), date.plusMonths(1).getMonth().maxLength());
            }else nextMonth = date.plusMonths(1);
        }
        return nextMonth;
    }

    private BigDecimal repayCredit(Transaction transaction) throws FileNotFoundException {
        BigDecimal money = null;
        Optional<Event> event = daoFactory.getEventDao().getEventByDateAndCurrency(transaction.getDate(), transaction.getCurrency());
        if(!event.isPresent()){
            if(transaction.getCurrency().equals(Currency.EUR)){
                BigDecimal cost = daoFactory.getSettingsDao().getSettings().getStartCostEUR();
                money = cost.multiply(transaction.getMoney());
            }else if(transaction.getCurrency().equals(Currency.USD)){
                BigDecimal cost = daoFactory.getSettingsDao().getSettings().getStartCostUSD();
                money = cost.multiply(transaction.getMoney());
            }
            else if(transaction.getCurrency().equals(Currency.BR)){
                money = transaction.getMoney();
            }
        }else {
            if (event.get().getCurrency().equals(Currency.BR)) {
                money = transaction.getMoney();
            } else money = event.get().getCost().multiply(transaction.getMoney());
        }
        return money;
    }

    private BigDecimal setMoneyWithRate(BigDecimal money, BigDecimal rate, LocalDate date) {
        BigDecimal newMoney = money;
        BigDecimal newRate = rate;
        List<Discount> discounts = daoFactory.getDiscountDao().getDiscountsByDate(date);
        if(!discounts.isEmpty()) {
            BigDecimal discount = setDiscount(discounts);
            newRate = newRate.subtract(discount).intValue() <= 0 ? new BigDecimal("0") : newRate.subtract(discount);
        }
        BigDecimal percent = newMoney.multiply(newRate).divide(new BigDecimal("100"));
        newMoney = newMoney.add(percent).setScale(2,BigDecimal.ROUND_HALF_UP);
        return newMoney;
    }

    private BigDecimal setDiscount(List<Discount> discounts){
        BigDecimal newDiscount = new BigDecimal("0");
        for (Discount d: discounts)
            newDiscount = newDiscount.add(d.getDiscount());
        return newDiscount;
    }

    private void showTable() throws FileNotFoundException {
        StringBuilder table = new StringBuilder();
        table.append(String.format("%-9s","Id credit")).append("|");
        table.append(String.format("%-7s","Id user")).append("|");
        table.append(String.format("%-20s","Name")).append("|");
        table.append(String.format("%-12s","Tran. amount")).append("|");
        table.append(String.format("%-20s","Debt")).append("|");
        table.append(String.format("%-6s","Period")).append("|");
        table.append(String.format("%-17s","Status")).append("|").append("\n");
        Sort sort = daoFactory.getSettingsDao().getSettings().getSortBy();
        if(!infoList.isEmpty()){
            if(sort.equals(Sort.AGE))
                infoList.stream().sorted(Comparator.comparing(Info::getBirthday, Comparator.nullsLast(LocalDate::compareTo))).forEach(i-> table.append(i.toString()));
            else if(sort.equals(Sort.DEBT))
                infoList.stream().sorted(Comparator.comparing(Info::getMoney,Comparator.nullsLast(BigDecimal::compareTo))).forEach(i-> table.append(i.toString()));
            else if(sort.equals(Sort.NAME))
                infoList.stream().sorted(Comparator.comparing(Info::getName, Comparator.nullsLast(String::compareTo))).forEach(i-> table.append(i.toString()));
        }
        System.out.println(table.toString());
    }
}
