package bankWorker;

import com.google.gson.*;
import dao.DaoFactory;
import dao.json.JsonWorker;
import entity.Settings;
import exсeptions.ThrowableConsumer;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class DepartmentsReader {

    private final String pathToDataBase;
    private final String pathToSettings;
    private final String pathToData;
    private List<Path> paths;
    private final DaoFactory daoFactory = DaoFactory.getDaoFactory();

    public DepartmentsReader(String path) {
        pathToData = path;
        pathToDataBase = path + "db.json";
        pathToSettings = path + "settings.json";
        this.paths = new ArrayList<>();
    }

    public boolean read() throws IOException {
        if(Objects.nonNull(pathToData)){
            if(checkFileExistence()) {
                JsonWorker.getInstance(pathToData);
                if (daoFactory.getSettingsDao().checkSettings()) {
                    Settings settings = daoFactory.getSettingsDao().getSettings();
                    if (settings.getUseDepartments() == null) {
                        moveTransactionsInDB("glob:**/db_*[a-z,A-Z,0-9].json");
                        return true;
                    }else if (!settings.getUseDepartments().isEmpty()) {
                        settings.getUseDepartments().forEach(ThrowableConsumer.check((i) -> moveTransactionsInDB("glob:**/db_" + i + ".json")));
                        return true;
                    }else if(settings.getUseDepartments().isEmpty()){
                        return true;
                    }
                } else {
                    System.exit(1);
                }
            }
        }else {
            System.out.println("Data is not found");
            return false;
        }
        return false;
    }

    private boolean checkFileExistence(){
        if(!Files.exists(Paths.get(pathToDataBase))) {
            System.out.println("File \"db.json\" is not found");
            return false;
        }
        if(!Files.exists(Paths.get(pathToSettings))){
            System.out.println("File \"settings.json\" is not found");
            return false;
        }
        return true;
    }

    private void moveTransactionsInDB(String pattern) throws IOException {
        final PathMatcher pathMatcher = Paths.get(pathToData).getFileSystem().getPathMatcher(pattern);
        paths = Files.list(Paths.get(pathToData)).filter(pathMatcher::matches).collect(Collectors.toList());
        paths.forEach(ThrowableConsumer.check(p->readDepartment(p.toAbsolutePath().toString())));
    }

    private void readDepartment(String path) throws IOException {
        Gson gson = new GsonBuilder().registerTypeAdapter(LocalDate.class, (JsonDeserializer<LocalDate>) (jsonElement, type, jsonDeserializationContext)
                -> LocalDate.parse(jsonElement.getAsJsonPrimitive().getAsString())).setPrettyPrinting().create();
        JsonObject jsonObject = new JsonParser().parse(new FileReader(path)).getAsJsonObject();
        JsonArray array = jsonObject.getAsJsonArray("transactions");
        Iterator<JsonElement> iterator = array.iterator();
        while (iterator.hasNext()){
            try(FileWriter fileWriter = new FileWriter(path)) {
                JsonElement element = iterator.next();
                writeTransactionsToDataBase(element);
                iterator.remove();
                fileWriter.write(gson.toJson(jsonObject));
                fileWriter.flush();
            }
        }
    }

    private void writeTransactionsToDataBase(JsonElement element) throws IOException {
        Gson gson =new GsonBuilder().setPrettyPrinting().create();
        JsonObject jsonObject = new JsonParser().parse(new FileReader(pathToDataBase)).getAsJsonObject();
        JsonObject jsonObjectData = jsonObject.getAsJsonObject("data");
        JsonArray array = jsonObjectData.getAsJsonArray("transactions");
        try(FileWriter fileWriter = new FileWriter(pathToDataBase)){
            array.add(element);
            fileWriter.write(gson.toJson(jsonObject));
            fileWriter.flush();
        }
    }
}
