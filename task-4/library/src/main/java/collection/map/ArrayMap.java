package collection.map;
import collection.abstractList.Array;
import collection.entity.Entity;
import collection.entity.Node;
import collection.list.ArrayList;
import collection.list.List;

public class ArrayMap<K,V> implements Map<K,V> {

    private static final int DEFAULT_INITIAL_CAPACITY = 16;
    private static final int MAXIMUM_CAPACITY = 1 << 30;
    private static final float DEFAULT_LOAD_FACTOR = 0.75f;

    private Array<Node<K,V>> table;

    private int size;
    private int threshold;
    private final float loadFactor;

    public ArrayMap(int initialCapacity, float loadFactor){
        if(initialCapacity<0)
            throw new IllegalArgumentException("Illegal initial capacity"
            + initialCapacity);
        if(initialCapacity>MAXIMUM_CAPACITY)
            initialCapacity = MAXIMUM_CAPACITY;
        if(loadFactor<=0 || Float.isNaN(loadFactor))
            throw new IllegalArgumentException("Illegal load factor"
                    + loadFactor);

        int capacity = 1;
        while (capacity<initialCapacity)
            capacity <<=1;

        this.loadFactor = loadFactor;
        threshold = (int) (loadFactor * capacity);
        table = new Array<>();
    }

    public ArrayMap(int initialCapacity){
        if(initialCapacity<0)
            throw new IllegalArgumentException("Illegal initial capacity"
                    + initialCapacity);
        if(initialCapacity>MAXIMUM_CAPACITY)
            initialCapacity = MAXIMUM_CAPACITY;

        int capacity = 1;
        while (capacity<initialCapacity)
            capacity <<=1;

        this.loadFactor = DEFAULT_LOAD_FACTOR;
        threshold = (int) (loadFactor * capacity);
        table = new Array<>(capacity);
    }

    public ArrayMap(){
        loadFactor = DEFAULT_LOAD_FACTOR;
        threshold = (int) (DEFAULT_INITIAL_CAPACITY * DEFAULT_LOAD_FACTOR);
        table = new Array<>(DEFAULT_INITIAL_CAPACITY);
    }

    @Override
    public boolean isEmpty() {
        return size==0;
    }

    @Override
    public void set(K key, V value) throws Exception {
        if(key==null || value==null) return;
        if(contains(value)) return;
        if ( containsKey(key))
            throw new IllegalArgumentException("This key is already in the map: "+ key);
        int hash = hash(key.hashCode());
        int i = indexFor(hash,table.size());
        for(Node<K,V> e = table.get(i); e != null; e = e.getNext()){
            Object k;
            if(e.getHash() == hash && ((k = e.getKey())== key || key.equals(k))){
                e.setValue(value);
            }
        }
        addEntry(hash,key,value,i);
    }

    private void addEntry(int hash, K key, V value, int bucketIndex) throws Exception {
        Node<K,V> e = table.get(bucketIndex);
        table.set(bucketIndex,new Node<K,V>(hash, key, value, e));
        if(size++ >= threshold)
            resize(2*table.size());
    }

    @Override
    public void set(Entity entity) throws Exception {
        set((K)entity.getKey(),(V)entity.getValue());
    }

    @Override
    public List getKeys() {
        List<K> keys = (List<K>) new ArrayList<K>();
        for(int i = 0; i < table.size(); i++){
            Node node = table.get(i);
            if(node!=null) {
                K key = table.get(i).getKey();
                if (key != null)
                    keys.add(key);
            }
        }
        return keys;
    }

    @Override
    public List getValues() {
        List<V> values = new ArrayList<>();
        for(int i = 0; i < table.size(); i++){
            Node node = table.get(i);
            if(node!=null) {
                V value = table.get(i).getValue();
                if (value != null)
                    values.add(value);
            }
        }
        return values;
    }

    @Override
    public V get(K key) {
        int hash = hash(key.hashCode());
        int i = indexFor(hash,table.size());
        for (Node<K,V> e = table.get(i); e!=null; e = e.getNext()){
            Object k;
            if(e.getHash() == hash && ((k = e.getKey()) == key || key.equals(k))){
                return e.getValue();
            }
        }
        return null;
    }

    @Override
    public Node getEntity(K key) {
        int hash = hash(key.hashCode());
        for (Node<K,V> e = table.get(indexFor(hash,table.size()));
             e!=null; e = e.getNext()){
            Object k;
            if(e.getHash() == hash && ((k = e.getKey()) == key || key.equals(k))){
                return e;
            }
        }
        return null;
    }

    @Override
    public Entity remove(Entity entity) {
        return remove((K) entity.getKey());
    }

    @Override
    public Node remove(K key) {
        int hash = hash(key.hashCode());
        int index = indexFor(hash,table.size());
        Node<K,V> prev = table.get(index);
        Node<K,V> node = prev;
        while (node!=null){
            Node<K,V> next = node.getNext();
            Object k;
            if (node.getHash() == hash && ((k=node.getKey())==key||
                    (key!=null&& key.equals(k)))) {
                size--;
                if (prev == node)
                    table.set(index,next);
                else
                    prev.setNext(next);
                return node;
            }
            prev = node;
            node = next;
        }
        return null;
    }

    @Override
    public boolean contains(V value) {
        if(value==null)
            throw new IllegalArgumentException("Illegal null" + null);
        Array<Node<K,V>> tab = table;
        for(int i=0;i<tab.size();i++)
            for(Node<K,V> node = tab.get(i); node!=null;node =  node.getNext())
                if(value.equals(node.getValue()))
                    return true;
        return false;
    }

    private boolean containsKey(K key){
        return getEntity(key)!=null;
    }

    @Override
    public int clear() {
        table.clear();
        return 0;
    }

    @Override
    public int size() {
        return size;
    }

    private static int hash(Object key){
        int h;
        return (key == null) ? 0 : (h = key.hashCode()) ^ (h >>> 16);
    }

    private static int indexFor(int h, int length){
        return h & (length-1);
    }

    private void resize(int newCapacity) throws Exception {
        Array<Node<K,V>> oldTable = table;
        int oldCapacity = oldTable.size();
        if(oldCapacity==MAXIMUM_CAPACITY){
            threshold = Integer.MAX_VALUE;
            return;
        }
        table = new Array<>(newCapacity);
        threshold = (int) (newCapacity*loadFactor);
        size=0;
        toRedistribute(oldTable);
    }

    private void toRedistribute(Array<Node<K,V>> oldTable) throws Exception {
        for (int i=0;i<oldTable.size();i++) {
            Node node = oldTable.get(i);
            if(node!=null){
                set((K)node.getKey(),(V)node.getValue());
            }
        }
    }
}
