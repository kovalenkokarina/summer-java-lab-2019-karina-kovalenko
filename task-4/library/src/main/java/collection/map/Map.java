package collection.map;
import collection.entity.Entity;
import collection.list.List;

public interface Map<K,V> {

    boolean isEmpty();

    void set(K key, V value) throws Exception;

    void set(Entity<K,V> entity) throws Exception;

    List getKeys();

    List getValues();

    V get(K key);

    Entity getEntity(K key);

    Entity remove(Entity entity);

    Entity remove(K key);

    boolean contains(V value);

    int clear();

    int size();
}
