package collection.map;

import collection.entity.Entity;
import collection.list.ArrayList;
import collection.list.List;

public class ListMap<K,V> implements Map<K,V> {

    private static final int DEFAULT_INITIAL_CAPACITY = 16;
    private static final int MAXIMUM_CAPACITY = 1 << 30;
    private static final float DEFAULT_LOAD_FACTOR = 0.75f;
    private Entry<K,V>[] table;
    private int capacity = DEFAULT_INITIAL_CAPACITY;
    private Entry<K,V> header;
    private Entry<K,V> last;

    private int size;
    private int threshold;
    private final float loadFactor;

    public ListMap(int initialCapacity, float loadFactor){
        if(initialCapacity<0)
            throw new IllegalArgumentException("Illegal initial capacity"
                    + initialCapacity);
        if(initialCapacity>MAXIMUM_CAPACITY)
            initialCapacity = MAXIMUM_CAPACITY;
        if(loadFactor<=0 || Float.isNaN(loadFactor))
            throw new IllegalArgumentException("Illegal load factor"
                    + loadFactor);

        int capacity = 1;
        while (capacity<initialCapacity)
            capacity <<=1;

        this.loadFactor = loadFactor;
        threshold = (int) (loadFactor * capacity);
        table = new Entry[capacity];
    }

    public ListMap(int initialCapacity){
        if(initialCapacity<0)
            throw new IllegalArgumentException("Illegal initial capacity"
                    + initialCapacity);
        if(initialCapacity>MAXIMUM_CAPACITY)
            initialCapacity = MAXIMUM_CAPACITY;

        int capacity = 1;
        while (capacity<initialCapacity)
            capacity <<=1;

        this.loadFactor = DEFAULT_LOAD_FACTOR;
        threshold = (int) (loadFactor * capacity);
        table = new Entry[capacity];
    }

    public ListMap(){
        loadFactor = DEFAULT_LOAD_FACTOR;
        threshold = (int) (DEFAULT_INITIAL_CAPACITY * DEFAULT_LOAD_FACTOR);
        table = new Entry[DEFAULT_INITIAL_CAPACITY];
    }

    @Override
    public boolean isEmpty() {
        return size==0;
    }

    @Override
    public void set(K key, V value) throws Exception {
        if(key==null || value==null) return;
        if(contains(value)) return;
        if ( containsKey(key))
            throw new IllegalArgumentException("This key is already in the map: "+ key);
        int hash = hash(key.hashCode());
        Entry<K,V> newEntry = new Entry<>(key, value, null);
        maintainOrderAfterInsert(newEntry);
        if(table[hash] == null){
            table[hash] = newEntry;
        }else{
            Entry<K,V> prev = null;
            Entry<K,V> node = table[hash];
            while(node != null){
                if(node.key.equals(key)){
                    if(prev==null){
                        newEntry.next=node.next;
                        table[hash]=newEntry;
                        return;
                    }
                    else{
                        newEntry.next=node.next;
                        prev.next=newEntry;
                        return;
                    }
                }
                prev=node;
                node = node.next;

            }
            prev.next = newEntry;
        }
        if(size++ >= threshold)
            resize(2*table.length);
    }

    @Override
    public void set(Entity entity) throws Exception {
        set((K)entity.getKey(),(V)entity.getValue());
    }

    @Override
    public List getKeys() {
        List<K> keys = new ArrayList<>();
        for(int i = 0; i <= size; i++){
            Entry<K,V> entry = table[i];
            if(entry!=null) {
                K key = entry.key;
                if (key != null)
                    keys.add(key);
            }
        }
        return keys;
    }

    @Override
    public List getValues() {
        List<V> values = new ArrayList<>();
        for(int i = 0; i <= size; i++){
            Entry<K,V> entry = table[i];
            if(entry!=null) {
                V value = entry.value;
                if (value != null)
                    values.add(value);
            }
        }
        return values;
    }

    @Override
    public V get(K key) {
        int hash = hash(key.hashCode());
        if(table[hash]==null) return null;
        else {
            Entry<K,V> entry = table[hash];
            while(entry!= null){
                if(entry.key.equals(key))
                    return entry.value;
                entry = entry.next;
            }
        }
        return null;
    }

    @Override
    public Entry<K, V> getEntity(K key) {
        int hash = hash(key.hashCode());
        if(table[hash]==null) return null;
        else {
            Entry<K,V> entry = table[hash];
            while(entry!= null){
                if(entry.key.equals(key))
                    return entry;
                entry = entry.next;
            }
        }
        return null;
    }

    @Override
    public Entity remove(Entity entity) {
        return remove((K)entity.getKey());
    }

    @Override
    public Entry remove(K key) {
        int hash = hash(key.hashCode());
        if(table[hash] == null) return null;
        else{
            Entry<K,V> previous = null;
            Entry<K,V> current = table[hash];

            while(current != null){ //we have reached last entry node of bucket.
                if(current.key.equals(key)){
                    maintainOrderAfterDeletion(current);
                    if(previous==null){
                        Entry<K,V> oldEntry = table[hash];
                        table[hash]=table[hash].next;
                        size--;
                        return oldEntry;
                    }
                    else{
                        previous.next=current.next;
                        return previous.next;
                    }
                }
                previous=current;
                current = current.next;
            }
            return null;
        }
    }

    @Override
    public boolean contains(V value) {
        for(int i = 0; i <= size; i++){
            Entry<K,V> entry = table[i];
            if(entry!=null) {
                V v = entry.value;
                if (v.equals(value))
                    return true;
            }
        }
        return false;
    }

    private boolean containsKey(K key){
        if(getKeys().find(key)!=-1)
            return true;
        return false;
    }

    @Override
    public int clear() {
        for (int i=0;i<capacity;i++){
            table[i] = null;
        }
        return size;
    }

    @Override
    public int size() {
        return size;
    }

    private int hash(Object key){
        return Math.abs(key.hashCode())%this.capacity;
    }

    private void resize(int newCapacity) throws Exception {
        Entry<K,V> [] oldTable= table;
        int oldCapacity = oldTable.length;
        if(oldCapacity==MAXIMUM_CAPACITY){
            threshold = Integer.MAX_VALUE;
            return;
        }
        table = new Entry[capacity];
        threshold = (int) (newCapacity*loadFactor);
        size=0;
        toRedistribute(oldTable);
    }

    private void toRedistribute(Entry<K,V> [] oldTable) throws Exception {
        for (Entry entry : oldTable) {
            if (entry != null) {
                set((K) entry.getKey(), (V) entry.getValue());
            }
        }
    }

    private void maintainOrderAfterInsert(Entry<K, V> newEntry) {
        if(header==null){
            header=newEntry;
            last=newEntry;
            return;
        }
        if(header.key.equals(newEntry.key)){
            deleteFirst();
            insertFirst(newEntry);
            return;
        }
        if(last.key.equals(newEntry.key)){
            deleteLast();
            insertLast(newEntry);
            return;
        }
        Entry<K, V> beforeDeleteEntry=deleteSpecificEntry(newEntry);
        if(beforeDeleteEntry==null) insertLast(newEntry);
        else insertAfter(beforeDeleteEntry,newEntry);
    }

    private void maintainOrderAfterDeletion(Entry<K, V> deleteEntry) {
        if(header.key.equals(deleteEntry.key)){
            deleteFirst();
            return;
        }
        if(last.key.equals(deleteEntry.key)){
            deleteLast();
            return;
        }
        deleteSpecificEntry(deleteEntry);
    }

    private void insertAfter(Entry<K, V> beforeDeleteEntry, Entry<K, V> newEntry) {
        Entry<K, V> current=header;
        while(current!=beforeDeleteEntry){
            current=current.after;
        }

        newEntry.after=beforeDeleteEntry.after;
        beforeDeleteEntry.after.before=newEntry;
        newEntry.before=beforeDeleteEntry;
        beforeDeleteEntry.after=newEntry;
    }

    private void deleteFirst(){
        if(header==last){ //only one entry found.
            header=last=null;
            return;
        }
        header=header.after;
        header.before=null;
    }

    private void insertFirst(Entry<K, V> newEntry){
        if(header==null){
            header=newEntry;
            last=newEntry;
            return;
        }
        newEntry.after=header;
        header.before=newEntry;
        header=newEntry;
    }

    private void insertLast(Entry<K, V> newEntry){
        if(header==null){
            header=newEntry;
            last=newEntry;
            return;
        }
        last.after=newEntry;
        newEntry.before=last;
        last=newEntry;
    }

    private void deleteLast(){
        if(header==last){
            header=last=null;
            return;
        }
        last=last.before;
        last.after=null;
    }

    private Entry<K, V> deleteSpecificEntry(Entry<K, V> newEntry){
        Entry<K, V> current=header;
        while(!current.key.equals(newEntry.key)){
            if(current.after==null) return null;
            current=current.after;
        }
        Entry<K, V> beforeDeleteEntry=current.before;
        current.before.after=current.after;
        current.after.before=current.before;
        return beforeDeleteEntry;
    }

    public static class Entry<K,V> implements Entity<K,V> {
        K key;
        V value;
        Entry<K,V> next;
        Entry<K,V> before,after;

        public Entry(K key, V value, Entry<K,V> next){
            this.key = key;
            this.value = value;
            this.next = next;
        }

        @Override
        public K getKey() {
            return null;
        }

        @Override
        public V getValue() {
            return null;
        }

        @Override
        public String toString() {
            return "Key: " + key + ", value: " + value;
        }
    }
}
