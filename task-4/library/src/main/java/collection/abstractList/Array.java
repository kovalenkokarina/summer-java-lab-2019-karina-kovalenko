package collection.abstractList;

import collection.Collection;
import collection.Iterator;
import java.util.Arrays;
import java.util.NoSuchElementException;

public class Array<E> implements Collection<E> {

    private Object[] objects;
    private int size;

    public Array(int capacity){
        if(capacity>0){
            objects = new Object[capacity];
            size = capacity;
        }else if (capacity==0){
            objects = new Object[]{};
        }
    }

    public Array(Object [] array){
        objects = new Object[]{};
        addAll((E[]) array);
    }

    public Array(Collection<? extends E> col){
        objects = new Object[]{};
        addAll(col);
    }

    public Array() {
        objects = new Object[]{};
    }

    private void resize(int newCapacity){
        objects = Arrays.copyOf(objects,newCapacity);
    }

    @Override
    public Iterator<E> getIterator() {
        return new Iter();
    }

    @Override
    public Object[] toArray() {
        return Arrays.copyOf(objects, size);
    }

    public int add(E e){
        resize(size+1);
        int index = size++;
        objects[index] = e;
        return index;
    }

    public void add(int index, E e){
        resize(size+1);
        System.arraycopy(objects, index, objects, index + 1, size - index);
        objects[index] = e;
        size++;
    }

    public void addAll(E[] array) {
        int arraySize = array.length;
        resize(size+arraySize);
        System.arraycopy(array,0,objects,size,arraySize);
        size+=arraySize;
    }

    public void addAll(Collection<? extends E> c){
        E [] array = (E[]) c.toArray();
        addAll(array);
    }

    public E set(int index, E e) {
        checkIndex(index);
        E oldElement = get(index);
        objects[index] = e;
        return oldElement;
    }

    public E remove(int index) {
        checkIndex(index);
        E oldElement = get(index);
        int numMoved = size - index -1;
        if(numMoved>0)
            System.arraycopy(objects,index+1,objects,index,numMoved);
        objects[--size] = null;
        return oldElement;
    }

    public int find(E e) {
        for (int i=0;i<size;i++){
            if (objects[i]==e){
                return i;
            }
        }
        return -1;
    }

    public E get(int index) {
        checkIndex(index);
        return (E) objects[index];
    }

    public <T> T[] toArray(T[] array) {
        if (array.length < size)
            return (T[]) Arrays.copyOf(objects, size, array.getClass());
        System.arraycopy(objects, 0, array, 0, size);
        if (array.length > size)
            array[size] = null;
        return array;
    }

    public int size() {
        return size;
    }

    public void trim() {
        Object [] newArray = new Object[]{};
        int newSize = newArray.length;
        for(int i=0;i<size;i++){
            if (objects[i]!=null){
                newArray = Arrays.copyOf(newArray,newSize+1);
                newArray[newSize++] = objects[i];
            }
        }
        objects = Arrays.copyOf(newArray,newArray.length);
        size = objects.length;
    }

    public void clear(){
        for(int i=0;i<size;i++){
            objects[i]=null;
        }
        size = 0;
    }

    private void checkIndex(int index){
        if (index >= size)
            throw new IndexOutOfBoundsException("Index: " + index + " Size: " +size);
    }

    private class Iter implements Iterator<E>{

        private int currentIndex;
        private int lastIndex=-1;

        Iter() {}

        @Override
        public E getNext() {
            int i = currentIndex;
            if(i>=size)
                throw new NoSuchElementException();
            lastIndex = i;
            currentIndex++;
            return (E) objects[i];
        }

        @Override
        public boolean hasNext() {
            return currentIndex!=size;
        }

        @Override
        public void remove() {
            Array.this.remove(lastIndex);
            currentIndex = lastIndex;
            lastIndex = -1;
        }

        @Override
        public void addBefore(E e) {
            int i = currentIndex;
            Array.this.add(i-1,e);
        }

        @Override
        public void addAfter(E e) {
            int i = currentIndex;
            Array.this.add(i+1,e);
        }
    }
}

