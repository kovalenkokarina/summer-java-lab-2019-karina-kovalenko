package collection.abstractList;

import collection.Collection;
import collection.Iterator;
import collection.ListIterator;

import java.util.NoSuchElementException;

public class LinkList<E> implements Collection<E> {

    private int size;

    Node<E> first;
    Node<E> last;

    public LinkList() {

    }

    public LinkList(Object [] objects){
        addAll((E[]) objects);
    }

    private void linkLast(E e) {
        Node<E> l = last;
        Node<E> newNode = new Node<>(l, e, null);
        last = newNode;
        if (l == null) first = newNode;
        else l.next = newNode;
        size++;
    }

    private E unlink(Node<E> node) {
        final E element = node.item;
        final Node<E> next = node.next;
        final Node<E> prev = node.prev;
        if (prev == null) {
            first = next;
        } else {
            prev.next = next;
            node.prev = null;
        }
        if (next == null) {
            last = prev;
        } else {
            next.prev = prev;
            node.next = null;
        }
        node.item = null;
        size--;
        return element;
    }

    @Override
    public Iterator<E> getIterator() {
        return new Iter();
    }

    public Iterator<E> getListIterator(){
        return new Iter(size-1);
    }

    @Override
    public int add(E e){
        linkLast(e);
        return find(e);
    }

    @Override
    public void addAll(E[] t) {
        for (Object o : t) {
            linkLast((E) o);
        }
    }

    @Override
    public void addAll(Collection<? extends E> c) {
        E[] array = (E[]) c.toArray();
        addAll(array);
    }

    public E set(int index, E e) {
        checkIndex(index);
        Node<E> node = (Node<E>) get(index);
        E oldValue = node.item;
        node.item = e;
        return oldValue;
    }

    @Override
    public E remove(int index) {
        return unlink((Node<E>) get(index));
    }

    public void clear() {
        for (Node<E> x = first; x != null; ) {
            Node<E> next = x.next;
            x.item = null;
            x.next = null;
            x.prev = null;
            x = next;
        }
        first = last = null;
        size = 0;
    }

    public int find(E e) {
        int i = 0;
        for (Node<E> node = first; node != null; node = node.next) {
            if (node.item == e)
                return i;
            i++;
        }
        return -1;
    }

    public E get(int index) {
        checkIndex(index);
        if (index < (size >> 1)) {
            Node<E> node = first;
            for (int i = 0; i < index; i++)
                node = node.next;
            return (E) node;
        } else {
            Node<E> node = last;
            for (int i = size - 1; i > index; i--)
                node = node.prev;
            return (E) node;
        }
    }

    public void remove(Node<E> node){
        unlink(node);
    }

    public E getElement(int index){
        Node<E> node = (Node<E>) get(index);
        return node.item;
    }


    @Override
    public Object[] toArray() {
        Object[] objects = new Object[size];
        int i = 0;
        for (Node<E> node = first; node != null; node = node.next) {
            objects[i++] = node.item;
        }
        return objects;
    }

    @Override
    public <T> T[] toArray(T[] array) {
        int i = 0;
        for (Node<E> node = first; node != null; node = node.next) {
            ((Object[]) array)[i++] = node.item;
        }
        if (array.length > size)
            array[size] = null;
        return array;
    }

    @Override
    public int size() {
        return size;
    }

    public void trim() {
        int i = 0;
        for (Node<E> node = first; node != null; node = node.next) {
            i++;
            if (node.item == null) {
                remove(i - 1);
                size--;
            }
        }
    }

    private void checkIndex(int index) {
        if (index < 0 || index >= size)
            throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size);
    }

    private static class Node<E> {
        E item;
        Node<E> next;
        Node<E> prev;

        Node(Node<E> prev, E element, Node<E> next) {
            this.item = element;
            this.next = next;
            this.prev = prev;
        }

        @Override
        public String toString(){
            return "Item:" + item;
        }
    }

    public class Iter implements ListIterator<E> {
        private Node<E> lastReturned;
        private Node<E> next;
        private int nextIndex;

        Iter(int index){
            next = (index == size) ? null : (Node<E>) get(index);
            nextIndex = size;
        }

        Iter() {
            next = (0 == size) ? null : (Node<E>) get(0);
            nextIndex = 0;
        }

        @Override
        public E getNext() {
            if (!hasNext())
                throw new NoSuchElementException();
            lastReturned = next;
            next = next.next;
            nextIndex++;
            return lastReturned.item;
        }

        @Override
        public boolean hasPrevious(){
            return nextIndex>0;
        }

        @Override
        public E getPrevious(){
            if (!hasPrevious())
                throw new NoSuchElementException();
            lastReturned = next;
            next= (next == null) ? last : next.prev;
            nextIndex--;
            return lastReturned.item;
        }

        @Override
        public boolean hasNext() {
            return nextIndex != size;
        }

        @Override
        public void remove() {
            if (lastReturned == null)
                throw new IllegalStateException();

            Node<E> lastNext = lastReturned.next;
            unlink(lastReturned);
            if (next == lastReturned)
                next = lastNext.next;
            else {
                next = lastNext;
                nextIndex--;
            }
        }

        @Override
        public void addBefore(E newElement) {
            Node<E> pred = next.prev;
            add(pred,newElement);
        }

        @Override
        public void addAfter(E newElement) {
            Node<E> pred = lastReturned.next;
            add(pred,newElement);
        }

        private void add(Node<E> pred, E newElement){
            Node<E> newNode = new Node<>(pred, newElement, pred.next);
            next.prev = newNode;
            if (pred == null) first = newNode;
            else pred.next = newNode;
            size++;
            nextIndex++;
        }
    }
}
