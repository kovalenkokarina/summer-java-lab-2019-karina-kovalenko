package collection;

public interface Iterator<E> {

    E getNext();

    boolean hasNext();

    void remove();

    void addBefore(E e);

    void addAfter(E e);

}
