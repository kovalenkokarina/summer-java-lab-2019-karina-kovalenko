package collection.queue;

import collection.Collection;
import collection.Iterator;
import collection.abstractList.LinkList;

public class ListQueue<E> implements Queue<E> {

    private int size;
    private LinkList<E> list;

    public ListQueue(){
        list = new LinkList<>();
    }

    public ListQueue(Collection<? extends E> col){
        list = new LinkList<>();
        list.addAll(col);
        size = col.size();
    }

    @Override
    public Iterator<E> getIterator() {
        return list.getIterator();
    }

    @Override
    public boolean isEmpty() {
        return size==0;
    }

    @Override
    public E poll() throws Exception {
        if(isEmpty())
            throw new Exception("Queue is empty");
        E obj = list.getElement(0);
        list.remove(0);
        size--;
        return obj;
    }

    @Override
    public E pull() throws Exception {
        if(isEmpty())
            throw new Exception("Queue is empty");
        return list.getElement(size-1);
    }

    @Override
    public E peek() throws Exception {
        if(isEmpty())
            throw new Exception("Queue is empty");
        return list.getElement(0);
    }

    @Override
    public E remove() throws Exception {
        if(isEmpty())
            throw new Exception("Queue is empty");
        int index = size - 1;
        E obj = list.getElement(index);
        list.remove(index);
        size--;
        return obj;
    }

    @Override
    public int search(E e) {
        return list.find(e);
    }

    @Override
    public void push(E e) {
        list.add(e);
        size++;
    }

    @Override
    public void pushAll(E[] array) {
        for (E e : array) push(e);
    }

    @Override
    public void pushAll(Collection<? extends E> c) {
        E [] array = (E[]) c.toArray();
        pushAll(array);
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public int clear() {
        list.clear();
        int oldSize = size;
        size = 0;
        return oldSize;
    }
}
