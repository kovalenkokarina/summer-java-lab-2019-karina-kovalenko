package collection.queue;

import collection.Collection;
import collection.Iterator;

public interface Queue<E> {

    Iterator<E> getIterator();

    boolean isEmpty();

    E poll() throws Exception;

    E pull() throws Exception;

    E peek() throws Exception;

    E remove() throws Exception;

    int search(E e);

    void push(E e);

    void pushAll(E [] array);

    void pushAll(Collection<?extends E> c);

    int size();

    int clear();
}
