package collection.queue;

import collection.Collection;
import collection.Iterator;
import collection.abstractList.Array;
public class ArrayQueue<E> implements Queue<E> {

    private int size;

    private Array<E> array;

    public ArrayQueue(){
        array = new Array<>();
    }

    public ArrayQueue(Collection<? extends E> col){
        array = new Array<>(col);
        size = col.size();
    }

    public ArrayQueue(int capacity){
        array = new Array<>(capacity);
        size = capacity;
    }

    @Override
    public Iterator<E> getIterator() {
        return array.getIterator();
    }

    @Override
    public boolean isEmpty() {
        return array.size() ==0;
    }

    @Override
    public E poll() throws Exception {
        if(isEmpty())
            throw new Exception("Queue is empty");
        E obj = array.get(0);
        array.remove(0);
        size--;
        return obj;
    }

    @Override
    public E pull() throws Exception {
        if(isEmpty())
            throw new Exception("Queue is empty");
        return array.get(array.size()-1);
    }

    @Override
    public E peek() throws Exception {
        if(isEmpty())
            throw new Exception("Queue is empty");
        return array.get(0);
    }

    @Override
    public E remove() throws Exception {
        if(isEmpty())
            throw new Exception("Queue is empty");
        int index = size - 1;
        E obj = array.get(index);
        remove(index);
        return obj;
    }

    @Override
    public int search(E e) {
        return array.find(e);
    }

    @Override
    public void push(E e) {
        array.add(e);
        size++;
    }

    @Override
    public void pushAll(E[] a) {
        for (E e : a) push(e);
    }

    @Override
    public void pushAll(Collection<? extends E> c) {
        E [] array = (E[]) c.toArray();
        pushAll(array);
    }

    @Override
    public int size() {
        return array.size();
    }

    @Override
    public int clear() {
        array.clear();
        int oldSize = size;
        size = 0;
        return oldSize;
    }

    private E get(int index){
        return array.get(index);
    }

    public void add(int index, E e){
        array.add(index,e);
        size++;
    }

    private E remove(int index) {
        E oldElement = array.remove(index);
        size--;
        return oldElement;
    }
}
