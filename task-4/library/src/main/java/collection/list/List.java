package collection.list;

import collection.Collection;
import collection.Iterator;

public interface List<E> extends Collection<E> {

    Iterator<E> getIterator();

    int add(E e);

    void addAll(E[] e);

    void addAll(Collection <?extends E> c);

    E set(int index, E e);

    E remove(int index);

    void clear();

    int find(E e);

    E get(int index);

    <T> T[] toArray(T[] a);

    int size();

    void trim();

    void filterMatches(E[] e);

    void filterMatches(Collection <?extends E> c);

    void filterDifference(E[] e);

    void filterDifference(Collection <?extends E> c);

    void setMaxSize(int size);

    int getMaxSize();
}
