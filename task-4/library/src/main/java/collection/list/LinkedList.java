package collection.list;

import collection.Collection;
import collection.HashComparator;
import collection.Iterator;
import collection.ListIterator;
import collection.abstractList.LinkList;

import java.util.Arrays;
import java.util.Comparator;

public class LinkedList<E> implements List<E> {

    private LinkList<E> list;

    private int size;
    private int maxSize;

    private Comparator<? super Object> comparator;

    public LinkedList(Comparator<? super Object> comparator){
        this.comparator = comparator;
        list = new LinkList<>();
    }

    public LinkedList() {
        list = new LinkList<>();
    }

    private void sort(){
        if (comparator!=null){
            Object [] objects = list.toArray();
            Arrays.sort(objects, comparator);
            list = new LinkList(objects);
        }else{
            Object [] objects = list.toArray();
            Arrays.sort(objects, new HashComparator());
            list = new LinkList(objects);
        }
    }

    @Override
    public Iterator<E> getIterator() {
        return new IterSort();
    }

    @Override
    public int add(E e) {
        checkMaxSize(size+1);
        list.add(e);
        sort();
        size++;
        return list.find(e);
    }

    @Override
    public void addAll(E[] t) {
        list.addAll(t);
        sort();
        size+=t.length;
    }

    @Override
    public void addAll(Collection<? extends E> c) {
        E[] array = (E[]) c.toArray();
        addAll(array);
    }

    @Override
    public E set(int index, E t) {
        E oldValue = list.set(index,t);
        sort();
        return oldValue;
    }

    @Override
    public E remove(int index) {
        E oldValue = list.remove(index);
        size--;
        sort();
        return oldValue;
    }

    @Override
    public void clear() {
        list.clear();
        size=0;
    }

    @Override
    public int find(E e) {
        return list.find(e);
    }

    @Override
    public E get(int index) {
        return list.get(index);
    }

    @Override
    public Object[] toArray() {
        return list.toArray();
    }

    @Override
    public <T> T[] toArray(T[] array) {
        return list.toArray(array);
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public void trim() {
        list.trim();
        size = list.size();
    }

    @Override
    public void filterMatches(E[] array) {
        LinkList<E> newList = filterTheSame(array);
        for (int k=0;k<newList.size();k++){
            E obg = newList.getElement(k);
            int index = list.find(obg);
            if(index!=-1){
                list.remove(index);
            }
        }
        size = list.size();
    }

    @Override
    public void filterMatches(Collection<? extends E> c) {
        filterMatches((E[]) c.toArray());
    }

    @Override
    public void filterDifference(E[] array) {
        list = filterTheSame(array);
        size = list.size();
    }

    private LinkList filterTheSame(E[] array){
        LinkList<E> newList = new LinkList<>();
        for (int i = 0;i<size;i++){
            E obj = list.getElement(i);
            for (E e : array) {
                if (obj == null) continue;
                if (obj.equals(e))
                    newList.add(obj);
            }
        }
        return newList;
    }

    @Override
    public void filterDifference(Collection<? extends E> c) {
        filterDifference((E[]) c.toArray());
    }

    @Override
    public void setMaxSize(int maxSize) {
        if (maxSize < size) {
            for (int i = size-1 ; i >= maxSize; i--) {
                remove(i);
            }
            this.maxSize = maxSize;
        } else this.maxSize = maxSize;
    }

    @Override
    public int getMaxSize() {
        return maxSize;
    }

    private void checkMaxSize(int size) {
        if (maxSize != 0) {
            if (size > maxSize) {
                throw new UnsupportedOperationException("Maximum Size "
                        + maxSize + " reached");
            }
        }
    }

    private class IterSort implements ListIterator<E> {

        LinkList.Iter iter;

        IterSort() {
            iter = (LinkList.Iter) list.getIterator();
        }

        @Override
        public E getNext() {
            return (E) iter.getNext();
        }

        @Override
        public boolean hasNext() {
            return iter.hasNext();
        }

        @Override
        public void remove() {
            iter.remove();
            size--;
        }

        @Override
        public void addBefore(E newElement) {
            iter.addBefore(newElement);
            size++;
            sort();
        }

        @Override
        public void addAfter(E newElement) {
            iter.addAfter(newElement);
            size++;
            sort();
        }

        @Override
        public boolean hasPrevious() {
            return iter.hasPrevious();
        }

        @Override
        public E getPrevious() {
            return (E) iter.getPrevious();
        }
    }

}

