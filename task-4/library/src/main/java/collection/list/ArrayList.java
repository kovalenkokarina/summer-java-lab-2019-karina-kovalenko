package collection.list;

import collection.Collection;
import collection.HashComparator;
import collection.Iterator;
import collection.abstractList.Array;
import java.util.Arrays;
import java.util.Comparator;
import java.util.NoSuchElementException;

public class ArrayList<E> implements List<E> {

    private Comparator<? super Object> comparator;
    private int size;
    private int maxSize;
    private Array array;

    public ArrayList(Object [] objects){
        array = new Array<>();
        array.addAll(objects);
    }

    public ArrayList(int capacity){
        array = new Array<>(capacity);
        size = capacity;
    }

    public ArrayList(Comparator<? super Object> comparator){
        this.comparator = comparator;
        array = new Array<>();
    }

    public ArrayList() {
        array = new Array<>();
    }

    @Override
    public Iterator<E> getIterator() {
        return new Iter();
    }

    @Override
    public Object[] toArray() {
        return array.toArray();
    }

    private void sort(){
        if (comparator!=null){
            Object [] objects = array.toArray();
            Arrays.sort(objects,comparator);
            array = new Array(objects);
        }else {
            Object [] objects = array.toArray();
            Arrays.sort(objects,new HashComparator());
            array = new Array(objects);
        }
    }

    public int add(E e){
        checkMaxSize(size + 1);
        array.add(e);
        sort();
        size++;
        return array.find(e);
    }

    public void addAll(E[] a) {
        checkMaxSize(size + a.length);
        for (E e : a) array.add(e);
        sort();
        size+=a.length;
    }

    public void addAll(Collection<? extends E> c){
        addAll((E[]) c.toArray());
    }

    public E set(int index, E e) {
        E oldElement = (E) array.set(index,e);
        sort();
        return oldElement;
    }

    public E remove(int index) {
        E oldElement = (E) array.remove(index);
        sort();
        size--;
        return oldElement;
    }

    public void clear() {
        array.clear();
        size=0;
    }

    public int find(E e) {
        return array.find(e);
    }

    public E get(int index) {
        return (E) array.get(index);
    }

    public <T> T[] toArray(T[] a) {
        return (T[]) array.toArray(a);
    }

    public int size() {
        return array.size();
    }

    public void trim() {
        array.trim();
    }

    public void filterMatches(E[] a){
        Array<E> newArray = filterTheSame(a);
        for (int k =0;k<newArray.size();k++){
            E obj = newArray.get(k);
            int index = array.find(obj);
            if(index!=-1){
                array.remove(index);
            }
        }
        size = array.size();
    }

    public void filterMatches(Collection <?extends E> c){
        filterMatches((E[]) c.toArray());
    }

    public void filterDifference(E[] a) {
        array  = filterTheSame(a);
        size = array.size();
    }

    public void filterDifference(Collection <?extends E> c){
        filterDifference((E[]) c.toArray());
    }

    private Array filterTheSame(E[] a){
        Array<E> newArray = new Array<>();
        for(int i=0;i<array.size();i++){
            Object object = array.get(i);
            for (E e : a) {
                if (object.equals(e)) {
                    newArray.add((E) object);
                }
            }
        }
        return newArray;
    }

    public void setMaxSize(int maxSize) {
        if(size>maxSize){
            int del = size - maxSize;
            Object [] objects = Arrays.copyOf(array.toArray(),size-del);
            array.clear();
            array.addAll(objects);
            this.maxSize = maxSize;
            size = maxSize;
        }else this.maxSize = maxSize;
    }

    public int getMaxSize() {
        return maxSize;
    }

    private void checkMaxSize(int size){
        if(maxSize!=0) {
            if(size > maxSize){
                throw new UnsupportedOperationException("Maximum Size "
                        + maxSize + " reached");
            }
        }
    }

    private class Iter implements Iterator<E>{

        private int currentIndex;
        private int lastIndex=-1;

        Iter() {}

        @Override
        public E getNext() {
            int i = currentIndex;
            if(i>=size)
                throw new NoSuchElementException();
            lastIndex = i;
            currentIndex++;
            return (E) array.get(i);
        }

        @Override
        public boolean hasNext() {
            return currentIndex!=size;
        }

        @Override
        public void remove() {
            array.remove(lastIndex);
            currentIndex = lastIndex;
            lastIndex = -1;
        }

        @Override
        public void addBefore(E e) {
            int i = currentIndex;
            array.add(i,e);
            sort();
        }

        @Override
        public void addAfter(E e) {
            int i = currentIndex;
            array.add(i+1,e);
            sort();
        }
    }

}
