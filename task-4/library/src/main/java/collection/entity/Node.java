package collection.entity;

public class Node<K,V> implements Entity<K, V>  {

    private int hash;

    private Node next;

    private K key;

    private V value;

    public Node(K key, V value) throws Exception {
        if((key == null) || (value == null))
            throw new Exception("Cannot insert null.");
        this.key = key;
        this.value = value;
    }

    public Node(int hash, K key, V value, Node next) throws Exception {
        if((key == null) || (value == null))
            throw new Exception("Cannot insert null.");
        this.key = key;
        this.value = value;
        this.hash = hash;
        this.next = next;
    }

    @Override
    public K getKey() {
        return key;
    }

    @Override
    public V getValue() {
        return value;
    }

    public Node getNext() {
        return next;
    }

    public void setNext(Node next) {
        this.next = next;
    }

    public int getHash(){
        return hash;
    }

    public void setValue(V newValue){
        value = newValue;
    }

    @Override
    public String toString() {
        return "Key: " + key + ", value: " + value;
    }
}
