package collection.entity;

public interface Entity<K,V> {

    K getKey();

    V getValue();
}
