package collection.entity;

public enum Gender {
    MALE("Male"),
    FEMALE("Female");

    private String gender;

    Gender(String gender){
        this.gender = gender;
    }
}
