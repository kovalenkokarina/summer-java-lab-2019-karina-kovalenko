package collection;

public class HashComparator<E> implements java.util.Comparator<E> {
    @Override
    public int compare(Object o1, Object o2) {
        if(o1 == null || o2 == null){
            if (o1 == null && o2 == null) {
                return 0;
            }
            if (o1 == null) {
                return 1;
            }
            if (o2 == null) {
                return -1;
            }
        }else {
            return Integer.compare(o1.hashCode(), o2.hashCode());
        }
        return 0;
    }
}
