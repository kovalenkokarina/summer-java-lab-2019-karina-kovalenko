package collection;

public interface ListIterator<E> extends Iterator<E> {

    boolean hasPrevious();

    E getPrevious();
}
