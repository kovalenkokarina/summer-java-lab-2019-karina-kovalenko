package collection;

public interface Collection<E> {

    int size();

    Iterator<E> getIterator();

    Object[] toArray();

    <T> T[] toArray(T[] a);

    int add(E e);

    E remove(int index);

    void addAll(E[] a);

    void addAll(Collection<? extends E> c);

}
