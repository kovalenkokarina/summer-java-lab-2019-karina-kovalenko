package collection.stack;

import collection.Collection;
import collection.Iterator;

import java.util.Comparator;

public interface Stack<E>  {

    Iterator<E> getIterator();

    boolean isEmpty();

    E peek();

    E pop();

    int search(E e);

    void push(E e);

    void pushAll(E [] array);

    void pushAll(Collection<?extends E> c);

    int size();

    int clear();

    void sort(Comparator<? super Object> comparator);

}
