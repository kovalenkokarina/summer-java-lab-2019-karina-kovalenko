package collection.stack;

import collection.Collection;
import collection.Iterator;
import collection.ListIterator;
import collection.abstractList.LinkList;

import java.util.Arrays;
import java.util.Comparator;
import java.util.EmptyStackException;

public class ListStack<E> implements Stack<E> {

    private int size;
    private LinkList<E> list;

    public ListStack(){
        list = new LinkList<>();
    }

    public ListStack(Object [] objects){
        list = new LinkList<>();
        list.addAll((E[]) objects);
        size+=objects.length;
    }

    @Override
    public Iterator<E> getIterator() {
        return new Iter();
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public E peek() {
        if(size==0)
            throw new EmptyStackException();
        return list.getElement(size-1);
    }

    @Override
    public E pop() {
        E object = peek();
        list.remove(size-1);
        size--;
        return object;
    }

    @Override
    public int search(E e) {
        return list.find(e);
    }

    @Override
    public void push(E e) {
        if(e!=null) {
            if (list.find(e) == -1) {
                list.add(e);
                size++;
            }
        }
    }

    @Override
    public void pushAll(E[] array){
        for (E e : array) {
            push(e);
        }
    }

    @Override
    public void pushAll(Collection<? extends E> c) {
        E [] array = (E[]) c.toArray();
        pushAll(array);
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public int clear() {
        list.clear();
        int oldSize = size;
        size = 0;
        return oldSize;
    }

    @Override
    public void sort(Comparator<? super Object> comparator) {
        Object [] objects = list.toArray();
        Arrays.sort(objects,comparator);
        list = new LinkList<>(objects);
    }

    private class Iter implements ListIterator<E> {

        ListIterator iterator;

        Iter(){
            iterator = (ListIterator) list.getListIterator();
        }
        @Override
        public E getNext() {
            return (E) iterator.getPrevious();
        }

        @Override
        public boolean hasNext() {
            return iterator.hasPrevious();
        }

        @Override
        public void remove() {
            iterator.remove();
        }

        @Override
        public void addBefore(E e) {
            iterator.addBefore(e);
        }

        @Override
        public void addAfter(E e) {
            iterator.addAfter(e);
        }

        @Override
        public boolean hasPrevious() {
            return iterator.hasNext();
        }

        @Override
        public E getPrevious() {
            return (E) iterator.getNext();
        }
    }

}
