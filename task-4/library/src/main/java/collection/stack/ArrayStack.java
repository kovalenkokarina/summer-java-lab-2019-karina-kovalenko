package collection.stack;

import collection.Collection;
import collection.Iterator;
import collection.abstractList.Array;

import java.util.Arrays;
import java.util.Comparator;
import java.util.EmptyStackException;
import java.util.NoSuchElementException;

public class ArrayStack<E> implements Stack<E> {

    private int size;
    private Array<E> arrayData;

    public ArrayStack(){
        arrayData = new Array<>();
    }

    public ArrayStack(Object [] objects){
        arrayData = new Array<>();
        arrayData.addAll((E[]) objects);
        size+=objects.length;
    }

    @Override
    public Iterator<E> getIterator() {
        return new Iter();
    }

    @Override
    public boolean isEmpty() {
        return size==0;
    }

    @Override
    public E peek() {
        int size = arrayData.size();
        if(size==0)
            throw new EmptyStackException();
        return arrayData.get(size-1);
    }

    @Override
    public E pop() {
        E object = peek();
        arrayData.remove(size-1);
        size--;
        return object;
    }

    @Override
    public int search(E e) {
        return arrayData.find(e);
    }

    @Override
    public void push(E e) {
        if(arrayData!=null) {
            if (arrayData.find(e) == -1) {
                arrayData.add(e);
                size++;
            }
        }
    }
     @Override
    public void pushAll(E[] array){
         for (E e : array) {
             push(e);
         }
    }

    @Override
    public void pushAll(Collection<? extends E> c) {
        E [] array = (E[]) c.toArray();
        pushAll(array);
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public int clear() {
        arrayData.clear();
        int oldSize = size;
        size = 0;
        return oldSize;
    }

    @Override
    public void sort(Comparator<? super Object> comparator) {
        Object [] objects = arrayData.toArray();
        Arrays.sort(objects,comparator);
        arrayData = new Array<>(objects);
    }

    public class Iter implements Iterator<E>{
        private int size = arrayData.size();
        private int currentIndex = size-1;
        private int lastIndex=-1;

        @Override
        public E getNext() {
            int i = currentIndex;
            if(i>size)
                throw new NoSuchElementException();
            lastIndex = i;
            currentIndex--;
            return arrayData.get(i);
        }

        @Override
        public boolean hasNext() {
            return (currentIndex!=size) && (currentIndex!=-1);
        }

        @Override
        public void remove() {
            arrayData.remove(lastIndex);
            currentIndex = lastIndex;
            lastIndex = -1;
        }

        @Override
        public void addBefore(E e) {
            int i = currentIndex;
            if(i>=size)
                throw new NoSuchElementException();
            if(hasNext()) {
                arrayData.add(i+1,e);
            }
        }

        @Override
        public void addAfter(E e) {
            int i = currentIndex;
            if(i>=size)
                throw new NoSuchElementException();
                arrayData.add(i,e);
        }
    }
}
