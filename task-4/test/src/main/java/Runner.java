import collection.Iterator;
import collection.entity.Gender;
import collection.entity.Node;
import collection.entity.User;
import collection.list.ArrayList;
import collection.list.LinkedList;
import collection.map.ArrayMap;
import collection.map.ListMap;
import collection.map.Map;
import collection.queue.ArrayQueue;
import collection.queue.ListQueue;
import collection.queue.Queue;
import collection.stack.ArrayStack;
import collection.stack.ListStack;
import collection.stack.Stack;


public class Runner {
    public static void main(String[] args) throws Exception {
        Integer [] array = new Integer[]{1,30,6,2,23,53,64};
        ArrayList<Integer> collection = new ArrayList<Integer>(new Integer[] {4,9,23,565,5,3});

        ArrayList<Integer> arrayList = new ArrayList();
        arrayList.addAll(array);
        arrayList.add(null);
        arrayList.add(2);
        arrayList.add(5);
        arrayList.addAll(collection);
        arrayList.add(null);
        System.out.println("Initial ArrayList: size = " + arrayList.size());
        Show(arrayList.getIterator());
        arrayList.setMaxSize(10);
        arrayList.trim();
        System.out.println("Old removed value: " + arrayList.remove(6));
        System.out.println("New value: 1000; Old value: " +arrayList.set(7,1000));
        System.out.println("ArrayList after setting the maximum size and trim:" + arrayList.size());
        Show(arrayList.getIterator());
        arrayList.filterMatches(array);
        System.out.println("Filtering on various elements {1,30,6,2,23,53,64}:");
        Show(arrayList.getIterator());
        Iterator<Integer> iterArrayList = arrayList.getIterator();
        while (iterArrayList.hasNext()){
            if(iterArrayList.getNext()==4)
                iterArrayList.addAfter(777);
        }
        System.out.println("Inserted '777' after the object referenced by the iterator:");
        Show(arrayList.getIterator());

        LinkedList<Integer> linkedList = new LinkedList<Integer>();
        linkedList.add(10);
        linkedList.add(null);
        linkedList.add(15);
        linkedList.addAll(collection);
        linkedList.add(100);
        System.out.println("\nInitial LinkedList: size = " + linkedList.size());
        Show(linkedList.getIterator());
        linkedList.setMaxSize(8);
        linkedList.trim();
        System.out.println("LinkedList after setting the maximum size and trim:" + linkedList.size());
        Show(linkedList.getIterator());
        linkedList.filterDifference(collection);
        System.out.println("Filtering the same elements {4,9,23,565,5,3}:");
        Show(linkedList.getIterator());
        System.out.println("Old removed value: " + linkedList.remove(0));
        System.out.println("New value:1000; Old value: " +linkedList.set(1,1000));
        System.out.println("Size: "+linkedList.size());
        System.out.println("Maximum size: "+linkedList.getMaxSize());
        System.out.println("Search for 30: "+linkedList.find(30));
        Iterator<Integer> iterLinkedList = linkedList.getIterator();
        while (iterLinkedList.hasNext()){
            if(iterLinkedList.getNext()==4)
                iterLinkedList.addBefore(777);
        }
        System.out.println("Inserted '777' after the object referenced by the iterator:");
        Show(linkedList.getIterator());


        Queue<Integer> queue = new ArrayQueue<Integer>();
        queue.push(1000);
        queue.push(3);
        queue.pushAll(collection);
        queue.pushAll(array);
        System.out.println("\nInitial ArrayQueue: size = " + queue.size());
        Show(queue.getIterator());
        System.out.println("Is empty: "+ queue.isEmpty());
        System.out.println("Peek: "+ queue.peek());
        System.out.println("Poll: "+queue.poll());
        System.out.println("Pull: "+ queue.pull());
        System.out.println("Remove: "+ queue.remove());
        System.out.println("Search '9': "+ queue.search(9));
        System.out.println("Size: "+ queue.size());
        System.out.println("After functions:");
        Show(queue.getIterator());

        Queue<Integer> listQueue = new ListQueue<Integer>();
        listQueue.push(1000);
        listQueue.push(3);
        listQueue.pushAll(collection);
        listQueue.pushAll(array);
        System.out.println("\nInitial ListQueue: size = " + listQueue.size());
        Show(listQueue.getIterator());
        System.out.println("Is empty: "+ listQueue.isEmpty());
        System.out.println("Peek: "+ listQueue.peek());
        System.out.println("Poll: "+listQueue.poll());
        System.out.println("Pull: "+ listQueue.pull());
        System.out.println("Remove: "+ listQueue.remove());
        System.out.println("Search '9': "+ listQueue.search(9));
        System.out.println("Size: "+ listQueue.size());
        System.out.println("After functions:");
        Show(listQueue.getIterator());
        System.out.println("Clear: "+ listQueue.clear());

        Stack<Integer> stack = new ArrayStack<Integer>();
        stack.push(1000);
        stack.push(3);
        stack.pushAll(collection);
        stack.pushAll(array);
        System.out.println("\nInitial ArrayStack: size = " + stack.size());
        Show(stack.getIterator());
        System.out.println("Is empty: "+ stack.isEmpty());
        System.out.println("Peek: "+ stack.peek());
        System.out.println("Pop: "+stack.pop());
        System.out.println("Search '9': "+ stack.search(9));
        System.out.println("Size: "+ stack.size());
        MyComparator comparator = new MyComparator();
        stack.sort(comparator);
        System.out.println("After functions:");
        Show(stack.getIterator());
        System.out.println("Clear: "+ stack.clear());

        Stack<Integer> listStack = new ListStack<>();
        listStack.push(1000);
        listStack.push(3);
        listStack.pushAll(collection);
        listStack.pushAll(array);
        System.out.println("\nInitial ListStack: size = " + listStack.size());
        Show(listStack.getIterator());
        System.out.println("Is empty: "+ listStack.isEmpty());
        System.out.println("Peek: "+ listStack.peek());
        System.out.println("Pop: "+listStack.pop());
        System.out.println("Search '9': "+ listStack.search(9));
        System.out.println("Size: "+ listStack.size());
        listStack.sort(comparator);
        System.out.println("After functions:");
        Show(listStack.getIterator());
        System.out.println("Clear: "+ listStack.clear());

        Map<String, Integer> map = new ArrayMap<String, Integer>(2);
        map.set("1",4);
        map.set("2",6);
        map.set("3",7);
        map.set("0",2);
        map.set("4",9);
        Node<String,Integer> node = new Node<String, Integer>("5",5);
        map.set(node);
        System.out.println("\nArrayMap:\nIs empty: "+ map.isEmpty());
        System.out.println("Is contains '7': "+ map.contains(7));
        System.out.println("Delete by key '3': " + map.remove("3").toString());
        System.out.println("Delete Node('5',5): "+ map.remove(node).toString());
        System.out.println("Size: "+ map.size());
        System.out.println("Keys:");
        ArrayList<Integer> keys = (ArrayList<Integer>) map.getKeys();
        Show(keys.getIterator());
        System.out.println("Values:");
        ArrayList<Integer> values = (ArrayList<Integer>) map.getValues();
        Show(values.getIterator());

        Map<String, Integer> listMap = new ListMap<String, Integer>();
        map.set("1",4);
        map.set("2",6);
        map.set("3",7);
        map.set("0",2);
        map.set("4",9);
        map.set(node);

        System.out.println("\nListMap:\nIs empty: "+ map.isEmpty());
        System.out.println("Get value by key '3': "+map.get("3"));
        System.out.println("Is contains '7': "+ map.contains(7));
        System.out.println("Delete by key '3': " + map.remove("3").toString());
        System.out.println("Delete Node('5',5): "+ map.remove(node).toString());
        System.out.println("Size: "+ map.size());
        System.out.println("Keys:");
        ArrayList<Integer> keys1 = (ArrayList<Integer>) map.getKeys();
        Show(keys.getIterator());
        System.out.println("Values:");
        ArrayList<Integer> values1 = (ArrayList<Integer>) map.getValues();
        Show(values.getIterator());

        Map<String, User> userMap = new ArrayMap<>();
        userMap.set("A",new User("Alina","Kitaneva@gmail.com",15,Gender.FEMALE));
        userMap.set("V",new User("Vika","Matuzko@mail.ru",18,Gender.FEMALE));
        User userDima = new User("Dima","Gaika@icloude.com",25,Gender.MALE);
        userMap.set("D",userDima);

        System.out.println("Keys:");
        ArrayList<String> stringKey = (ArrayList<String>) userMap.getKeys();
        Show(stringKey.getIterator());
        System.out.println("Values:");
        Show(userMap.getValues().getIterator());

        System.out.println("Get value by key 'V': "+userMap.get("V").toString());

        System.out.println("Is contains 'Dima Gaika': "+ userMap.contains(userDima));

    }

    public static void Show(Iterator iterator){
        Iterator<Integer> iter = iterator;
        String str = "";
        while (iter.hasNext()){
            str+=iter.getNext() + " ";
        }
        System.out.println(str);
    }
}


