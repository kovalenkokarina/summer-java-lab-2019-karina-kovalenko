import java.util.Comparator;

public class MyComparator<E> implements Comparator<E> {

    MyComparator(){

    }
    @Override
    public int compare(E o1, E o2) {
        if(o1 == null || o2 == null){
            if (o1 == null && o2 == null) {
                return 0;
            }
            if (o1 == null) {
                return 1;
            }
            if (o2 == null) {
                return -1;
            }
        }else {
            if (o1.hashCode() > o2.hashCode()) {
                return 1;
            } else if (o1.hashCode() < o2.hashCode()) {
                return -1;
            } else {
                return 0;
            }
        }
        return 0;
    }
}
